PARSER_BEGIN(MiniJavaParser)
package parser;

import java.util.List;
import java.util.LinkedList;
import java.util.Stack;
import java.util.Arrays;
import syntax.*;

public class MiniJavaParser {
   public static final List<ParseException> syntaxErrors = new LinkedList<ParseException>();
}
PARSER_END(MiniJavaParser)

SKIP : {
   < "//" (~["\n"])* "\n" >
 | < "/*" (["\u0000"-"\uFFFE"])* "*/" >
 | < " " | "\t" | "\n" | "\f" | "\r" | "\u000B" | "\u001C" | "\u001D" | "\u001E" | "\u001F">
}

TOKEN : {
   < CLASS : "class" >
 | < PUBLIC : "public" >
 | < STATIC : "static" >
 | < PRINT : "System.out.println" >
 | < VOID : "void" >
 | < EXTENDS : "extends" >
 | < RETURN : "return" >
 | < INT_TYPE : "int" >
 | < BOOLEAN_TYPE : "boolean" >
 | < STRING : "String" >
 | < MAIN : "main" >
 | < LENGTH : "length" >
 | < IF : "if" >
 | < ELSE : "else" >
 | < WHILE : "while" >
 | < TRUE : "true" >
 | < FALSE : "false" >
 | < THIS : "this" >
 | < NEW : "new" >
 | < BANG : "!" >
 | < LEFT_PAREN : "(" >
 | < RIGHT_PAREN : ")" >
 | < LEFT_BRACE : "{" >
 | < RIGHT_BRACE : "}" >
 | < LEFT_BRACKET : "[" >
 | < RIGHT_BRACKET : "]" >
 | < ASSIGN : "=" >
 | < DOT : "." >
 | < COMMA : "," >
 | < SEMICOLON : ";" >
 | < INT_LITERAL : ["0"-"9"] (["0"-"9"])* >
 | < AND : "&&" >
 | < LT : "<" >
 | < PLUS : "+" >
 | < MINUS : "-" >
 | < TIMES : "*" >
 | < ID : ["a"-"z", "A"-"Z"] (["a"-"z", "A"-"Z", "0"-"9", "_"])* >
 | < ERROR : ["\u0000"-"\uFFFE"] >
}

Identifier Identifier() : {} {
   { final Token t; }
   t = <ID>

   { return new syntax.Identifier(t.beginLine, t.beginColumn, t.image); }
}

IdentifierExp IdentifierExp() : {} {
   { final Token t; }
   t = <ID>

   { return new syntax.IdentifierExp(t.beginLine, t.beginColumn, t.image); }
}

IntegerLiteral IntegerLiteral() : {} {
   { final Token t; }
   t = <INT_LITERAL>

   { return new syntax.IntegerLiteral(Integer.parseInt(t.image)); }
}

Program Program() : {} {
   { final MainClass mainClass; }
   mainClass = MainClass()

   { final List<ClassDecl> classDecls = new LinkedList<ClassDecl>(); }
   (
      { final ClassDecl classDecl; }
      classDecl = ClassDecl()
      { classDecls.add(classDecl); }
   )*

   { return new syntax.Program(mainClass, classDecls); }
}

MainClass MainClass() : {} {
   { final Token classToken; }
   classToken = <CLASS>
   { final int line = classToken.beginLine, column = classToken.beginColumn; }

   { final Identifier classId; }
   classId = Identifier()

   <LEFT_BRACE>
      <PUBLIC> <STATIC> <VOID> <MAIN> <LEFT_PAREN>
         <STRING> <LEFT_BRACKET> <RIGHT_BRACKET>
         { final Identifier argsId; }
         argsId = Identifier()
      <RIGHT_PAREN>
      <LEFT_BRACE>
         { final Statement body; }
         body = Statement()
      <RIGHT_BRACE>
   <RIGHT_BRACE>

   { return new syntax.MainClass(line, column, classId, argsId, body); }
}

ClassDecl ClassDecl() : {} {
   <CLASS>

   { final Identifier className; }
   className = Identifier()

   { Identifier superclassName = null; }
   [
      LOOKAHEAD(1) <EXTENDS>
      superclassName = Identifier()
   ]

   <LEFT_BRACE>
      { final List<VarDecl> fields = new LinkedList<VarDecl>(); }
      (
         { final VarDecl field; }
         field = VarDecl()
         { fields.add(field); }
      )*

      { final List<MethodDecl> methods = new LinkedList<MethodDecl>(); }
      (
         { final MethodDecl method; }
         method = MethodDecl()
         { methods.add(method); }
      )*
   <RIGHT_BRACE>

   {
      return superclassName == null ?
            new syntax.SimpleClassDecl(className, fields, methods) :
            new syntax.ExtendingClassDecl(className, superclassName, fields, methods);
   }
}

VarDecl VarDecl() : {
   VarDecl varDecl = null;
}{
   try {
      { final Type type; }
      type = Type()

      { final Identifier id; }
      id = Identifier()

      <SEMICOLON>

      { varDecl = new syntax.VarDecl(type, id); }
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return varDecl; }
}

MethodDecl MethodDecl() : {
   MethodDecl methodDecl = null;
}{
   try {
      <PUBLIC>
      
      { final Type returnType; }
      returnType = Type()

      { final Identifier id; }
      id = Identifier()

      <LEFT_PAREN>
         { final List<Formal> params = new LinkedList<Formal>(); }
         [
            { final List<Formal> paramList; }
            paramList = FormalList()
            { params.addAll(paramList); }
         ]
      <RIGHT_PAREN>

      <LEFT_BRACE>
         { final List<VarDecl> localVars = new LinkedList<VarDecl>(); }
         (
            LOOKAHEAD(2)
            { final VarDecl localVar; }
            localVar = VarDecl()
            { localVars.add(localVar); }
         )*

         { final List<Statement> body = new LinkedList<Statement>(); }
         (
            { final Statement st; }
            st = Statement()
            { body.add(st); }
         )*

         <RETURN>
         { final Expression returnVal; }
         returnVal = Exp()
         <SEMICOLON>

         { methodDecl = new syntax.MethodDecl(returnType, id, params, localVars, body, returnVal); }
      <RIGHT_BRACE>
   } catch (ParseException e) {
      recoverError(RIGHT_BRACE);
   }

   { return methodDecl; }
}

List<Formal> FormalList() : {
   final List<Formal> formals = new LinkedList<Formal>();
}{
   try {
      { final Type type; }
      type = Type()

      { final Identifier id; }
      id = Identifier()

      { formals.add(new syntax.Formal(type, id)); }

      (
         { final Formal f; }
         f = FormalRest()
         { formals.add(f); }
      )*
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return formals; }
}

Formal FormalRest() : {
   Formal f = null;
}{
   try {
      <COMMA>
      
      { final Type type; }
      type = Type()

      { final Identifier id; }
      id = Identifier()

      { f = new syntax.Formal(type, id); }
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return f; }
}

List<Expression> ExpList() : {
   final List<Expression> exps = new LinkedList<Expression>();
}{
   try {
      { final Expression exp; }
      exp = Exp()

      { exps.add(exp); }

      (
         { final Expression e; }
         e = ExpRest()
         { exps.add(e); }
      )*
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return exps; }
}

Expression ExpRest() : {
   Expression exp = null;
}{
   try {
      <COMMA>
      exp = Exp()
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return exp; }
}

Type Type() : {
   Type type = null;
}{
   try {
      (
         <BOOLEAN_TYPE>
         { type = Type.THE_BOOLEAN_TYPE; }
      ) | LOOKAHEAD(2) (
         <INT_TYPE> <LEFT_BRACKET> <RIGHT_BRACKET>
         { type = Type.THE_INT_ARRAY_TYPE; }
      ) | (
         <INT_TYPE>
         { type = Type.THE_INTEGER_TYPE; }
      ) | (
         { final Token id; }
         id = <ID>
         { type = new syntax.IdentifierType(id.beginLine, id.beginColumn, id.image); }
      )
         
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return type; }
}

Statement Statement() : {
   Statement statement = null;
   final int line, column;
   final Expression condition;
}{
   try {
      (
         <LEFT_BRACE>
            { final List<Statement> blockStatements = new LinkedList<Statement>(); }
            (
               { final Statement blockStatement; }
               blockStatement = Statement()
               { blockStatements.add(blockStatement); }
            )*
         <RIGHT_BRACE>
         { statement = new syntax.Block(blockStatements); }
      ) | (
         { final Token ifToken; }
         ifToken = <IF>
         { line = ifToken.beginLine; column = ifToken.beginColumn; }

         <LEFT_PAREN>
            condition = Exp()
         <RIGHT_PAREN>

         { final Statement trueClause, falseClause; }
         trueClause = Statement()
         <ELSE>
         falseClause = Statement()

         { statement = new syntax.If(line, column, condition, trueClause, falseClause); }
      ) | (
         { final Token whileToken; }
         whileToken = <WHILE>
         { line = whileToken.beginLine; column = whileToken.beginColumn; }

         <LEFT_PAREN>
            condition = Exp()
         <RIGHT_PAREN>

         { final Statement body; }
         body = Statement()

         { statement = new syntax.While(line, column, condition, body); }
      ) | (
         { final Token printToken; }
         printToken = <PRINT>
         { line = printToken.beginLine; column = printToken.beginColumn; }

         <LEFT_PAREN>
            { final Expression exp; }
            exp = Exp()
         <RIGHT_PAREN>

         <SEMICOLON>

         { statement = new syntax.Print(line, column, exp); }
      ) | (
         { final Identifier id; }
         id = Identifier()

         { Expression arrayIndex = null; }
         [
            <LEFT_BRACKET>
               arrayIndex = Exp()
            <RIGHT_BRACKET>
         ]

         { final Token assignToken; }
         assignToken = <ASSIGN>
         { line = assignToken.beginLine; column = assignToken.beginColumn; }

         { final Expression val; }
         val = Exp()

         <SEMICOLON>

         {
            statement = arrayIndex == null ?
                  new syntax.Assign(line, column, id, val) :
                  new syntax.ArrayAssign(line, column, id, arrayIndex, val);
         }
      )
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return statement; }
}

Expression Exp() : {
   final Token t;
   Expression exp = null;
}{
   try {
      ( 
         exp = IntegerLiteral()
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | (
         exp = IdentifierExp()
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | (
         t = <TRUE>
         { exp = new True(t.beginLine, t.beginColumn); }
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | (
         t = <FALSE>
         { exp = new False(t.beginLine, t.beginColumn); }
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | (
         t = <THIS>
         { exp = new This(t.beginLine, t.beginColumn); }
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | (
         <BANG>
         { final Expression e; }
         e = Exp()
         { exp = new Not(e); }
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | (
         <LEFT_PAREN>
            exp = Exp()
         <RIGHT_PAREN>
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | LOOKAHEAD(2) (
         <NEW> <INT_TYPE>
         <LEFT_BRACKET>
            { final Expression arraySize; }
            arraySize = Exp()
         <RIGHT_BRACKET>
         { exp = new NewArray(arraySize); }
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      ) | (
         <NEW>
         { final Identifier className; }
         className = Identifier()
         <LEFT_PAREN> <RIGHT_PAREN>
         { exp = new NewObject(className); }
         [ LOOKAHEAD(1) exp = ExpRight(exp) ]
      )
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return exp; }
}

Expression ExpRight(final Expression left) : {
   Token t = null;
   final Expression right;
   Expression exp = null;
}{
   try {
      (
         t = <AND>
         right = Exp()
         { exp = new syntax.And(t.beginLine, t.beginColumn, left, right); }
      ) | (
         t = <LT>
         right = Exp()
         { exp = new syntax.LessThan(left, right); }
      ) | (
         t = <PLUS>
         right = Exp()
         { exp = new syntax.Plus(t.beginLine, t.beginColumn, left, right); }
      ) | (
         t = <MINUS>
         right = Exp()
         { exp = new syntax.Minus(t.beginLine, t.beginColumn, left, right); }
      ) | (
         t = <TIMES>
         right = Exp()
         { exp = new syntax.Times(left, right); }
      ) | (
         <LEFT_BRACKET>
            { final Expression index; }
            index = Exp()
         <RIGHT_BRACKET>
         { exp = new syntax.ArrayLookup(left, index); }
      ) | LOOKAHEAD(2) (
         <DOT>
         <LENGTH>
         { exp = new syntax.ArrayLength(left); }
      ) | (
         t = <DOT>
         { final Identifier methodId; }
         methodId = Identifier()

         <LEFT_PAREN>
            { final List<Expression> args = new LinkedList<Expression>(); }
            [
               { final List<Expression> expList; }
               expList = ExpList()
               { args.addAll(expList); }
            ]
         <RIGHT_PAREN>

         { exp = new syntax.Call(t.beginLine, t.beginColumn, left, methodId, args); }
      )
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }

   { return exp; }
}

// adapted from http://javacc.java.net/doc/errorrecovery.html
JAVACODE
void recoverError(final int... kinds) {
   syntaxErrors.add(generateParseException());

   Token t;
   int curr = 0;
   do {
      t = getNextToken();
      if (t.kind == kinds[curr]) {
         curr++;
      }
   } while (t.kind != EOF && curr < kinds.length);
}
