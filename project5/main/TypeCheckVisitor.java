package main;

import syntax.*;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

public class TypeCheckVisitor implements SyntaxTreeVisitor<Type> {
   private static final Type ERROR = new Type() {
      @Override
      public <T> T accept(SyntaxTreeVisitor<T> v) {
         throw new RuntimeException("compiler error: invalid type referenced");
      }
   };

   private static final Type NO_TYPE = new Type() {
      @Override
      public <T> T accept(SyntaxTreeVisitor<T> v) {
         throw new RuntimeException("compiler error: statement used as expression");
      }
   };

   private final Map<String, ClassSymbolTable> classes;
   public final List<SemanticError> errors = new LinkedList<SemanticError>();

   private ClassSymbolTable currentClass = null;
   private MethodSymbolTable currentMethod = null;

   public TypeCheckVisitor(final Map<String, ClassSymbolTable> theClasses) {
      classes = theClasses;
   }

   private void err(final int line, final int column, final String msg) {
      errors.add(new SemanticError(line, column, msg));
   }

   private void err(final AST token, final String msg) {
      err(token.lineNumber, token.columnNumber, msg);
   }

   private ClassSymbolTable getSuper(final ClassSymbolTable clas) {
      assert clas != null;
      return classes.get(clas.superclassId);
   }

   private Type lookupField(final ClassSymbolTable clas, final String id) {
      assert clas != null;

      if (clas.fields.containsKey(id)) {
         return clas.fields.get(id);
      }
      
      final String superclass = clas.superclassId;
      if (superclass != null) {
         assert classes.containsKey(superclass);
         return lookupField(classes.get(superclass), id);
      }

      return ERROR;
   }

   private Type lookupVar(final String id) {
      if (currentMethod != null && currentMethod.localVars.containsKey(id)) {
         return currentMethod.localVars.get(id);
      }

      assert currentClass != null;
      return lookupField(currentClass, id);
   }

   private boolean hasMethod(final String methodId, final ClassSymbolTable clas) {
      return clas == null ?
            false :
            clas.methods.containsKey(methodId) || hasMethod(methodId, getSuper(clas));
   }

   private MethodSymbolTable lookupMethod(final String id, final ClassSymbolTable clas) {
      if (clas == null) {
         return null;
      }

      return clas.methods.containsKey(id) ?
            clas.methods.get(id) :
            lookupMethod(id, getSuper(clas));
   }

   private MethodSymbolTable lookupMethod(final String id) {
      return lookupMethod(id, currentClass);
   }

   // returns true if left is equal to or less specific than right
   private boolean covariantEq(final Type left, final Type right) {
      if ((left instanceof IdentifierType) && (right instanceof IdentifierType)) {
         if (left.toString().equals(right.toString())) {
            return true;
         } else {
            final ClassSymbolTable clas = classes.get(right.toString());

            if (clas.superclassId != null) {
               final Type superRight = new IdentifierType(right.lineNumber,
                     right.columnNumber, classes.get(right.toString()).superclassId);
               return covariantEq(left, superRight);
            } else {
               return false;
            }
         }
      } else {
         return left == right;
      }
   }


   // Declarations

   public Type visit(Program n) {
      // Main class
      n.m.accept(this);

      // Other classes
      for (final ClassDecl c : n.cl)
         c.accept(this);

      return null;
   }

   public Type visit(MainClass n) {
      // Class identifier
      final String classId = n.i1.s;

      currentClass = classes.get(classId);

      currentMethod = currentClass.methods.get("main");

      // Main method body
      n.s.accept(this);

      return null;
   }

   public Type visit(SimpleClassDecl n) {
      // Class identifier
      final String classId = n.i.s;

      currentClass = classes.get(classId);

      // Fields
      for (final VarDecl v : n.vl)
         v.accept(this);

      // Methods
      for (final MethodDecl m : n.ml)
         m.accept(this);


      return null;
   }

   public Type visit(ExtendingClassDecl n) {
      // Class identifier
      final String classId = n.i.s;

      // Superclass identifier
      final String superclassId = n.j.s;

      if (!classes.containsKey(superclassId)) {
         err(n.j, superclassId + " is not a valid class");
      }

      currentClass = classes.get(classId);

      // Fields
      for (final VarDecl v : n.vl)
         v.accept(this);

      // Methods
      for (final MethodDecl m : n.ml)
         m.accept(this);

      return null;
   }

   public Type visit(MethodDecl n) {
      final Type methodReturnType = n.t;
      final String methodId = n.i.s;

      currentMethod = currentClass.methods.get(methodId);

      // Formal parameters
      for (final Formal f : n.fl)
         f.accept(this);

      // Local variables
      for (final VarDecl v : n.vl)
         v.accept(this);

      // Method body
      for (final Statement s : n.sl)
         s.accept(this);

      final Type returnValType = n.e.accept(this);
      if (!covariantEq(returnValType, methodReturnType)) {
         err(n.e, "mismatched types in return statement: expected " + methodReturnType
               + ", found " + returnValType);
      }

      return null;
   }

   public Type visit(VarDecl n) {
      return null;
   }

   public Type visit(Formal n) {
      return null;
   }


   // Types

   public Type visit(IntArrayType n) {
      return null;
   }

   public Type visit(BooleanType n) {
      return null;
   }

   public Type visit(IntegerType n) {
      return null;
   }

   public Type visit(IdentifierType n) {
      return null;
   }


   // Statements

   public Type visit(Block n) {
      for (final Statement s : n.sl)
         s.accept(this);
      
      return null;
   }

   public Type visit(If n) {
      final Type clauseType = n.e.accept(this);
      if (clauseType != Type.THE_BOOLEAN_TYPE) {
         err(n.e, "if clause should be boolean; found " + clauseType);
      }

      n.s1.accept(this);
      n.s2.accept(this);

      return null;
   }

   public Type visit(While n) {
      final Type clauseType = n.e.accept(this);
      if (clauseType != Type.THE_BOOLEAN_TYPE) {
         err(n.e, "while clause should be boolean; found " + clauseType);
      }

      n.s.accept(this);

      return null;
   }

   public Type visit(Print n) {
      final Type type = n.e.accept(this);
      if (type != Type.THE_INTEGER_TYPE) {
         err(n.e, "argument to System.out.println should be integer; found " + type);
      }
      return null;
   }

   public Type visit(Assign n) {
      final String leftId = n.i.s;

      final Type left = lookupVar(leftId);
      if (left == ERROR) {
         err(n.i, leftId + " is not a valid identifier");
         return ERROR;
      }

      final Type right = n.e.accept(this);
      if (right == ERROR) {
         // null type indicates an error in the expression;
         // ignore the error so it doesn't bubble up
         return ERROR;
      }

      if (!covariantEq(left, right)) {
         err(n, "mismatched types in assignment: left side is " + left
               + ", right side is " + right);
      }

      return null;
   }

   public Type visit(ArrayAssign n) {
      final String arrId = n.i.s;

      final Type arrType = lookupVar(arrId);
      if (arrType != Type.THE_INT_ARRAY_TYPE) {
         err(n.i, arrId + " is not a valid array; expected int[], found " + arrType);
      }

      final Type indexType = n.e1.accept(this);
      if (indexType != Type.THE_INTEGER_TYPE) {
         err(n.e1, "array index must be an integer");
      }

      final Type valType = n.e2.accept(this);
      if (valType != Type.THE_INTEGER_TYPE) {
         err(n.e2, "mismatched types in assignment: left side is int, "
               + "right side is " + valType.toString());
      }

      return null;
   }


   // Expressions

   public Type visit(And n) {
      final Type left = n.e1.accept(this);
      final Type right = n.e2.accept(this);

      if (left != Type.THE_BOOLEAN_TYPE) {
         err(n.e1, "left argument to && should be boolean; found " + left);
      }

      if (right != Type.THE_BOOLEAN_TYPE) {
         err(n.e2, "right argument to && should be boolean; found " + right);
      }

      return Type.THE_BOOLEAN_TYPE;
   }

   public Type visit(LessThan n) {
      final Type left = n.e1.accept(this);
      final Type right = n.e2.accept(this);

      if (left != Type.THE_INTEGER_TYPE) {
         err(n.e1, "left argument to < should be int; found " + left);
      }

      if (right != Type.THE_INTEGER_TYPE) {
         err(n.e2, "right argument to < should be int; found " + right);
      }

      return Type.THE_BOOLEAN_TYPE;
   }

   public Type visit(Plus n) {
      final Type left = n.e1.accept(this);
      final Type right = n.e2.accept(this);

      if (left != Type.THE_INTEGER_TYPE) {
         err(n.e1, "left argument to + should be int; found " + left);
      }

      if (right != Type.THE_INTEGER_TYPE) {
         err(n.e2, "right argument to + should be int; found " + right);
      }

      return Type.THE_INTEGER_TYPE;
   }

   public Type visit(Minus n) {
      final Type left = n.e1.accept(this);
      final Type right = n.e2.accept(this);

      if (left != Type.THE_INTEGER_TYPE) {
         err(n.e1, "left argument to - should be int; found " + left);
      }

      if (right != Type.THE_INTEGER_TYPE) {
         err(n.e2, "right argument to - should be int; found " + right);
      }

      return Type.THE_INTEGER_TYPE;
   }

   public Type visit(Times n) {
      final Type left = n.e1.accept(this);
      final Type right = n.e2.accept(this);

      if (left != Type.THE_INTEGER_TYPE) {
         err(n.e1, "left argument to * should be int; found " + left);
      }

      if (right != Type.THE_INTEGER_TYPE) {
         err(n.e2, "right argument to * should be int; found " + right);
      }

      return Type.THE_INTEGER_TYPE;
   }

   public Type visit(Not n) {
      final Type type = n.e.accept(this);

      if (type != Type.THE_BOOLEAN_TYPE) {
         err(n.e, "argument to ! should be boolean; found " + type);
      }

      return Type.THE_BOOLEAN_TYPE;
   }

   public Type visit(ArrayLookup n) {
      final Type arrayType = n.e1.accept(this);
      final Type indexType = n.e2.accept(this);

      if (arrayType != Type.THE_INT_ARRAY_TYPE) {
         err(n.e1, "left expression in array lookup should be int[]; found " + arrayType);
      }

      if (indexType != Type.THE_INTEGER_TYPE) {
         err(n.e2, "index expression in array lookup should be int; found " + indexType);
      }

      return Type.THE_INTEGER_TYPE;
   }

   public Type visit(ArrayLength n) {
      final Type arrayType = n.e.accept(this);

      if (arrayType != Type.THE_INT_ARRAY_TYPE) {
         err(n.e, "can't get length of " + arrayType + "; expected int[]");
      }

      return Type.THE_INTEGER_TYPE;
   }

   public Type visit(Call n) {
      final String methodName = n.i.s;

      final Type objType = n.e.accept(this);
      if (objType == ERROR) {
         err(n.e, "expression doesn't evaluate to an object");
         return ERROR;
      } else if (!(objType instanceof IdentifierType)) {
         err(n.e, "can't call method " + methodName + " on primitive value "
               + "of type " + objType);

         return ERROR;
      }

      final ClassSymbolTable clas = classes.get(objType.toString());
      final MethodSymbolTable method = lookupMethod(methodName, clas);
      if (method == null) {
         err(n.i, "method " + methodName + " does not exist in class "
               + clas.classId);
         return null;
      }

      final List<Expression> args = n.el;
      final List<Type> params = method.formalParams;

      if (args.size() != params.size()) {
         err(n.e, "wrong number of arguments to " + methodName + ": found "
               + args.size() + ", expected " + params.size());
      } else {
         for (int i = 0; i < args.size(); i++) {
            final Expression e = args.get(i);
            final Type argType = e.accept(this);
            final Type paramType = params.get(i);

            if (!covariantEq(paramType, argType)) {
               err(e, "mismatched argument type: found " + argType + ", expected "
                     + paramType);
            }
         }
      }

      return method.returnType;
   }

   public Type visit(IntegerLiteral n) {
      return Type.THE_INTEGER_TYPE;
   }

   public Type visit(True n) {
      return Type.THE_BOOLEAN_TYPE;
   }

   public Type visit(False n) {
      return Type.THE_BOOLEAN_TYPE;
   }

   public Type visit(This n) {
      return new IdentifierType(n.lineNumber, n.columnNumber, currentClass.classId);
   }

   public Type visit(NewArray n) {
      final Type lengthType = n.e.accept(this);
      if (lengthType != Type.THE_INTEGER_TYPE) {
         err(n.e, "array length argument must be int; found " + lengthType);
      }

      return Type.THE_INT_ARRAY_TYPE;
   }

   public Type visit(NewObject n) {
      if (!classes.containsKey(n.i.s)) {
         err(n.i, n.i.s + " is not a valid class name");
         return ERROR;
      }

      final ClassSymbolTable clas = classes.get(n.i.s);
      return new IdentifierType(n.i.lineNumber, n.i.columnNumber, clas.classId);
   }

   public Type visit(IdentifierExp n) {
      final Type t = lookupVar(n.s);

      if (t == ERROR) {
         err(n, n.s + " is not a valid identifier");
      }

      return t;
   }

   public Type visit(Identifier n) {
      final Type t = lookupVar(n.s);

      if (t == ERROR) {
         err(n, n.s + " is not a valid identifier");
      }

      return t;
   }
}
