package main;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import syntax.Type;

public class MethodSymbolTable {
   public final List<Type> formalParams = new LinkedList<Type>();
   public final Map<String, Type> localVars = new HashMap<String, Type>();
   public final String methodId;
   public final Type returnType;

   public MethodSymbolTable(final String theMethodId, final Type theReturnType) {
      methodId = theMethodId;
      returnType = theReturnType;
   }
}
