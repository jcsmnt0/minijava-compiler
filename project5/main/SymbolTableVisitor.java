package main;

import syntax.*;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

public class SymbolTableVisitor implements SyntaxTreeVisitor<Void> {
   public final List<SemanticError> errors = new LinkedList<SemanticError>();

   public final Map<String, ClassSymbolTable> table
         = new HashMap<String, ClassSymbolTable>();

   private ClassSymbolTable currentClass = null;
   private MethodSymbolTable currentMethod = null;

   private <T> boolean bind(final String key, final T val, final Map<String, T> map) {
      if (!map.containsKey(key)) {
         map.put(key, val);
         return true;
      } else {
         return false;
      }
   }

   public Void visit(Program n) {
      // Main class
      n.m.accept(this);

      // Other classes
      for (final ClassDecl c : n.cl)
         c.accept(this);

      return null;
   }

   public Void visit(MainClass n) {
      // Class identifier
      final String classId = n.i1.s;

      currentClass = new ClassSymbolTable(classId);

      currentMethod = new MethodSymbolTable("main", null);
      currentClass.methods.put("main", currentMethod);

      // Command-line args parameter is ignored, since it's unusable
      // as per the spec

      // Main method body
      n.s.accept(this);

      table.put(classId, currentClass);

      return null;
   }

   public Void visit(SimpleClassDecl n) {
      // Class identifier
      final String classId = n.i.s;

      currentClass = new ClassSymbolTable(classId);
      currentMethod = null;

      // Fields
      for (final VarDecl v : n.vl)
         v.accept(this);

      // Methods
      for (final MethodDecl m : n.ml)
         m.accept(this);

      table.put(classId, currentClass);

      return null;
   }

   public Void visit(ExtendingClassDecl n) {
      // Class identifier
      final String classId = n.i.s;

      // Superclass identifier
      final String superclassId = n.j.s;

      currentClass = new ClassSymbolTable(classId, superclassId);
      currentMethod = null;

      // Fields
      for (final VarDecl v : n.vl)
         v.accept(this);

      // Methods
      for (final MethodDecl m : n.ml)
         m.accept(this);

      table.put(classId, currentClass);

      return null;
   }

   public Void visit(MethodDecl n) {
      final Type returnType = n.t;
      final String methodId = n.i.s;

      currentMethod = new MethodSymbolTable(methodId, returnType);

      if (!(bind(methodId, currentMethod, currentClass.methods))) {
         errors.add(new SemanticError(n.i.lineNumber, n.i.columnNumber,
               methodId + " is already defined in " + currentClass.classId));
      }

      // Formal parameters
      for (final Formal f : n.fl)
         f.accept(this);

      // Local variables
      for (final VarDecl v : n.vl)
         v.accept(this);

      // Method body
      for (final Statement s : n.sl)
         s.accept(this);

      // The return statement can't introduce bindings, so ignore it

      return null;
   }

   public Void visit(VarDecl n) {
      final Type type = n.t;
      final String varId = n.i.s;

      if (currentMethod == null) {
         if (!bind(varId, type, currentClass.fields)) {
            errors.add(new SemanticError(n.i.lineNumber, n.i.columnNumber,
                     varId + " is already defined in " + currentClass.classId));
         }
      } else {
         if (!bind(varId, type, currentMethod.localVars)) {
            errors.add(new SemanticError(n.i.lineNumber, n.i.columnNumber,
                  varId + " is already defined in " + currentMethod.methodId));
         }
      }

      return null;
   }

   public Void visit(Formal n) {
      final Type type = n.t;
      final String formalId = n.i.s;

      if (!bind(formalId, type, currentMethod.localVars)) {
         errors.add(new SemanticError(n.i.lineNumber, n.i.columnNumber,
                  formalId + " is already defined in " + currentMethod.methodId));
      }

      currentMethod.formalParams.add(type);

      return null;
   }

   public Void visit(IntArrayType n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(BooleanType n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(IntegerType n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(IdentifierType n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Block n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(If n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(While n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Print n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Assign n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(ArrayAssign n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(And n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(LessThan n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Plus n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Minus n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Times n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(ArrayLookup n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(ArrayLength n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Call n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(IntegerLiteral n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(True n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(False n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(IdentifierExp n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(This n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(NewArray n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(NewObject n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Not n) {
      // can't introduce new bindings
      return null;
   }

   public Void visit(Identifier n) {
      // can't introduce new bindings
      return null;
   }
}
