package main;

import java.util.Map;
import java.util.HashMap;
import syntax.Type;

public class ClassSymbolTable {
   public final Map<String, Type> fields = new HashMap<String, Type>();
   public final Map<String, MethodSymbolTable> methods
         = new HashMap<String, MethodSymbolTable>();
   public final String classId, superclassId;

   public ClassSymbolTable(final String theClassId, final String theSuperclassId) {
      classId = theClassId;
      superclassId = theSuperclassId;
   }

   public ClassSymbolTable(final String theClassId) {
      this(theClassId, null);
   }
}
