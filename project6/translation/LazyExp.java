package translation;

import tree.*;

class LazyExp implements LazyIRTree {
   final Exp exp;

   LazyExp(final Exp e) {
      exp = e;
   }

   public Exp asExp() {
      return exp;
   }

   public Stm asStm() {
      return new EVAL(exp);
   }

   public Stm asCond(final NameOfLabel t, final NameOfLabel f) {
      throw new UnsupportedOperationException();
   }
}
