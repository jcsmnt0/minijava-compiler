package translation;

import tree.*;

public interface Frame {
   public Exp fp();
   public int wordSize();
   public Exp getAddr(final String var);
   public Exp getThis();
   public Exp returnAddr();
   public LazyIRTree procEntryExit1(final Stm body);
}
