package translation;

import tree.NameOfLabel;
import tree.Exp;
import tree.Stm;

public interface LazyIRTree {
   Exp asExp();
   Stm asStm();
   Stm asCond(final NameOfLabel t, final NameOfLabel f);
}
