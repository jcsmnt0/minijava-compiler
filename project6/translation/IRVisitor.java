package translation;

import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

import semantics.*;
import syntax.*;
import tree.*;

public class IRVisitor implements SyntaxTreeVisitor<LazyIRTree> {
   private static final String METHOD_SEPARATOR = "$";
   private static final Exp FALSE = new CONST(0), TRUE = new CONST(1);

   public final Map<String, Fragment> fragments = new HashMap<String, Fragment>();

   private final Map<String, Integer> classSizes;
   private final Map<String, Frame> frames;

   private String currentClass = null;
   private Frame currentFrame = null;

   public IRVisitor(final Map<String, Integer> theClassSizes,
         final Map<String, Frame> theFrames) {
      classSizes = theClassSizes;
      frames = theFrames;
   }

   public static String getMethodName(final String classId, final String methodId) {
      return String.format("%s%s%s", classId, METHOD_SEPARATOR, methodId);
   }
   
   private String getMethodName(final String methodId) {
      assert currentClass != null;
      return getMethodName(currentClass, methodId);
   }

   private Exp subscript(final Exp arr, final Exp index) {
      // +1 because [arr+0] is arr.length
      return new BINOP(BINOP.PLUS, arr, new BINOP(BINOP.PLUS, index, TRUE));
   }

   private static ExpList toExpList(final List<Exp> exps) {
      if (exps.size() == 0) {
         return null;
      } else {
         ExpList lst = new ExpList(exps.get(exps.size()-1), null);
         for (int i = exps.size()-2; i >= 0; i--) {
            lst = new ExpList(exps.get(i), lst);
         }
         return lst;
      }
   }

   
   // Declarations

   public LazyIRTree visit (Program n) {
      // Main class
      n.m.accept(this);

      // Other classes
      for (final ClassDecl c : n.cl)
         c.accept(this);

      return null;
   }

   public LazyIRTree visit (MainClass n) {
      currentClass = n.i1.s;

      // Main method body
      n.s.accept(this);

      return null;
   }

   public LazyIRTree visit (SimpleClassDecl n) {
      currentClass = n.i.s;

      // Methods
      for (final MethodDecl m : n.ml)
         m.accept(this);

      return null;
   }

   public LazyIRTree visit (ExtendingClassDecl n) {
      currentClass = n.i.s;

      // Methods
      for (final MethodDecl m : n.ml)
         m.accept(this);

      return null;
   }

   public LazyIRTree visit (MethodDecl n) {
      final String id = getMethodName(n.i.s);
      currentFrame = frames.get(id);

      final Exp returnExp = n.e.accept(this).asExp();

      final Stm body;
      if (n.sl.size() == 0) {
         return currentFrame.procEntryExit1(new MOVE(currentFrame.returnAddr(), returnExp));
      } else if (n.sl.size() == 1) {
         body = n.sl.get(0).accept(this).asStm();
      } else {
         final Stm[] stms = new Stm[n.sl.size()];
         for (int i = 0; i < n.sl.size(); i++)
            stms[i] = n.sl.get(i).accept(this).asStm();

         body = SEQ.fromList(stms);
      }

      final LazyIRTree code = currentFrame.procEntryExit1(SEQ.fromList(
            body,
            new MOVE(currentFrame.returnAddr(), returnExp)));

      fragments.put(id, new Fragment(currentFrame, code));

      return null;
   }

   public LazyIRTree visit (VarDecl n) {
      return null;
   }

   public LazyIRTree visit (Formal n) {
      return null;
   }


   // Types

   public LazyIRTree visit (IntArrayType n) {
      return null;
   }

   public LazyIRTree visit (BooleanType n) {
      return null;
   }

   public LazyIRTree visit (IntegerType n) {
      return null;
   }

   public LazyIRTree visit (IdentifierType n) {
      return null;
   }


   // Statements

   public LazyIRTree visit (Block n) {
      if (n.sl.size() == 1)
         return new LazyStm(n.sl.get(0).accept(this).asStm());

      final Stm[] statements = new Stm[n.sl.size()];
      for (int i = 0; i < n.sl.size(); i++) {
         statements[i] = n.sl.get(i).accept(this).asStm();
      }

      return new LazyStm(SEQ.fromList(statements));
   }

   public LazyIRTree visit (If n) {
      final Exp cond = n.e.accept(this).asExp();
      final Stm ifTrue = n.s1.accept(this).asStm();
      final Stm ifFalse = n.s2.accept(this).asStm();

      final NameOfLabel t = NameOfLabel.generateLabel("t");
      final NameOfLabel f = NameOfLabel.generateLabel("f");
      final NameOfLabel end = NameOfLabel.generateLabel("end");

      return new LazyStm(SEQ.fromList(
            new CJUMP(CJUMP.EQ, cond, TRUE, t, f),
            new LABEL(t), ifTrue, new JUMP(end),
            new LABEL(f), ifFalse, new JUMP(end),
            new LABEL(end)));
   }

   public LazyIRTree visit (While n) {
      final Exp cond = n.e.accept(this).asExp();
      final Stm body = n.e.accept(this).asStm();

      final NameOfLabel test = NameOfLabel.generateLabel("test");
      final NameOfLabel start = NameOfLabel.generateLabel("body");
      final NameOfLabel end = NameOfLabel.generateLabel("end");

      return new LazyStm(SEQ.fromList(
            new LABEL(test), new CJUMP(CJUMP.EQ, cond, TRUE, start, end),
            new LABEL(start), body, new JUMP(test),
            new LABEL(end)));
   }

   public LazyIRTree visit (Print n) {
      final ExpList arg = ExpList.toExpList(n.e.accept(this).asExp());
      return new LazyExp(new CALL(new NAME(new NameOfLabel("printInt")), arg));
   }

   public LazyIRTree visit (Assign n) {
      final Exp var = currentFrame.getAddr(n.i.s);
      final Exp val = n.e.accept(this).asExp();
      return new LazyStm(new MOVE(var, val));
   }

   public LazyIRTree visit (ArrayAssign n) {
      final Exp arr = new MEM(currentFrame.getAddr(n.i.s));
      final Exp index = n.e1.accept(this).asExp();
      final Exp val = n.e2.accept(this).asExp();

      return new LazyStm(new MOVE(subscript(arr, index), val));
   }


   // Expressions

   public LazyIRTree visit (And n) {
      final Exp left = n.e1.accept(this).asExp();
      final Exp right = n.e2.accept(this).asExp();

      return new LazyExp(new BINOP(BINOP.AND, left, right));
   }

   public LazyIRTree visit (LessThan n) {
      final Exp left = n.e1.accept(this).asExp();
      final Exp right = n.e2.accept(this).asExp();

      final NameOfLabel t = NameOfLabel.generateLabel("t");
      final NameOfLabel f = NameOfLabel.generateLabel("f");
      final NameOfTemp x = NameOfTemp.generateTemp("x");

      return new LazyExp(new ESEQ(
            SEQ.fromList(
                  new MOVE(new TEMP(x), TRUE),
                  new CJUMP(CJUMP.LT, left, right, t, f),
                  new LABEL(f),
                  new MOVE(new TEMP(x), FALSE),
                  new LABEL(t)
            ),
            new TEMP(x)));

   }

   public LazyIRTree visit (Plus n) {
      return new LazyExp(new BINOP(BINOP.PLUS, n.e1.accept(this).asExp(),
               n.e2.accept(this).asExp()));
   }

   public LazyIRTree visit (Minus n) {
      return new LazyExp(new BINOP(BINOP.MINUS, n.e1.accept(this).asExp(),
               n.e2.accept(this).asExp()));
   }

   public LazyIRTree visit (Times n) {
      return new LazyExp(new BINOP(BINOP.MUL, n.e1.accept(this).asExp(),
               n.e2.accept(this).asExp()));
   }

   public LazyIRTree visit (Not n) {
      return new LazyExp(new BINOP(BINOP.XOR, n.e.accept(this).asExp(), TRUE));
   }

   public LazyIRTree visit (ArrayLookup n) {
      final Exp arr = n.e1.accept(this).asExp();
      final Exp index = n.e2.accept(this).asExp();
      return new LazyExp(subscript(arr, index));
   }

   public LazyIRTree visit (ArrayLength n) {
      return new LazyExp(new MEM(n.e.accept(this).asExp()));
   }

   public LazyIRTree visit (Call n) {
      final Exp obj = n.e.accept(this).asExp();
      final Exp method = new NAME(new NameOfLabel(getMethodName(n.i.s)));

      final List<Exp> args = new LinkedList<Exp>();
      args.add(obj);
      for (final Expression arg : n.el)
         args.add(arg.accept(this).asExp());

      return new LazyExp(new CALL(method, toExpList(args)));
   }

   public LazyIRTree visit (IntegerLiteral n) {
      return new LazyExp(new CONST(n.i));
   }

   public LazyIRTree visit (True n) {
      return new LazyExp(TRUE);
   }

   public LazyIRTree visit (False n) {
      return new LazyExp(FALSE);
   }

   public LazyIRTree visit (IdentifierExp n) {
      final String id = n.s;
      return new LazyExp(new MEM(currentFrame.getAddr(id)));
   }

   public LazyIRTree visit (This n) {
      return new LazyExp(currentFrame.getThis());
   }

   public LazyIRTree visit (NewArray n) {
      final ExpList args = ExpList.toExpList(n.e.accept(this).asExp());
      return new LazyExp(new CALL(new NAME("initArray"), args));
   }

   public LazyIRTree visit (NewObject n) {
      final String classId = n.i.s;

      assert classSizes.containsKey(classId);
      final ExpList args = ExpList.toExpList(new CONST(classSizes.get(classId)));
      return new LazyExp(new CALL(new NAME("initObject"), args));
   }

   public LazyIRTree visit (Identifier n) {
      return null;
   }
}
