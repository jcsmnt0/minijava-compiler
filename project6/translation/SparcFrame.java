package translation;

import java.util.List;
import java.util.LinkedList;

import semantics.*;
import tree.*;

public class SparcFrame implements Frame {
   private final List<String> locals, params, fields;

   public SparcFrame(final MethodSymbolTable table) {
      fields = new LinkedList<String>(table.clas.fields.keySet());
      locals = new LinkedList<String>(table.localVars.keySet());
      params = table.formalParamNames;
   }

   @Override
   public Exp fp() {
      return new TEMP(new NameOfTemp("%fp"));
   }

   @Override
   public Exp returnAddr() {
      return new TEMP(new NameOfTemp("%o0"));
   }

   @Override
   public int wordSize() {
      return 4;
   }

   @Override
   public Exp getThis() {
      // 'this' is always at [fp+0]
      return new MEM(fp());
   }

   @Override
   public Exp getAddr(final String var) {
      if (locals.contains(var)) {
         // +1 because 'this' is at [fp+0]
         final int index = locals.indexOf(var);
         return new BINOP(BINOP.PLUS, fp(), new CONST((index+1)*wordSize()));
      } else {
         // implicit 'this.'
         assert fields.contains(var);
         final int index = fields.indexOf(var);
         return new BINOP(BINOP.PLUS, getThis(), new CONST(index*wordSize()));
      }
   }

   private Exp arg(final int i) {
      assert i < 6;
      return new TEMP(new NameOfTemp("%i" + i));
   }

   @Override
   public LazyIRTree procEntryExit1(final Stm body) {
      final Stm saveThis = new MOVE(fp(), arg(0));

      if (params.size() < 1) {
         return new LazyStm(new SEQ(saveThis, body));
      } else {
         final Stm[] prologue = new Stm[params.size()+1];
         prologue[0] = saveThis;

         for (int i = 1; i < params.size()+1; i++) {
            final String param = params.get(i-1);
            prologue[i] = new MOVE(getAddr(param), arg(i));
         }
         return new LazyStm(SEQ.fromList(SEQ.fromList(prologue), body));
      }
   }
}
