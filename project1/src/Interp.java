import javax.management.BadBinaryOpValueExpException;
import javax.swing.event.TreeExpansionListener;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

class Interp {
    private static final HashMap<String, Integer> vars
            = new HashMap<String, Integer>();

    public static void interp(final Stm s) {
        if (s instanceof CompoundStm) {
            final CompoundStm cs = (CompoundStm) s;
            interp(cs.stm1);
            interp(cs.stm2);
        } else if (s instanceof AssignStm) {
            final AssignStm as = (AssignStm) s;
            vars.put(as.id, eval(as.exp));
        } else if (s instanceof PrintStm) {
            final PrintStm ps = (PrintStm) s;
            for (final int i : evalList(ps.exps)) {
                System.out.print(i + " ");
            }
            System.out.println();
        } else {
            throw new RuntimeException("invalid statement type");
        }
    }

    private static int eval(final Exp e) {
        if (e instanceof IdExp) {
            final IdExp ie = (IdExp) e;
            final String id = ie.id;

            if (!vars.containsKey(id)) {
                throw new RuntimeException(id + " is not a valid identifier");
            }

            return vars.get(id);
        } else if (e instanceof NumExp) {
            final NumExp ne = (NumExp) e;
            return ne.num;
        } else if (e instanceof OpExp) {
            final OpExp oe = (OpExp) e;
            final int x = eval(oe.left);
            final int y = eval(oe.right);
            switch (oe.oper) {
                case OpExp.Plus:
                    return x + y;
                case OpExp.Minus:
                    return x - y;
                case OpExp.Times:
                    return x * y;
                case OpExp.Div:
                    return x / y;
                default:
                    throw new RuntimeException("invalid operator type");
            }
        } else if (e instanceof EseqExp) {
            final EseqExp ee = (EseqExp) e;
            interp(ee.stm);
            return eval(ee.exp);
        } else {
            throw new RuntimeException("invalid expression type");
        }
    }

    private static List<Integer> evalList(final ExpList l) {
        if (l instanceof PairExpList) {
            final PairExpList pel = (PairExpList) l;
            final int head = eval(pel.head);
            final List<Integer> tail = new LinkedList<Integer>(evalList(pel.tail));
            tail.add(0, head);
            return tail;
        } else if (l instanceof LastExpList) {
            final LastExpList lel = (LastExpList) l;
            final int lastVal = eval(lel.head);
            return Arrays.asList(lastVal);
        } else {
            throw new RuntimeException("invalid list type");
        }
    }

    private static int listSize(final ExpList l) {
        if (l instanceof PairExpList) {
            final PairExpList pel = (PairExpList) l;
            return 1 + listSize(pel.tail);
        } else if (l instanceof LastExpList) {
            return 1;
        } else {
            throw new RuntimeException("invalid list type");
        }
    }

    public static int maxargs(final Stm s) {
        if (s instanceof CompoundStm) {
            final CompoundStm cs = (CompoundStm) s;
            return Math.max(maxargs(cs.stm1), maxargs(cs.stm2));
        } else if (s instanceof AssignStm) {
            final AssignStm as = (AssignStm) s;
            return maxargs(as.exp);
        } else if (s instanceof PrintStm) {
            final PrintStm ps = (PrintStm) s;
            final int args = listSize(ps.exps);
            return Math.max(args, maxargs(ps.exps));
        } else {
            throw new RuntimeException("invalid statement type");
        }
    }

    private static int maxargs(final Exp e) {
        if (e instanceof OpExp) {
            final OpExp oe = (OpExp) e;
            return Math.max(maxargs(oe.left), maxargs(oe.right));
        } else if (e instanceof EseqExp) {
            final EseqExp ee = (EseqExp) e;
            return Math.max(maxargs(ee.stm), maxargs(ee.exp));
        } else {
            return 0;
        }
    }

    private static int maxargs(final ExpList l) {
        if (l instanceof PairExpList) {
            final PairExpList pel = (PairExpList) l;
            return Math.max(maxargs(pel.head), maxargs(pel.tail));
        } else if (l instanceof LastExpList) {
            final LastExpList lel = (LastExpList) l;
            return maxargs(lel.head);
        } else {
            throw new RuntimeException("invalid list type");
        }
    }

    public static void main(final Stm s)
            throws IOException {
        System.out.println(maxargs(s));
        interp(s);
    }

    public static void main(final String[] args)
            throws IOException {
	    main(Example.a_program);
        main(Example.another_program);
    }
}
