import javax.management.BadBinaryOpValueExpException;

public class Example {
    // Stm :: a:=5+3; b := (print (a, a-1), 10*a); print(b)
    // Out ::
    //        8 7
    //        80
    static Stm a_program =
            new CompoundStm(new AssignStm("a",new OpExp(new NumExp(5), OpExp.Plus, new NumExp(3))),
                    new CompoundStm(new AssignStm("b",
                            new EseqExp(new PrintStm(new PairExpList(new IdExp("a"),
                                    new LastExpList(new OpExp(new IdExp("a"), OpExp.Minus, new NumExp(1))))),
                                    new OpExp(new NumExp(10), OpExp.Times, new IdExp("a")))),
                            new PrintStm(new LastExpList(new IdExp("b")))));

    // Stm :: print ((x:=3, x), (y:=4, y), (z:=x*y, z))
    // Out :: 3 4 12
    static Stm another_program =
        new PrintStm(
            new PairExpList(
                new EseqExp(
                    new AssignStm("x", new NumExp(3)),
                    new IdExp("x")
                ),
                new PairExpList(
                    new EseqExp(
                        new AssignStm("y", new NumExp(4)),
                        new IdExp("y")
                    ),
                    new LastExpList(
                        new EseqExp(
                            new AssignStm(
                                "z",
                                new OpExp(
                                    new IdExp("x"),
                                    OpExp.Times,
                                    new IdExp("y")
                                )
                            ),
                            new IdExp("z")
                        )
                    )
                )
            )
        );
}
