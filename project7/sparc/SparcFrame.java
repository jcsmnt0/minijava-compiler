package sparc;

import java.util.List;
import java.util.LinkedList;

import translation.*;
import semantics.*;
import tree.*;

public class SparcFrame implements Frame {
   private final String methodName;
   private final List<String> locals, params, fields;

   public static final String
      SAVE_LABEL = "$$save",
      EPILOGUE_LABEL = "$epilogueStart",
      DUMMY_JUMP = "$$nowhere",
      MAIN_START = "main",
      MAIN_END = "$$mainEnd";

   public SparcFrame(final MethodSymbolTable table) {
      fields = new LinkedList<String>(table.clas.fields.keySet());
      locals = new LinkedList<String>(table.localVars.keySet());
      params = table.formalParamNames;
      methodName = table.clas.classId + IRVisitor.METHOD_SEPARATOR + table.methodId;
   }

   @Override
   public Exp fp() {
      return new TEMP("%fp");
   }

   @Override
   public Exp returnAddr() {
      return new TEMP("%i0");
   }

   @Override
   public int wordSize() {
      return 4;
   }

   @Override
   public Exp getThis() {
      // 'this' is always at [fp-4]
      return new MEM(new BINOP(BINOP.MINUS, fp(), new CONST(wordSize())));
   }

   @Override
   public Exp getAddr(final String var) {
      if (locals.contains(var)) {
         final int index = locals.indexOf(var);
         return new MEM(new BINOP(BINOP.MINUS, fp(), new CONST((index+2)*wordSize())));
      } else {
         // implicit 'this.'
         assert fields.contains(var);
         final int index = fields.indexOf(var);
         return new MEM(new BINOP(BINOP.PLUS, getThis(), new CONST(index*wordSize())));
      }
   }

   private Exp arg(final int i) {
      assert i < 6;
      return new TEMP("%i" + i);
   }

   @Override
   public LazyIRTree procEntryExit1(final Stm body) {
      final Stm prologueStart = new LABEL(SAVE_LABEL + (locals.size()+1));
      final LABEL prologueEnd = new LABEL(methodName + "$prologueEnd");

      final NameOfLabel epilogueStart = new NameOfLabel(methodName + EPILOGUE_LABEL);
      final Stm labeledBody = SEQ.fromList(prologueEnd, body, new JUMP(epilogueStart));

      Stm prologue;
      final Stm saveThis = new MOVE(getThis(), arg(0));

      if (params.size() < 1) {
         prologue = new SEQ(prologueStart, saveThis);
      } else {
         final Stm[] saveArgs = new Stm[params.size()+1];
         saveArgs[0] = saveThis;

         for (int i = 1; i < params.size()+1; i++) {
            final String param = params.get(i-1);
            saveArgs[i] = new MOVE(getAddr(param), arg(i));
         }

         prologue = new SEQ(prologueStart, SEQ.fromList(saveArgs));
      }

      for (final String local : locals) {
         if (!params.contains(local)) {
            prologue = new SEQ(prologue, new MOVE(getAddr(local), new CONST(0)));
         }
      }

      return new LazyStm(SEQ.fromList(prologue, prologueEnd, body, new LABEL(epilogueStart),
               new JUMP(DUMMY_JUMP)));
   }
}
