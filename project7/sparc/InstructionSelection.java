package sparc;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;

import assem.*;
import canon.*;
import tree.*;

import translation.Fragment;

public class InstructionSelection {
   private static final Instruction NOP = new assem.OPER("   nop");

   public static Map<String, List<Instruction>> select(final Map<String, Fragment> frags) {
      final Map<String, List<Instruction>> bodies = new HashMap<String, List<Instruction>>();

      for (final Map.Entry<String, Fragment> frag : frags.entrySet()) {
         final String name = frag.getKey();

         final List<Instruction> instructions = new LinkedList<Instruction>();
         bodies.put(name, instructions);

         final List<Stm> stms = transform(frag.getValue().body.asStm());

         for (final Stm stm : stms) {
            System.err.println(stm);
            munchStm(stm, instructions);
         }
      }

      return bodies;
   }

   private static List<Stm> transform(final Stm s) {
      final List<Stm> stms = Canon.linearize(s);
      final List<List<Stm>> basicBlocks = BasicBlocks.makeBlocks(stms);
      final List<Stm> traces = TraceSchedule.trace(basicBlocks);
      return traces;
   }

   private static void munchStm(final Stm stm, final List<Instruction> instructions) {
      if (stm instanceof tree.SEQ) {
         final tree.SEQ seq = (tree.SEQ)stm;
         munchStm(seq.left, instructions);
         munchStm(seq.right, instructions);
      } else if (stm instanceof tree.MOVE) {
         munchMove((tree.MOVE)stm, instructions);
      } else if (stm instanceof tree.JUMP) {
         munchJump((tree.JUMP)stm, instructions);
      } else if (stm instanceof tree.CJUMP) {
         munchCJump((tree.CJUMP)stm, instructions);
      } else if (stm instanceof tree.LABEL) {
         munchLabel((tree.LABEL)stm, instructions);
      } else if (stm instanceof tree.EVAL) {
         munchExp(((tree.EVAL)stm).exp, instructions);
      } else {
         throw new RuntimeException("invalid statement: " + stm);
      }
   }

   private static void munchMove(final tree.MOVE move, final List<Instruction> instrs) {
      if (move.dst instanceof tree.TEMP) {
         final tree.NameOfTemp tmp = ((tree.TEMP)move.dst).temp;
         instrs.add(new assem.MOVE("   mov `s0, `d0", tmp, munchExp(move.src, instrs)));
      } else if (move.dst instanceof tree.MEM) {
         final tree.MEM mem = (tree.MEM)move.dst;
         instrs.add(new assem.MOVE("   st `s0, [`d0]", munchExp(mem.exp, instrs),
               munchExp(move.src, instrs)));
      } else {
         instrs.add(new assem.MOVE("   mov `s0, `d0", munchExp(move.dst, instrs),
                  munchExp(move.src, instrs)));
      }
   }

   private static void munchLabel(final tree.LABEL label, final List<Instruction> instrs) {
      final String l = label.label.toString();
      if (l.startsWith(SparcFrame.SAVE_LABEL)) {
         // special label for 'save' instruction
         final String offset = l.substring(SparcFrame.SAVE_LABEL.length());
         instrs.add(new assem.OPER("   save %sp, -4*(24+" + offset + "+0)&-8, %sp"));
      } else if (l.endsWith(SparcFrame.EPILOGUE_LABEL)) {
         // special label for epilogue
         instrs.addAll(Arrays.asList(
               new assem.LabelInstruction(l + ":", label.label),
               new assem.OPER("   ret"),
               new assem.OPER("   restore")));
      } else if (l.equals(SparcFrame.MAIN_START)) {
         instrs.addAll(Arrays.asList(
               new assem.LabelInstruction(l + ":", label.label),
               new assem.OPER("   save %sp, -96, %sp")
         ));
      } else if (l.equals(SparcFrame.MAIN_END)) {
         instrs.addAll(Arrays.asList(
               new assem.LabelInstruction(l + ":", label.label),
               new assem.OPER("   clr %o0"),
               new assem.OPER("   mov 1, %g1"),
               new assem.OPER("   ta 0")
         ));
      } else {
         instrs.add(new assem.LabelInstruction(l + ":", label.label));
      }
   }

   private static void munchJump(final tree.JUMP jump, final List<Instruction> instrs) {
      if (jump.exp instanceof tree.NAME) {
         final String label = ((NAME)jump.exp).label.toString();
         if (label.equals(SparcFrame.DUMMY_JUMP))
            return;
         instrs.add(new assem.OPER("   ba " + label));
      } else {
         instrs.add(new assem.OPER("   ba `s0", Collections.<NameOfTemp>emptyList(),
                  Arrays.asList(munchExp(jump.exp, instrs))));
      }
      instrs.add(NOP);
   }

   private static void munchCJump(final tree.CJUMP jump, final List<Instruction> instrs) {
      final String trueLabel = jump.iftrue.toString();
      final String falseLabel = jump.iffalse.toString();

      instrs.add(new assem.OPER("   cmp `s0, `s1", null,
            Arrays.asList(munchExp(jump.left, instrs), munchExp(jump.right, instrs))));

      final String branch;
      switch (jump.relop) {
         case CJUMP.EQ:
            branch = "be";
            break;
         case CJUMP.NE:
            branch = "bne";
            break;
         case CJUMP.LT:
            branch = "bl";
            break;
         case CJUMP.GT:
            branch = "bg";
            break;
         case CJUMP.LE:
            branch = "ble";
            break;
         case CJUMP.GE:
            branch = "bge";
            break;
         case CJUMP.ULT:
            branch = "blu";
            break;
         case CJUMP.ULE:
            branch = "bleu";
            break;
         case CJUMP.UGT:
            branch = "bgu";
            break;
         case CJUMP.UGE:
            branch = "bgeu";
            break;
         default: throw new RuntimeException("invalid CJUMP operator: " + jump.relop);
      }

      instrs.add(new assem.OPER("   " + branch + " " + trueLabel));
      instrs.add(NOP);
      instrs.add(new assem.OPER("   ba " + falseLabel));
      instrs.add(NOP);
   }

   private static NameOfTemp munchExp(final Exp exp, final List<Instruction> instrs) {
      if (exp instanceof BINOP) {
         return munchBinop((BINOP)exp, instrs);
      } else if (exp instanceof CALL) {
         return munchCall((CALL)exp, instrs);
      } else if (exp instanceof CONST) {
         return munchConst((CONST)exp, instrs);
      } else if (exp instanceof MEM) {
         return munchMem((MEM)exp, instrs);
      } else if (exp instanceof TEMP) {
         return ((TEMP)exp).temp;
      }

      throw new RuntimeException("invalid exp: " + exp.toString());
   }

   private static NameOfTemp munchBinop(final BINOP binop, final List<Instruction> instrs) {
      final NameOfTemp left = munchExp(binop.left, instrs);
      final NameOfTemp right = munchExp(binop.right, instrs);
      final NameOfTemp result = NameOfTemp.generateTemp("binop_result");

      final String instr;
      switch (binop.binop) {
         case BINOP.PLUS:
            instr = "add";
            break;
         case BINOP.MINUS:
            instr = "sub";
            break;
         case BINOP.MUL:
            final NameOfTemp prod = NameOfTemp.generateTemp("prod");
            instrs.addAll(Arrays.asList(
                  new assem.MOVE("   mov `s0, `d0", new NameOfTemp("%o0"), left),
                  new assem.MOVE("   mov `s0, `d0", new NameOfTemp("%o1"), right),
                  new assem.OPER("   call .mul"),
                  NOP,
                  new assem.MOVE("   mov `s0, `d0", prod, new NameOfTemp("%o0"))
            ));
            return prod;
         case BINOP.DIV:
            throw new RuntimeException("division not implemented - hopefully unnecessary");
         case BINOP.AND:
            instr = "and";
            break;
         case BINOP.OR:
            instr = "or";
            break;
         case BINOP.LSHIFT:
            instr = "sll";
            break;
         case BINOP.RSHIFT:
            instr = "srl";
            break;
         case BINOP.ARSHIFT:
            instr = "sra";
            break;
         case BINOP.XOR:
            instr = "xor";
            break;
         default:
            throw new RuntimeException("invalid binop: " + binop.binop);
      }

      instrs.add(new assem.OPER("   " + instr + " `s0, `s1, `d0", result, left, right));
      return result;
   }

   private static NameOfTemp munchCall(final CALL call, final List<Instruction> instrs) {
      final List<Exp> args = call.args.toList();
      if (args.size() > 6) {
         throw new RuntimeException("no more than 6 arguments per call are supported");
      }

      for (int i = 0; i < args.size(); i++) {
         final NameOfTemp arg = munchExp(args.get(i), instrs);
         final NameOfTemp reg = new NameOfTemp("%o" + i);
         instrs.add(new assem.MOVE("   mov `s0, `d0", reg, arg));
      }

      if (call.func instanceof NAME) {
         final String label = ((NAME)call.func).label.toString();
         instrs.add(new assem.OPER("   call " + label));
      } else {
         instrs.add(new assem.OPER("   call `s0", Collections.<NameOfTemp>emptyList(),
                  Arrays.asList(munchExp(call.func, instrs))));
      }
      instrs.add(NOP);

      return new NameOfTemp("%o0");
   }

   private static NameOfTemp munchConst(final CONST cnst, final List<Instruction> instrs) {
      final NameOfTemp result = NameOfTemp.generateTemp("const" + cnst.value + "_");
      instrs.add(new assem.MOVE("   set " + cnst.value + ", `d0", result, result));
      return result;
   }

   private static NameOfTemp munchMem(final MEM mem, final List<Instruction> instrs) {
      final NameOfTemp result = NameOfTemp.generateTemp("mem");
      instrs.add(new assem.MOVE("   ld [`s0], `d0", result, munchExp(mem.exp, instrs)));
      return result;
   }
}
