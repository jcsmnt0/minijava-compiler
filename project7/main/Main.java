package main;

import java.io.PrintWriter;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import syntax.Program;
import tree.TreePrint;

import assem.Instruction;
import parser.*;
import tree.*;
import main.*;
import semantics.*;
import translation.*;
import sparc.*;

public class Main {
   private static final boolean VERBOSE = System.getProperty("verbose") != null;

   private static final Map<Integer, String> kindImages = new HashMap<Integer, String>();
   static {
      try {
         final Class c = Class.forName("parser.MiniJavaParserConstants");
         final Field[] fs = c.getDeclaredFields();
         for (int i = 0; i < fs.length; i++) {
            if (fs[i].getType() == int.class) {
               final int val = fs[i].getInt(null);
               if (!kindImages.containsKey(val)) {
                  kindImages.put(val, fs[i].getName());
               }
            }
         }
      } catch (final Exception e) {
         throw new RuntimeException("Could not locate token kind constants");
      }
   }

   private static final String[] REGS = {
      "%l0", "%l1", "%l2", "%l3", "%l4", "%l5", "%l6", "%l7",
      "%i0", "%i1", "%i2", "%i3", "%i4", "%i5"
   };

   private static void printResult(final PrintStream out, final String inFile,
         final int errorCount) {
      final String result = String.format("filename=%s, errors=%d%n", inFile, errorCount);
      out.printf(result);
   }

   public static void main(final String[] args) {
      final String inFile = args[0];
      final PrintStream out = System.out;

      final MiniJavaParser parser = getParser(inFile);

      Program program = null;
      try {
         program = parser.Program();
      } catch (final ParseException e) {
         // If this catch block is triggered, the ParseException that triggered it has no
         // recovery method implemented in the parser
         parser.syntaxErrors.add(e);
      }

      int errorCount = parser.syntaxErrors.size();
      printParseErrors(inFile, parser.syntaxErrors);

      if (!parser.syntaxErrors.isEmpty()) {
         printResult(out, inFile, errorCount);
         return;
      }

      final SymbolTableVisitor symbolVisitor = new SymbolTableVisitor();
      program.accept(symbolVisitor);

      if (!symbolVisitor.errors.isEmpty()) {
         errorCount += symbolVisitor.errors.size();
         for (final SemanticError err : symbolVisitor.errors) {
            System.err.printf("%s:%s%n", inFile, err);
         }

         printResult(out, inFile, errorCount);
         return;
      }

      final Map<String, ClassSymbolTable> symbolTable = symbolVisitor.table;
      final TypeCheckVisitor typeChecker = new TypeCheckVisitor(symbolTable);
      program.accept(typeChecker);

      if (!typeChecker.errors.isEmpty()) {
         errorCount += typeChecker.errors.size();
         for (final SemanticError err : typeChecker.errors) {
            System.err.printf("%s:%s%n", inFile, err);
         }

         printResult(out, inFile, errorCount);
         return;
      }

      final Map<String, Integer> classSizes = new HashMap<String, Integer>();
      final Map<String, Frame> frames = new HashMap<String, Frame>();
      for (final ClassSymbolTable clas : symbolTable.values()) {
         final String id = clas.classId;
         classSizes.put(id, clas.fields.size());

         for (final MethodSymbolTable method : clas.methods.values()) {
            final String methodName = IRVisitor.getMethodName(id, method.methodId);
            frames.put(methodName, new SparcFrame(method));
         }
      }

      final IRVisitor translator = new IRVisitor(classSizes, frames, symbolTable);
      program.accept(translator);
      final Map<String, Fragment> fragments = translator.fragments;

      if (VERBOSE) {
         final TreePrint printer = new TreePrint(System.out);
         for (final String fragName : fragments.keySet()) {
            System.out.println(fragName + ":");
            printer.print(translator.fragments.get(fragName).body.asStm());
         }
      }

      final Map<String, List<Instruction>> bodies = InstructionSelection.select(fragments);
      final Map<NameOfTemp, String> regs = new HashMap<NameOfTemp, String>();

      for (final List<Instruction> body : bodies.values()) {
         int i = 1;
         for (final Instruction instr : body) {
            for (final NameOfTemp temp : instr.temps()) {
               if (!regs.containsKey(temp)) {
                  if (temp.toString().charAt(0) == '%') {
                     regs.put(temp, temp.toString());
                  } else {
                     regs.put(temp, REGS[i]);
                     i = (i+1) % REGS.length;
                  }
               }
            }
         }

         final int dirSeparator = inFile.lastIndexOf('/');
         final String asmDir = inFile.substring(0, dirSeparator);
         final String asmName = inFile.substring(dirSeparator, inFile.lastIndexOf('.')) + ".s";
         try {
            final PrintWriter asmFile = new PrintWriter(new File(asmDir+asmName));

            asmFile.println("   .section \".text\"");
            asmFile.println("   .align 4");
            asmFile.println("   .global main");
            
            for (final Map.Entry<String, List<Instruction>> entry : bodies.entrySet()) {
               asmFile.println(entry.getKey() + ":");
               for (final Instruction instr : entry.getValue()) {
                  asmFile.println(instr.format(regs));
               }
            }
            asmFile.close();
         } catch (final FileNotFoundException e) {
            System.err.printf("The file %s couldn't be written to%n", asmDir+asmName);
         }
      }

      printResult(out, inFile, errorCount);
   }

   private static void printParseErrors(final String file, final List<ParseException> errs) {
      for (final ParseException e : errs) {
         final StringBuilder expectedTokens = new StringBuilder();
         final Token t = e.currentToken;

         if (e.expectedTokenSequences.length > 0) {
            for (int i = 0; i < e.expectedTokenSequences.length; i++) {
               final int[] sequence = e.expectedTokenSequences[i];

               if (i > 0) {
                  expectedTokens.append("or ");
               }

               for (final int kind : sequence) {
                  expectedTokens.append(e.tokenImage[kind]).append(" ");
               }
            }

            System.err.printf("%s:%03d:%03d: Syntax Error: expecting %s%n", file, t.beginLine,
                  t.beginColumn, expectedTokens);
         } else {
            System.err.printf("%s:%03d:%03d: Syntax Error: expecting something or other%n",
                  file, t.beginLine, t.beginColumn);
         }
      }
   }

   private static MiniJavaParser getParser(final String inFile) {
      try {
         final MiniJavaParser parser = new MiniJavaParser(new FileInputStream(inFile),
               "UTF-8");

         if (VERBOSE) {
            System.setOut(new PrintStream(new FileOutputStream(new File("verbose.txt"))));
            parser.enable_tracing();
         } else {
            System.setOut(new PrintStream(new OutputStream() {
               @Override
               public void write(final int b) {/* ignored */}
            }));
            parser.disable_tracing();
         }

         return parser;
      } catch (final FileNotFoundException e) {
         System.err.println(e);
         throw new RuntimeException("The file could not be read.");
      }
   }
}
