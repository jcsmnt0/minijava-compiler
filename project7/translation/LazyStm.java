package translation;

import tree.*;

public class LazyStm implements LazyIRTree {
   final Stm stm;

   public LazyStm(final Stm s) {
      stm = s;
   }

   public Exp asExp() {
      throw new UnsupportedOperationException();
   }

   public Stm asStm() {
      return stm;
   }

   public Stm asCond(final NameOfLabel t, final NameOfLabel f) {
      throw new UnsupportedOperationException();
   }
}
