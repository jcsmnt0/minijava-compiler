package translation;

import tree.*;

public class LazyExp implements LazyIRTree {
   final Exp exp;

   public LazyExp(final Exp e) {
      exp = e;
   }

   public Exp asExp() {
      return exp;
   }

   public Stm asStm() {
      return new EVAL(exp);
   }

   public Stm asCond(final NameOfLabel t, final NameOfLabel f) {
      throw new UnsupportedOperationException();
   }
}
