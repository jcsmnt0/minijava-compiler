package translation;

import tree.NameOfLabel;
import tree.Exp;
import tree.Stm;

public interface LazyIRTree {
   public Exp asExp();
   public Stm asStm();
   public Stm asCond(final NameOfLabel t, final NameOfLabel f);
}
