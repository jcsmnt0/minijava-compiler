package translation;

import tree.*;

public class Fragment {
   public final Frame frame;
   public final LazyIRTree body;

   public Fragment(final Frame theFrame, final LazyIRTree theBody) {
      frame = theFrame;
      body = theBody;
   }
}
