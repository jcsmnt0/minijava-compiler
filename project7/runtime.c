#include <stdio.h>
#include <stdlib.h>

int* initObject(int size) {
   int* obj = malloc(sizeof(int) * size);

   if (obj == NULL) {
      fprintf(stderr, "malloc failed for object");
      fflush(stderr);
   }

   int i;
   for (i = 0; i < size; i++)
      obj[i] = 0;

   return obj;
}

int* initArray(int size) {
   int* arr = malloc(sizeof(int) * (size+1));
   
   if (arr == NULL) {
      fprintf(stderr, "malloc failed for array");
      fflush(stderr);
   }

   arr[0] = size;

   int i;
   for (i = 1; i < size+1; i++)
      arr[i] = 0;

   return arr;
}

void printInt(int n) {
   printf("%d\n", n);
   fflush(stdout);
}
