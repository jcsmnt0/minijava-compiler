#include <iostream>

int* initObject(int size) {
   int *obj = new int[size];
   int i;
   for (i = 0; i < size; i++)
      obj[i] = 0;
   return obj;
}

int* initArray(int size) {
   int *arr = new int[size+1];
   arr[0] = size;
   int i;
   for (i = 1; i < size+1; i++)
      arr[i] = 0;
   return arr;
}

void printInt(int n) {
   std::cout << n << std::endl;
}
