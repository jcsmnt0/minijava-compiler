class Main {
   public static void main(String[] s) {
      System.out.println(new Test().test());
   }
}

class Test {
   public int test() {
      int i;

      i = 0;
      while (i < 10) {
         System.out.println(i);
         i = i + 1;
      }

      return i;
   }
}
