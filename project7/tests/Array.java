class Main {
   public static void main(String[] s) {
      System.out.println((new Array()).test1());
   }
}

class Array {
   int[] xs;

   public int test1() {
      int i;

      xs = new int[10];

      i = 0;
      while (i < 10) {
         xs[i] = i*4;
         i = i+1;
      }

      i = this.print();

      return 0;
   }

   public int print() {
      int i;

      i = 0;
      while (i < xs.length) {
         System.out.println(xs[i]);
         i = i+1;
      }

      return 0;
   }
}
