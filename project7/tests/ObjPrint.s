   .section ".text"
   .align 4
   .global main
Print$print:
   save %sp, -4*(16+2+0)&-8, %sp
   st %i0, [%fp]
   mov 4, %r2
   sub %fp, %r2, %r3
   st %i1, [%r3]
   ba Print$print$prologueEnd
   nop
Print$print$prologueEnd:
   mov 4, %r4
   sub %fp, %r4, %r5
   ld [%r5], %r6
   mov 1, %r16
   add %r6, %r16, %r17
   mov %r17, %i0
   ba Print$print$epilogueStart
   nop
Print$print$epilogueStart:
   ret
   restore
BB$begin001:
   mov 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov 1, %r21
   add %r20, %r21, %r22
   mov %r22, %i0
   ba Print$print$epilogueStart
   nop
Main$main:
main:
   save %sp, -96, %sp
   mov 0, %r2
   mov %r2, %o0
   call initObject
   nop
   mov %o0, %r3
   mov %r3, %o0
   mov 2, %r4
   mov %r4, %o1
   call Print$print
   nop
   mov %o0, %r5
   mov %r5, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
