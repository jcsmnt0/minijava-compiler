   .section ".text"
   .align 4
   .global main
Main$main:
main:
   save %sp, -96, %sp
   set 1, %r17
   mov %r17, %o0
   call initObject
   nop
   mov %o0, %r18
   mov %r18, %o0
   call Array$test1
   nop
   mov %o0, %r19
   mov %r19, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
Array$print:
   save %sp, -4*(24+2+0)&-8, %sp
   set 4, %r17
   sub %fp, %r17, %r18
   st %i0, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba Array$print$prologueEnd
   nop
Array$print$prologueEnd:
   set 8, %r22
   sub %fp, %r22, %r23
   set 0, %r24
   st %r24, [%r23]
   ba test008
   nop
test008:
   set 1, %r25
   mov %r25, %r26
   set 8, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   set 0, %r19
   add %r18, %r19, %r20
   ld [%r20], %r21
   ld [%r21], %r22
   cmp %r29, %r22
   bl t006
   nop
   ba f007
   nop
f007:
   set 0, %r23
   mov %r23, %r26
   ba t006
   nop
t006:
   set 1, %r24
   cmp %r26, %r24
   be body009
   nop
   ba end010
   nop
end010:
   set 0, %r25
   mov %r25, %i0
   ba Array$print$epilogueStart
   nop
Array$print$epilogueStart:
   ret
   restore
body009:
   set 4, %r26
   sub %fp, %r26, %r27
   ld [%r27], %r28
   set 0, %r29
   add %r28, %r29, %r16
   ld [%r16], %r17
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   set 4, %r21
   mov %r20, %o0
   mov %r21, %o1
   call .mul
   nop
   mov %o0, %r22
   set 4, %r23
   add %r22, %r23, %r24
   add %r17, %r24, %r25
   ld [%r25], %r26
   mov %r26, %o0
   call printInt
   nop
   set 8, %r27
   sub %fp, %r27, %r28
   set 8, %r29
   sub %fp, %r29, %r16
   ld [%r16], %r17
   set 1, %r18
   add %r17, %r18, %r19
   st %r19, [%r28]
   ba test008
   nop
Array$test1:
   save %sp, -4*(24+2+0)&-8, %sp
   set 4, %r17
   sub %fp, %r17, %r18
   st %i0, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba Array$test1$prologueEnd
   nop
Array$test1$prologueEnd:
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r24
   set 0, %r25
   add %r24, %r25, %r26
   mov %r26, %r27
   set 10, %r28
   mov %r28, %o0
   call initArray
   nop
   mov %o0, %r29
   st %r29, [%r27]
   set 8, %r16
   sub %fp, %r16, %r17
   set 0, %r18
   st %r18, [%r17]
   ba test003
   nop
test003:
   set 1, %r19
   mov %r19, %r20
   set 8, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   set 10, %r24
   cmp %r23, %r24
   bl t001
   nop
   ba f002
   nop
f002:
   set 0, %r25
   mov %r25, %r20
   ba t001
   nop
t001:
   set 1, %r26
   cmp %r20, %r26
   be body004
   nop
   ba end005
   nop
end005:
   set 8, %r27
   sub %fp, %r27, %r28
   mov %r28, %r29
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Array$print
   nop
   mov %o0, %r19
   st %r19, [%r29]
   set 0, %r20
   mov %r20, %i0
   ba Array$test1$epilogueStart
   nop
Array$test1$epilogueStart:
   ret
   restore
body004:
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   set 0, %r24
   add %r23, %r24, %r25
   ld [%r25], %r26
   set 8, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   set 4, %r16
   mov %r29, %o0
   mov %r16, %o1
   call .mul
   nop
   mov %o0, %r17
   set 4, %r18
   add %r17, %r18, %r19
   add %r26, %r19, %r20
   set 8, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   set 4, %r24
   mov %r23, %o0
   mov %r24, %o1
   call .mul
   nop
   mov %o0, %r25
   st %r25, [%r20]
   set 8, %r26
   sub %fp, %r26, %r27
   set 8, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   set 1, %r17
   add %r16, %r17, %r18
   st %r18, [%r27]
   ba test003
   nop
