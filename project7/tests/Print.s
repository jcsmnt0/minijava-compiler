   .section ".text"
   .align 4
   .global main
Main$main:
main:
   save %sp, -96, %sp
   set 2, %l1
   mov %l1, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
