class Main {
   public static void main(String[] s) {
      System.out.println(new Test().test());
   }
}

class Test {
   public int test() {
      Test i;
      i = new Test();
      
      if (true) {
         System.out.println(1);
      } else {
         System.out.println(0);
      }

      if (false) {
         System.out.println(0);
      } else {
         System.out.println(1);
      }

      System.out.println(this.boolToInt(true));
      System.out.println((this.boolToInt(false)) + 1);

      System.out.println(this.boolToInt(this.equal(3, 4)));

      return 1;
   }

   public boolean equal(int x, int y) {
      boolean eq;
      System.out.println(x);
      System.out.println(y);

      if (x < y)
         eq = false;
      else
         if (y < x)
            eq = false;
         else
            eq = true;

      return eq;
   }
      
   public int boolToInt(boolean x) {
      int out;
      if (x) {
         out = 1;
      } else {
         out = 0;
      }
      return out;
   }
}
