class Main {
   public static void main(String[] s) {
      System.out.println(new List().test());
   }
}

class List {
   int head;
   List tail;
   boolean end;

   public int test() {
      return 0;
   }

   public List empty() {
      List l;

      l = new List();
      l.end = true;

      return l;
   }

   public int init(int elem, List tail, boolean end) {
      head = elem;
      tail = tail;
      end = end;

      return 0;
   }

   public List push(int elem) {
      List newList;

      newList = this.init(elem, this, false);

      return newList;
   }

   public List pop() {
      return this.tail;
   }
}
