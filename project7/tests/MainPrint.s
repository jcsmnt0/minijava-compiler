   .section ".text"
   .align 4
   .global main
MainPrint$main:
main:
   save %sp, -96, %sp
   mov 1, %r2
   mov %r2, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
