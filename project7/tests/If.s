   .section ".text"
   .align 4
   .global main
Test$test:
   save %sp, -4*(16+2+0)&-8, %sp
   st %i0, [%fp]
   mov 4, %r2
   sub %fp, %r2, %r3
   mov 0, %r4
   st %r4, [%r3]
   ba Test$test$prologueEnd
   nop
Test$test$prologueEnd:
   mov 4, %r5
   sub %fp, %r5, %r6
   mov %r6, %r16
   mov 0, %r17
   mov %r17, %o0
   call initObject
   nop
   mov %o0, %r18
   st %r18, [%r16]
   mov 1, %r19
   mov 1, %r20
   cmp %r19, %r20
   be t001
   nop
   ba f002
   nop
f002:
   mov 0, %r21
   mov %r21, %o0
   call printInt
   nop
   ba end003
   nop
end003:
   mov 0, %r22
   mov 1, %r23
   cmp %r22, %r23
   be t004
   nop
   ba f005
   nop
f005:
   mov 1, %r24
   mov %r24, %o0
   call printInt
   nop
   ba end006
   nop
end006:
   ld [%fp], %r25
   mov %r25, %o0
   mov 1, %r26
   mov %r26, %o1
   call Test$boolToInt
   nop
   mov %o0, %r27
   mov %r27, %o0
   call printInt
   nop
   ld [%fp], %r28
   mov %r28, %o0
   mov 0, %r29
   mov %r29, %o1
   call Test$boolToInt
   nop
   mov %o0, %r1
   mov 1, %r2
   add %r1, %r2, %r3
   mov %r3, %o0
   call printInt
   nop
   ld [%fp], %r4
   mov %r4, %r5
   ld [%fp], %r6
   mov %r6, %o0
   mov 3, %r16
   mov %r16, %o1
   mov 4, %r17
   mov %r17, %o2
   call Test$equal
   nop
   mov %o0, %r18
   mov %r5, %o0
   mov %r18, %o1
   call Test$boolToInt
   nop
   mov %o0, %r19
   mov %r19, %o0
   call printInt
   nop
   mov 1, %r20
   mov %r20, %i0
   ba Test$test$epilogueStart
   nop
Test$test$epilogueStart:
   ret
   restore
t001:
   mov 1, %r21
   mov %r21, %o0
   call printInt
   nop
   ba end003
   nop
t004:
   mov 0, %r22
   mov %r22, %o0
   call printInt
   nop
   ba end006
   nop
Main$main:
main:
   save %sp, -96, %sp
   mov 0, %r2
   mov %r2, %o0
   call initObject
   nop
   mov %o0, %r3
   mov %r3, %o0
   call Test$test
   nop
   mov %o0, %r4
   mov %r4, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
Test$boolToInt:
   save %sp, -4*(16+3+0)&-8, %sp
   st %i0, [%fp]
   mov 4, %r2
   sub %fp, %r2, %r3
   st %i1, [%r3]
   mov 8, %r4
   sub %fp, %r4, %r5
   mov 0, %r6
   st %r6, [%r5]
   ba Test$boolToInt$prologueEnd
   nop
Test$boolToInt$prologueEnd:
   mov 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov 1, %r19
   cmp %r18, %r19
   be t017
   nop
   ba f018
   nop
f018:
   mov 8, %r20
   sub %fp, %r20, %r21
   mov 0, %r22
   st %r22, [%r21]
   ba end019
   nop
end019:
   mov 8, %r23
   sub %fp, %r23, %r24
   ld [%r24], %r25
   mov %r25, %i0
   ba Test$boolToInt$epilogueStart
   nop
Test$boolToInt$epilogueStart:
   ret
   restore
t017:
   mov 8, %r26
   sub %fp, %r26, %r27
   mov 1, %r28
   st %r28, [%r27]
   ba end019
   nop
Test$equal:
   save %sp, -4*(16+4+0)&-8, %sp
   st %i0, [%fp]
   mov 4, %r2
   sub %fp, %r2, %r3
   st %i1, [%r3]
   mov 8, %r4
   sub %fp, %r4, %r5
   st %i2, [%r5]
   mov 12, %r6
   sub %fp, %r6, %r16
   mov 0, %r17
   st %r17, [%r16]
   ba Test$equal$prologueEnd
   nop
Test$equal$prologueEnd:
   mov 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   call printInt
   nop
   mov 8, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call printInt
   nop
   mov 1, %r24
   mov %r24, %r25
   mov 4, %r26
   sub %fp, %r26, %r27
   ld [%r27], %r28
   mov 8, %r29
   sub %fp, %r29, %r1
   ld [%r1], %r2
   cmp %r28, %r2
   bl t007
   nop
   ba f008
   nop
f008:
   mov 0, %r3
   mov %r3, %r25
   ba t007
   nop
t007:
   mov 1, %r4
   cmp %r25, %r4
   be t014
   nop
   ba f015
   nop
f015:
   mov 1, %r5
   mov %r5, %r6
   mov 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   cmp %r18, %r21
   bl t009
   nop
   ba f010
   nop
f010:
   mov 0, %r22
   mov %r22, %r6
   ba t009
   nop
t009:
   mov 1, %r23
   cmp %r6, %r23
   be t011
   nop
   ba f012
   nop
f012:
   mov 12, %r24
   sub %fp, %r24, %r25
   mov 1, %r26
   st %r26, [%r25]
   ba end013
   nop
end013:
   ba end016
   nop
end016:
   mov 12, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   mov %r29, %i0
   ba Test$equal$epilogueStart
   nop
Test$equal$epilogueStart:
   ret
   restore
t014:
   mov 12, %r1
   sub %fp, %r1, %r2
   mov 0, %r3
   st %r3, [%r2]
   ba end016
   nop
t011:
   mov 12, %r4
   sub %fp, %r4, %r5
   mov 0, %r6
   st %r6, [%r5]
   ba end013
   nop
