   .section ".text"
   .align 4
   .global main
Test$test:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   ba Test$test$prologueEnd
   nop
Test$test$prologueEnd:
   set 4, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   ba test003
   nop
test003:
   set 1, %r23
   mov %r23, %r24
   set 4, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   set 10, %r28
   cmp %r27, %r28
   bl t001
   nop
   ba f002
   nop
f002:
   set 0, %r29
   mov %r29, %r24
   ba t001
   nop
t001:
   set 1, %r16
   cmp %r24, %r16
   be body004
   nop
   ba end005
   nop
end005:
   set 4, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %i0
   ba Test$test$epilogueStart
   nop
Test$test$epilogueStart:
   ret
   restore
body004:
   set 4, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   call printInt
   nop
   set 4, %r23
   sub %fp, %r23, %r24
   set 4, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   set 1, %r28
   add %r27, %r28, %r29
   st %r29, [%r24]
   ba test003
   nop
Main$main:
main:
   save %sp, -96, %sp
   set 0, %r17
   mov %r17, %o0
   call initObject
   nop
   mov %o0, %r18
   mov %r18, %o0
   call Test$test
   nop
   mov %o0, %r19
   mov %r19, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
