package semantics;

import java.util.Map;
import java.util.LinkedHashMap;
import syntax.Type;

public class ClassSymbolTable {
   public final Map<String, Type> fields = new LinkedHashMap<String, Type>();
   public final Map<String, MethodSymbolTable> methods
         = new LinkedHashMap<String, MethodSymbolTable>();
   public final String classId, superclassId;

   public ClassSymbolTable(final String theClassId, final String theSuperclassId) {
      classId = theClassId;
      superclassId = theSuperclassId;
   }

   public ClassSymbolTable(final String theClassId) {
      this(theClassId, null);
   }
}
