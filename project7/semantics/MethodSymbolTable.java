package semantics;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.LinkedList;
import syntax.Type;

public class MethodSymbolTable {
   public final List<String> formalParamNames = new LinkedList<String>();
   public final List<Type> formalParams = new LinkedList<Type>();
   public final Map<String, Type> localVars = new LinkedHashMap<String, Type>();
   public final String methodId;
   public final Type returnType;
   public final ClassSymbolTable clas;

   public MethodSymbolTable(final ClassSymbolTable theClass, final String theMethodId,
         final Type theReturnType) {
      clas = theClass;
      methodId = theMethodId;
      returnType = theReturnType;
   }
}
