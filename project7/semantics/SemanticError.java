package semantics;

public class SemanticError {
   public final int line, column;
   public final String message;

   public SemanticError(final int theLine, final int theColumn,
         final String theMessage) {
      line = theLine;
      column = theColumn;
      message = theMessage;
   }

   @Override
   public String toString() {
      return String.format("%d.%d: %s", line, column, message);
   }
}
