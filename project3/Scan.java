import scanner.*;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.HashMap;

public class Scan {
   private static final Map<Integer, String> kindImages = new HashMap<Integer, String>();
   static {
      try {
         final Class c = Class.forName("scanner.MiniJavaScannerConstants");
         final Field[] fs = c.getDeclaredFields();
         for (int i = 0; i < fs.length; i++) {
            if (fs[i].getType() == int.class) {
               final int val = fs[i].getInt(null);
               if (!kindImages.containsKey(val)) {
                  kindImages.put(val, fs[i].getName());
               }
            }
         }
      } catch (final Exception e) {
         throw new RuntimeException("Could not locate token kind constants");
      }
   }

   private static final boolean verbose = System.getProperty("verbose") != null;

   public static void main(final String[] args) {
      final String filePath = args[0];

      final MiniJavaScanner lexer;
      try {
         lexer = new MiniJavaScanner(new FileInputStream(filePath), "UTF-8");
      } catch (final FileNotFoundException e) {
         throw new RuntimeException("The file could not be read.");
      }

      final PrintWriter vout;
      if (verbose) {
         try {
            vout = new PrintWriter(new File("verbose.txt"));
         } catch (final IOException e) {
            throw new RuntimeException("Could not create verbose log file");
         }
      } else {
         vout = null;
      }

      int errors = 0;

      // kind 0 is EOF
      Token t;
      do {
         t = lexer.getNextToken();
         final String kindImage = kindImages.get(t.kind);

         if ("ERROR".equals(kindImage)) {
            System.err.printf("%s:%03d.%03d: ERROR -- illegal character %s%n", filePath, t.beginLine,
                  t.beginColumn, t.image);
            errors++;
         }

         if (verbose) {
            vout.printf("%s:%03d.%03d: %s \"%s\"%n", filePath, t.beginLine, t.beginColumn, kindImage,
                  t.image);
         }
      } while (t.kind != 0); // kind 0 is EOF

      final String result = String.format("filename=%s, errors=%d%n", filePath, errors);
      System.out.printf(result);

      if (verbose) {
         vout.print(result);
         vout.close();
      }
   }
}
