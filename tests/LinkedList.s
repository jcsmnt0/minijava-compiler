   .section ".text"
   .align 4
   .global main
LL$Start:
   save %sp, -4*(24+7+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 8, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 12, %r23
   sub %fp, %r23, %r24
   set 0, %r25
   st %r25, [%r24]
   set 16, %r26
   sub %fp, %r26, %r27
   set 0, %r28
   st %r28, [%r27]
   set 20, %r29
   sub %fp, %r29, %r16
   set 0, %r17
   st %r17, [%r16]
   set 24, %r18
   sub %fp, %r18, %r19
   set 0, %r20
   st %r20, [%r19]
   ba LL$Start$prologueEnd
   nop
LL$Start$prologueEnd:
   set 8, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 3, %r24
   mov %r24, %o0
   call initObject
   nop
   mov %o0, %r25
   st %r25, [%r23]
   set 12, %r26
   sub %fp, %r26, %r27
   mov %r27, %r28
   set 8, %r29
   sub %fp, %r29, %r16
   ld [%r16], %r17
   mov %r17, %o0
   call List$Init
   nop
   mov %o0, %r18
   st %r18, [%r28]
   set 4, %r19
   sub %fp, %r19, %r20
   set 8, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   st %r23, [%r20]
   set 12, %r24
   sub %fp, %r24, %r25
   mov %r25, %r26
   set 4, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   mov %r29, %o0
   call List$Init
   nop
   mov %o0, %r16
   st %r16, [%r26]
   set 12, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 4, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   call List$Print
   nop
   mov %o0, %r23
   st %r23, [%r19]
   set 16, %r24
   sub %fp, %r24, %r25
   mov %r25, %r26
   set 3, %r27
   mov %r27, %o0
   call initObject
   nop
   mov %o0, %r28
   st %r28, [%r26]
   set 12, %r29
   sub %fp, %r29, %r16
   mov %r16, %r17
   set 16, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   set 25, %r21
   mov %r21, %o1
   set 37000, %r22
   mov %r22, %o2
   set 0, %r23
   mov %r23, %o3
   call Element$Init
   nop
   mov %o0, %r24
   st %r24, [%r17]
   set 4, %r25
   sub %fp, %r25, %r26
   mov %r26, %r27
   set 4, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   mov %r16, %o0
   set 16, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o1
   call List$Insert
   nop
   mov %o0, %r20
   st %r20, [%r27]
   set 12, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 4, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   mov %r26, %o0
   call List$Print
   nop
   mov %o0, %r27
   st %r27, [%r23]
   set 10000000, %r28
   mov %r28, %o0
   call printInt
   nop
   set 16, %r29
   sub %fp, %r29, %r16
   mov %r16, %r17
   set 3, %r18
   mov %r18, %o0
   call initObject
   nop
   mov %o0, %r19
   st %r19, [%r17]
   set 12, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   set 16, %r23
   sub %fp, %r23, %r24
   ld [%r24], %r25
   mov %r25, %o0
   set 39, %r26
   mov %r26, %o1
   set 42000, %r27
   mov %r27, %o2
   set 1, %r28
   mov %r28, %o3
   call Element$Init
   nop
   mov %o0, %r29
   st %r29, [%r22]
   set 20, %r16
   sub %fp, %r16, %r17
   set 16, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   st %r20, [%r17]
   set 4, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 4, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   mov %r26, %o0
   set 16, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   mov %r29, %o1
   call List$Insert
   nop
   mov %o0, %r16
   st %r16, [%r23]
   set 12, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 4, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   call List$Print
   nop
   mov %o0, %r23
   st %r23, [%r19]
   set 10000000, %r24
   mov %r24, %o0
   call printInt
   nop
   set 16, %r25
   sub %fp, %r25, %r26
   mov %r26, %r27
   set 3, %r28
   mov %r28, %o0
   call initObject
   nop
   mov %o0, %r29
   st %r29, [%r27]
   set 12, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 16, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   set 22, %r22
   mov %r22, %o1
   set 34000, %r23
   mov %r23, %o2
   set 0, %r24
   mov %r24, %o3
   call Element$Init
   nop
   mov %o0, %r25
   st %r25, [%r18]
   set 4, %r26
   sub %fp, %r26, %r27
   mov %r27, %r28
   set 4, %r29
   sub %fp, %r29, %r16
   ld [%r16], %r17
   mov %r17, %o0
   set 16, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o1
   call List$Insert
   nop
   mov %o0, %r21
   st %r21, [%r28]
   set 12, %r22
   sub %fp, %r22, %r23
   mov %r23, %r24
   set 4, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   mov %r27, %o0
   call List$Print
   nop
   mov %o0, %r28
   st %r28, [%r24]
   set 24, %r29
   sub %fp, %r29, %r16
   mov %r16, %r17
   set 3, %r18
   mov %r18, %o0
   call initObject
   nop
   mov %o0, %r19
   st %r19, [%r17]
   set 12, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   set 24, %r23
   sub %fp, %r23, %r24
   ld [%r24], %r25
   mov %r25, %o0
   set 27, %r26
   mov %r26, %o1
   set 34000, %r27
   mov %r27, %o2
   set 0, %r28
   mov %r28, %o3
   call Element$Init
   nop
   mov %o0, %r29
   st %r29, [%r22]
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   set 20, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o1
   call List$Search
   nop
   mov %o0, %r22
   mov %r22, %o0
   call printInt
   nop
   set 4, %r23
   sub %fp, %r23, %r24
   ld [%r24], %r25
   mov %r25, %o0
   set 24, %r26
   sub %fp, %r26, %r27
   ld [%r27], %r28
   mov %r28, %o1
   call List$Search
   nop
   mov %o0, %r29
   mov %r29, %o0
   call printInt
   nop
   set 10000000, %r16
   mov %r16, %o0
   call printInt
   nop
   set 16, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 3, %r20
   mov %r20, %o0
   call initObject
   nop
   mov %o0, %r21
   st %r21, [%r19]
   set 12, %r22
   sub %fp, %r22, %r23
   mov %r23, %r24
   set 16, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   mov %r27, %o0
   set 28, %r28
   mov %r28, %o1
   set 35000, %r29
   mov %r29, %o2
   set 0, %r16
   mov %r16, %o3
   call Element$Init
   nop
   mov %o0, %r17
   st %r17, [%r24]
   set 4, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 16, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   mov %r26, %o1
   call List$Insert
   nop
   mov %o0, %r27
   st %r27, [%r20]
   set 12, %r28
   sub %fp, %r28, %r29
   mov %r29, %r16
   set 4, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o0
   call List$Print
   nop
   mov %o0, %r20
   st %r20, [%r16]
   set 2220000, %r21
   mov %r21, %o0
   call printInt
   nop
   set 4, %r22
   sub %fp, %r22, %r23
   mov %r23, %r24
   set 4, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   mov %r27, %o0
   set 20, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   mov %r16, %o1
   call List$Delete
   nop
   mov %o0, %r17
   st %r17, [%r24]
   set 12, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call List$Print
   nop
   mov %o0, %r24
   st %r24, [%r20]
   set 33300000, %r25
   mov %r25, %o0
   call printInt
   nop
   set 4, %r26
   sub %fp, %r26, %r27
   mov %r27, %r28
   set 4, %r29
   sub %fp, %r29, %r16
   ld [%r16], %r17
   mov %r17, %o0
   set 16, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o1
   call List$Delete
   nop
   mov %o0, %r21
   st %r21, [%r28]
   set 12, %r22
   sub %fp, %r22, %r23
   mov %r23, %r24
   set 4, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   mov %r27, %o0
   call List$Print
   nop
   mov %o0, %r28
   st %r28, [%r24]
   set 44440000, %r29
   mov %r29, %o0
   call printInt
   nop
   set 0, %r16
   mov %r16, %i0
   ba LL$Start$epilogueStart
   nop
LL$Start$epilogueStart:
   ret
   restore
List$InitNew:
   save %sp, -4*(24+4+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   st %i2, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   st %i3, [%r22]
   ba List$InitNew$prologueEnd
   nop
List$InitNew$prologueEnd:
   ld [%fp], %r23
   set 8, %r24
   add %r23, %r24, %r25
   set 12, %r26
   sub %fp, %r26, %r27
   ld [%r27], %r28
   st %r28, [%r25]
   ld [%fp], %r29
   set 0, %r16
   add %r29, %r16, %r17
   set 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   st %r20, [%r17]
   ld [%fp], %r21
   set 4, %r22
   add %r21, %r22, %r23
   set 8, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   st %r26, [%r23]
   set 1, %r27
   mov %r27, %i0
   ba List$InitNew$epilogueStart
   nop
List$InitNew$epilogueStart:
   ret
   restore
Element$GetMarried:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Element$GetMarried$prologueEnd
   nop
Element$GetMarried$prologueEnd:
   ld [%fp], %r17
   set 8, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Element$GetMarried$epilogueStart
   nop
Element$GetMarried$epilogueStart:
   ret
   restore
BB$begin049:
   ld [%fp], %r21
   set 8, %r22
   add %r21, %r22, %r23
   ld [%r23], %r24
   mov %r24, %i0
   ba Element$GetMarried$epilogueStart
   nop
List$Search:
   save %sp, -4*(24+7+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 12, %r22
   sub %fp, %r22, %r23
   set 0, %r24
   st %r24, [%r23]
   set 16, %r25
   sub %fp, %r25, %r26
   set 0, %r27
   st %r27, [%r26]
   set 20, %r28
   sub %fp, %r28, %r29
   set 0, %r16
   st %r16, [%r29]
   set 24, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   ba List$Search$prologueEnd
   nop
List$Search$prologueEnd:
   set 8, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 12, %r23
   sub %fp, %r23, %r24
   ld [%fp], %r25
   st %r25, [%r24]
   set 20, %r26
   sub %fp, %r26, %r27
   ld [%fp], %r28
   set 8, %r29
   add %r28, %r29, %r16
   ld [%r16], %r17
   st %r17, [%r27]
   set 16, %r18
   sub %fp, %r18, %r19
   ld [%fp], %r20
   set 0, %r21
   add %r20, %r21, %r22
   ld [%r22], %r23
   st %r23, [%r19]
   ba test043
   nop
test043:
   set 20, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   set 1, %r27
   xor %r26, %r27, %r28
   set 1, %r29
   cmp %r28, %r29
   be body044
   nop
   ba end045
   nop
end045:
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %i0
   ba List$Search$epilogueStart
   nop
List$Search$epilogueStart:
   ret
   restore
body044:
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   set 16, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r24
   mov %r24, %o1
   call Element$Equal
   nop
   mov %o0, %r25
   set 1, %r26
   cmp %r25, %r26
   be t040
   nop
   ba f041
   nop
f041:
   set 24, %r27
   sub %fp, %r27, %r28
   set 0, %r29
   st %r29, [%r28]
   ba end042
   nop
end042:
   set 12, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 12, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call List$GetNext
   nop
   mov %o0, %r22
   st %r22, [%r18]
   set 20, %r23
   sub %fp, %r23, %r24
   mov %r24, %r25
   set 12, %r26
   sub %fp, %r26, %r27
   ld [%r27], %r28
   mov %r28, %o0
   call List$GetEnd
   nop
   mov %o0, %r29
   st %r29, [%r25]
   set 16, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 12, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call List$GetElem
   nop
   mov %o0, %r22
   st %r22, [%r18]
   ba test043
   nop
t040:
   set 8, %r23
   sub %fp, %r23, %r24
   set 1, %r25
   st %r25, [%r24]
   ba end042
   nop
Element$Init:
   save %sp, -4*(24+4+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   st %i2, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   st %i3, [%r22]
   ba Element$Init$prologueEnd
   nop
Element$Init$prologueEnd:
   ld [%fp], %r23
   set 0, %r24
   add %r23, %r24, %r25
   set 4, %r26
   sub %fp, %r26, %r27
   ld [%r27], %r28
   st %r28, [%r25]
   ld [%fp], %r29
   set 4, %r16
   add %r29, %r16, %r17
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   st %r20, [%r17]
   ld [%fp], %r21
   set 8, %r22
   add %r21, %r22, %r23
   set 12, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   st %r26, [%r23]
   set 1, %r27
   mov %r27, %i0
   ba Element$Init$epilogueStart
   nop
Element$Init$epilogueStart:
   ret
   restore
Element$Compare:
   save %sp, -4*(24+5+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   st %i2, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   set 16, %r24
   sub %fp, %r24, %r25
   set 0, %r26
   st %r26, [%r25]
   ba Element$Compare$prologueEnd
   nop
Element$Compare$prologueEnd:
   set 12, %r27
   sub %fp, %r27, %r28
   set 0, %r29
   st %r29, [%r28]
   set 16, %r16
   sub %fp, %r16, %r17
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   set 1, %r21
   add %r20, %r21, %r22
   st %r22, [%r17]
   set 1, %r23
   mov %r23, %r24
   set 4, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   set 8, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   cmp %r27, %r16
   bl t016
   nop
   ba f017
   nop
f017:
   set 0, %r17
   mov %r17, %r24
   ba t016
   nop
t016:
   set 1, %r18
   cmp %r24, %r18
   be t023
   nop
   ba f024
   nop
f024:
   set 1, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   set 16, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   cmp %r23, %r26
   bl t018
   nop
   ba f019
   nop
f019:
   set 0, %r27
   mov %r27, %r20
   ba t018
   nop
t018:
   set 1, %r28
   xor %r20, %r28, %r29
   set 1, %r16
   cmp %r29, %r16
   be t020
   nop
   ba f021
   nop
f021:
   set 12, %r17
   sub %fp, %r17, %r18
   set 1, %r19
   st %r19, [%r18]
   ba end022
   nop
end022:
   ba end025
   nop
end025:
   set 12, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %i0
   ba Element$Compare$epilogueStart
   nop
Element$Compare$epilogueStart:
   ret
   restore
t023:
   set 12, %r23
   sub %fp, %r23, %r24
   set 0, %r25
   st %r25, [%r24]
   ba end025
   nop
t020:
   set 12, %r26
   sub %fp, %r26, %r27
   set 0, %r28
   st %r28, [%r27]
   ba end022
   nop
List$Print:
   save %sp, -4*(24+4+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 8, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 12, %r23
   sub %fp, %r23, %r24
   set 0, %r25
   st %r25, [%r24]
   ba List$Print$prologueEnd
   nop
List$Print$prologueEnd:
   set 4, %r26
   sub %fp, %r26, %r27
   ld [%fp], %r28
   st %r28, [%r27]
   set 8, %r29
   sub %fp, %r29, %r16
   ld [%fp], %r17
   set 8, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   st %r20, [%r16]
   set 12, %r21
   sub %fp, %r21, %r22
   ld [%fp], %r23
   set 0, %r24
   add %r23, %r24, %r25
   ld [%r25], %r26
   st %r26, [%r22]
   ba test046
   nop
test046:
   set 8, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   set 1, %r16
   xor %r29, %r16, %r17
   set 1, %r18
   cmp %r17, %r18
   be body047
   nop
   ba end048
   nop
end048:
   set 1, %r19
   mov %r19, %i0
   ba List$Print$epilogueStart
   nop
List$Print$epilogueStart:
   ret
   restore
body047:
   set 12, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   call Element$GetAge
   nop
   mov %o0, %r23
   mov %r23, %o0
   call printInt
   nop
   set 4, %r24
   sub %fp, %r24, %r25
   mov %r25, %r26
   set 4, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   mov %r29, %o0
   call List$GetNext
   nop
   mov %o0, %r16
   st %r16, [%r26]
   set 8, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 4, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   call List$GetEnd
   nop
   mov %o0, %r23
   st %r23, [%r19]
   set 12, %r24
   sub %fp, %r24, %r25
   mov %r25, %r26
   set 4, %r27
   sub %fp, %r27, %r28
   ld [%r28], %r29
   mov %r29, %o0
   call List$GetElem
   nop
   mov %o0, %r16
   st %r16, [%r26]
   ba test046
   nop
Element$Equal:
   save %sp, -4*(24+6+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 12, %r22
   sub %fp, %r22, %r23
   set 0, %r24
   st %r24, [%r23]
   set 16, %r25
   sub %fp, %r25, %r26
   set 0, %r27
   st %r27, [%r26]
   set 20, %r28
   sub %fp, %r28, %r29
   set 0, %r16
   st %r16, [%r29]
   ba Element$Equal$prologueEnd
   nop
Element$Equal$prologueEnd:
   set 8, %r17
   sub %fp, %r17, %r18
   set 1, %r19
   st %r19, [%r18]
   set 12, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   set 4, %r23
   sub %fp, %r23, %r24
   ld [%r24], %r25
   mov %r25, %o0
   call Element$GetAge
   nop
   mov %o0, %r26
   st %r26, [%r22]
   ld [%fp], %r27
   mov %r27, %o0
   set 12, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   mov %r16, %o1
   ld [%fp], %r17
   set 0, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o2
   call Element$Compare
   nop
   mov %o0, %r21
   set 1, %r22
   xor %r21, %r22, %r23
   set 1, %r24
   cmp %r23, %r24
   be t013
   nop
   ba f014
   nop
f014:
   set 16, %r25
   sub %fp, %r25, %r26
   mov %r26, %r27
   set 4, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   mov %r16, %o0
   call Element$GetSalary
   nop
   mov %o0, %r17
   st %r17, [%r27]
   ld [%fp], %r18
   mov %r18, %o0
   set 16, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o1
   ld [%fp], %r22
   set 4, %r23
   add %r22, %r23, %r24
   ld [%r24], %r25
   mov %r25, %o2
   call Element$Compare
   nop
   mov %o0, %r26
   set 1, %r27
   xor %r26, %r27, %r28
   set 1, %r29
   cmp %r28, %r29
   be t010
   nop
   ba f011
   nop
f011:
   ld [%fp], %r16
   set 8, %r17
   add %r16, %r17, %r18
   ld [%r18], %r19
   set 1, %r20
   cmp %r19, %r20
   be t007
   nop
   ba f008
   nop
f008:
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call Element$GetMarried
   nop
   mov %o0, %r24
   set 1, %r25
   cmp %r24, %r25
   be t004
   nop
   ba f005
   nop
f005:
   set 20, %r26
   sub %fp, %r26, %r27
   set 0, %r28
   st %r28, [%r27]
   ba end006
   nop
end006:
   ba end009
   nop
end009:
   ba end012
   nop
end012:
   ba end015
   nop
end015:
   set 8, %r29
   sub %fp, %r29, %r16
   ld [%r16], %r17
   mov %r17, %i0
   ba Element$Equal$epilogueStart
   nop
Element$Equal$epilogueStart:
   ret
   restore
t013:
   set 8, %r18
   sub %fp, %r18, %r19
   set 0, %r20
   st %r20, [%r19]
   ba end015
   nop
t010:
   set 8, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   ba end012
   nop
t007:
   set 4, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   mov %r26, %o0
   call Element$GetMarried
   nop
   mov %o0, %r27
   set 1, %r28
   xor %r27, %r28, %r29
   set 1, %r16
   cmp %r29, %r16
   be t001
   nop
   ba f002
   nop
f002:
   set 20, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   ba end003
   nop
end003:
   ba end009
   nop
t001:
   set 8, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   ba end003
   nop
t004:
   set 8, %r23
   sub %fp, %r23, %r24
   set 0, %r25
   st %r25, [%r24]
   ba end006
   nop
LinkedList$main:
main:
   save %sp, -96, %sp
   set 0, %r17
   mov %r17, %o0
   call initObject
   nop
   mov %o0, %r18
   mov %r18, %o0
   call LL$Start
   nop
   mov %o0, %r19
   mov %r19, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
List$SetNext:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   ba List$SetNext$prologueEnd
   nop
List$SetNext$prologueEnd:
   ld [%fp], %r19
   set 4, %r20
   add %r19, %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r24
   st %r24, [%r21]
   set 1, %r25
   mov %r25, %i0
   ba List$SetNext$epilogueStart
   nop
List$SetNext$epilogueStart:
   ret
   restore
List$GetElem:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba List$GetElem$prologueEnd
   nop
List$GetElem$prologueEnd:
   ld [%fp], %r17
   set 0, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba List$GetElem$epilogueStart
   nop
List$GetElem$epilogueStart:
   ret
   restore
BB$begin050:
   ld [%fp], %r21
   set 0, %r22
   add %r21, %r22, %r23
   ld [%r23], %r24
   mov %r24, %i0
   ba List$GetElem$epilogueStart
   nop
Element$GetAge:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Element$GetAge$prologueEnd
   nop
Element$GetAge$prologueEnd:
   ld [%fp], %r17
   set 0, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Element$GetAge$epilogueStart
   nop
Element$GetAge$epilogueStart:
   ret
   restore
BB$begin051:
   ld [%fp], %r21
   set 0, %r22
   add %r21, %r22, %r23
   ld [%r23], %r24
   mov %r24, %i0
   ba Element$GetAge$epilogueStart
   nop
List$Delete:
   save %sp, -4*(24+11+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 12, %r22
   sub %fp, %r22, %r23
   set 0, %r24
   st %r24, [%r23]
   set 16, %r25
   sub %fp, %r25, %r26
   set 0, %r27
   st %r27, [%r26]
   set 20, %r28
   sub %fp, %r28, %r29
   set 0, %r16
   st %r16, [%r29]
   set 24, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 28, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 32, %r23
   sub %fp, %r23, %r24
   set 0, %r25
   st %r25, [%r24]
   set 36, %r26
   sub %fp, %r26, %r27
   set 0, %r28
   st %r28, [%r27]
   set 40, %r29
   sub %fp, %r29, %r16
   set 0, %r17
   st %r17, [%r16]
   ba List$Delete$prologueEnd
   nop
List$Delete$prologueEnd:
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%fp], %r20
   st %r20, [%r19]
   set 12, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   set 36, %r24
   sub %fp, %r24, %r25
   set 0, %r26
   set 1, %r27
   sub %r26, %r27, %r28
   st %r28, [%r25]
   set 20, %r29
   sub %fp, %r29, %r16
   ld [%fp], %r17
   st %r17, [%r16]
   set 24, %r18
   sub %fp, %r18, %r19
   ld [%fp], %r20
   st %r20, [%r19]
   set 28, %r21
   sub %fp, %r21, %r22
   ld [%fp], %r23
   set 8, %r24
   add %r23, %r24, %r25
   ld [%r25], %r26
   st %r26, [%r22]
   set 32, %r27
   sub %fp, %r27, %r28
   ld [%fp], %r29
   set 0, %r16
   add %r29, %r16, %r17
   ld [%r17], %r18
   st %r18, [%r28]
   ba test037
   nop
test037:
   set 28, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   set 1, %r22
   xor %r21, %r22, %r23
   set 12, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   set 1, %r27
   xor %r26, %r27, %r28
   and %r23, %r28, %r29
   set 1, %r16
   cmp %r29, %r16
   be body038
   nop
   ba end039
   nop
end039:
   set 8, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %i0
   ba List$Delete$epilogueStart
   nop
List$Delete$epilogueStart:
   ret
   restore
body038:
   set 4, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   set 32, %r23
   sub %fp, %r23, %r24
   ld [%r24], %r25
   mov %r25, %o1
   call Element$Equal
   nop
   mov %o0, %r26
   set 1, %r27
   cmp %r26, %r27
   be t031
   nop
   ba f032
   nop
f032:
   set 40, %r28
   sub %fp, %r28, %r29
   set 0, %r16
   st %r16, [%r29]
   ba end033
   nop
end033:
   set 12, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   set 1, %r20
   xor %r19, %r20, %r21
   set 1, %r22
   cmp %r21, %r22
   be t034
   nop
   ba f035
   nop
f035:
   set 40, %r23
   sub %fp, %r23, %r24
   set 0, %r25
   st %r25, [%r24]
   ba end036
   nop
end036:
   ba test037
   nop
t031:
   set 12, %r26
   sub %fp, %r26, %r27
   set 1, %r28
   st %r28, [%r27]
   set 1, %r29
   mov %r29, %r16
   set 36, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   set 0, %r20
   cmp %r19, %r20
   bl t026
   nop
   ba f027
   nop
f027:
   set 0, %r21
   mov %r21, %r16
   ba t026
   nop
t026:
   set 1, %r22
   cmp %r16, %r22
   be t028
   nop
   ba f029
   nop
f029:
   set 0, %r23
   set 555, %r24
   sub %r23, %r24, %r25
   mov %r25, %o0
   call printInt
   nop
   set 16, %r26
   sub %fp, %r26, %r27
   mov %r27, %r28
   set 24, %r29
   sub %fp, %r29, %r16
   ld [%r16], %r17
   mov %r17, %r18
   set 20, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call List$GetNext
   nop
   mov %o0, %r22
   mov %r18, %o0
   mov %r22, %o1
   call List$SetNext
   nop
   mov %o0, %r23
   st %r23, [%r28]
   set 0, %r24
   set 555, %r25
   sub %r24, %r25, %r26
   mov %r26, %o0
   call printInt
   nop
   ba end030
   nop
end030:
   ba end033
   nop
t028:
   set 8, %r27
   sub %fp, %r27, %r28
   mov %r28, %r29
   set 20, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call List$GetNext
   nop
   mov %o0, %r19
   st %r19, [%r29]
   ba end030
   nop
t034:
   set 24, %r20
   sub %fp, %r20, %r21
   set 20, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r24
   st %r24, [%r21]
   set 20, %r25
   sub %fp, %r25, %r26
   mov %r26, %r27
   set 20, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   mov %r16, %o0
   call List$GetNext
   nop
   mov %o0, %r17
   st %r17, [%r27]
   set 28, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 20, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call List$GetEnd
   nop
   mov %o0, %r24
   st %r24, [%r20]
   set 32, %r25
   sub %fp, %r25, %r26
   mov %r26, %r27
   set 20, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   mov %r16, %o0
   call List$GetElem
   nop
   mov %o0, %r17
   st %r17, [%r27]
   set 36, %r18
   sub %fp, %r18, %r19
   set 1, %r20
   st %r20, [%r19]
   ba end036
   nop
List$Init:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba List$Init$prologueEnd
   nop
List$Init$prologueEnd:
   ld [%fp], %r17
   set 8, %r18
   add %r17, %r18, %r19
   set 1, %r20
   st %r20, [%r19]
   set 1, %r21
   mov %r21, %i0
   ba List$Init$epilogueStart
   nop
List$Init$epilogueStart:
   ret
   restore
Element$GetSalary:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Element$GetSalary$prologueEnd
   nop
Element$GetSalary$prologueEnd:
   ld [%fp], %r17
   set 4, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Element$GetSalary$epilogueStart
   nop
Element$GetSalary$epilogueStart:
   ret
   restore
BB$begin052:
   ld [%fp], %r21
   set 4, %r22
   add %r21, %r22, %r23
   ld [%r23], %r24
   mov %r24, %i0
   ba Element$GetSalary$epilogueStart
   nop
List$GetNext:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba List$GetNext$prologueEnd
   nop
List$GetNext$prologueEnd:
   ld [%fp], %r17
   set 4, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba List$GetNext$epilogueStart
   nop
List$GetNext$epilogueStart:
   ret
   restore
BB$begin053:
   ld [%fp], %r21
   set 4, %r22
   add %r21, %r22, %r23
   ld [%r23], %r24
   mov %r24, %i0
   ba List$GetNext$epilogueStart
   nop
List$Insert:
   save %sp, -4*(24+5+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 12, %r22
   sub %fp, %r22, %r23
   set 0, %r24
   st %r24, [%r23]
   set 16, %r25
   sub %fp, %r25, %r26
   set 0, %r27
   st %r27, [%r26]
   ba List$Insert$prologueEnd
   nop
List$Insert$prologueEnd:
   set 12, %r28
   sub %fp, %r28, %r29
   ld [%fp], %r16
   st %r16, [%r29]
   set 16, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 3, %r20
   mov %r20, %o0
   call initObject
   nop
   mov %o0, %r21
   st %r21, [%r19]
   set 8, %r22
   sub %fp, %r22, %r23
   mov %r23, %r24
   set 16, %r25
   sub %fp, %r25, %r26
   ld [%r26], %r27
   mov %r27, %o0
   set 4, %r28
   sub %fp, %r28, %r29
   ld [%r29], %r16
   mov %r16, %o1
   set 12, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o2
   set 0, %r20
   mov %r20, %o3
   call List$InitNew
   nop
   mov %o0, %r21
   st %r21, [%r24]
   set 16, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r24
   mov %r24, %i0
   ba List$Insert$epilogueStart
   nop
List$Insert$epilogueStart:
   ret
   restore
List$GetEnd:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba List$GetEnd$prologueEnd
   nop
List$GetEnd$prologueEnd:
   ld [%fp], %r17
   set 8, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba List$GetEnd$epilogueStart
   nop
List$GetEnd$epilogueStart:
   ret
   restore
BB$begin054:
   ld [%fp], %r21
   set 8, %r22
   add %r21, %r22, %r23
   ld [%r23], %r24
   mov %r24, %i0
   ba List$GetEnd$epilogueStart
   nop
