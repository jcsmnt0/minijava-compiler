   .section ".text"
   .align 4
   .global main
Factorial$main:
main:
   save %sp, -96, %sp
   mov 0, %r17
   mov %r17, %o0
   call initObject
   nop
   mov %o0, %r18
   mov %r18, %o0
   mov 10, %r19
   mov %r19, %o1
   call Fac$ComputeFac
   nop
   mov %o0, %r20
   mov %r20, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
Fac$ComputeFac:
   save %sp, -4*(24+3+0)&-8, %sp
   st %i0, [%fp]
   mov 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   mov 8, %r19
   sub %fp, %r19, %r20
   mov 0, %r21
   st %r21, [%r20]
   ba Fac$ComputeFac$prologueEnd
   nop
Fac$ComputeFac$prologueEnd:
   mov 1, %r22
   mov %r22, %r23
   mov 4, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   mov 1, %r27
   cmp %r26, %r27
   bl t001
   nop
   ba f002
   nop
f002:
   mov 0, %r28
   mov %r28, %r23
   ba t001
   nop
t001:
   mov 1, %r29
   cmp %r23, %r29
   be t003
   nop
   ba f004
   nop
f004:
   mov 8, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   mov 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %r22
   ld [%fp], %r23
   mov %r23, %o0
   mov 4, %r24
   sub %fp, %r24, %r25
   ld [%r25], %r26
   mov 1, %r27
   sub %r26, %r27, %r28
   mov %r28, %o1
   call Fac$ComputeFac
   nop
   mov %o0, %r29
   smul %r22, %r29, %r16
   st %r16, [%r18]
   ba end005
   nop
end005:
   mov 8, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %i0
   ba Fac$ComputeFac$epilogueStart
   nop
Fac$ComputeFac$epilogueStart:
   ret
   restore
t003:
   mov 8, %r20
   sub %fp, %r20, %r21
   mov 1, %r22
   st %r22, [%r21]
   ba end005
   nop
