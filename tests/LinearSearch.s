   .section ".text"
   .align 4
   .global main
LS$Search:
   save %sp, -4*(24+8+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   set 12, %l5
   sub %fp, %l5, %l6
   set 0, %l7
   st %l7, [%l6]
   set 16, %i0
   sub %fp, %i0, %i1
   set 0, %i2
   st %i2, [%i1]
   set 20, %i3
   sub %fp, %i3, %i4
   set 0, %i5
   st %i5, [%i4]
   set 24, %l0
   sub %fp, %l0, %l1
   set 0, %l2
   st %l2, [%l1]
   set 28, %l3
   sub %fp, %l3, %l4
   set 0, %l5
   st %l5, [%l4]
   set 32, %l6
   sub %fp, %l6, %l7
   set 0, %i0
   st %i0, [%l7]
   ba LS$Search$prologueEnd
   nop
LS$Search$prologueEnd:
   set 12, %i1
   sub %fp, %i1, %i2
   set 1, %i3
   st %i3, [%i2]
   set 16, %i4
   sub %fp, %i4, %i5
   set 0, %l0
   st %l0, [%i5]
   set 20, %l1
   sub %fp, %l1, %l2
   set 0, %l3
   st %l3, [%l2]
   ba test018
   nop
test018:
   set 1, %l4
   mov %l4, %l5
   set 12, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 4, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   cmp %i0, %l0
   bl t006
   nop
   ba f007
   nop
f007:
   set 0, %l1
   mov %l1, %l5
   ba t006
   nop
t006:
   set 1, %l2
   cmp %l5, %l2
   be body019
   nop
   ba end020
   nop
end020:
   set 20, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   mov %l5, %i0
   ba LS$Search$epilogueStart
   nop
LS$Search$epilogueStart:
   ret
   restore
body019:
   set 24, %l6
   sub %fp, %l6, %l7
   set 4, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 0, %i3
   add %i2, %i3, %i4
   ld [%i4], %i5
   set 12, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 4, %l3
   mov %l2, %o0
   mov %l3, %o1
   call .mul
   nop
   mov %o0, %l4
   set 4, %l5
   add %l4, %l5, %l6
   add %i5, %l6, %l7
   ld [%l7], %i0
   st %i0, [%l7]
   set 28, %i1
   sub %fp, %i1, %i2
   set 8, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   set 1, %l0
   add %i5, %l0, %l1
   st %l1, [%i2]
   set 1, %l2
   mov %l2, %l3
   set 24, %l4
   sub %fp, %l4, %l5
   ld [%l5], %l6
   set 8, %l7
   sub %fp, %l7, %i0
   ld [%i0], %i1
   cmp %l6, %i1
   bl t008
   nop
   ba f009
   nop
f009:
   set 0, %i2
   mov %i2, %l3
   ba t008
   nop
t008:
   set 1, %i3
   cmp %l3, %i3
   be t015
   nop
   ba f016
   nop
f016:
   set 1, %i4
   mov %i4, %i5
   set 24, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 28, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   cmp %l2, %l5
   bl t010
   nop
   ba f011
   nop
f011:
   set 0, %l6
   mov %l6, %i5
   ba t010
   nop
t010:
   set 1, %l7
   xor %i5, %l7, %i0
   set 1, %i1
   cmp %i0, %i1
   be t012
   nop
   ba f013
   nop
f013:
   set 16, %i2
   sub %fp, %i2, %i3
   set 1, %i4
   st %i4, [%i3]
   set 20, %i5
   sub %fp, %i5, %l0
   set 1, %l1
   st %l1, [%l0]
   set 12, %l2
   sub %fp, %l2, %l3
   set 4, %l4
   sub %fp, %l4, %l5
   ld [%l5], %l6
   set 4, %l7
   add %l6, %l7, %i0
   ld [%i0], %i1
   st %i1, [%l3]
   ba end014
   nop
end014:
   ba end017
   nop
end017:
   set 12, %i2
   sub %fp, %i2, %i3
   set 12, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 1, %l1
   add %l0, %l1, %l2
   st %l2, [%i3]
   ba test018
   nop
t015:
   set 32, %l3
   sub %fp, %l3, %l4
   set 0, %l5
   st %l5, [%l4]
   ba end017
   nop
t012:
   set 32, %l6
   sub %fp, %l6, %l7
   set 0, %i0
   st %i0, [%l7]
   ba end014
   nop
LS$Start:
   save %sp, -4*(24+4+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   set 12, %l5
   sub %fp, %l5, %l6
   set 0, %l7
   st %l7, [%l6]
   set 16, %i0
   sub %fp, %i0, %i1
   set 0, %i2
   st %i2, [%i1]
   ba LS$Start$prologueEnd
   nop
LS$Start$prologueEnd:
   set 12, %i3
   sub %fp, %i3, %i4
   mov %i4, %i5
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   mov %l2, %o0
   set 8, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   mov %l5, %o1
   call LS$Init
   nop
   mov %o0, %l6
   st %l6, [%i5]
   set 16, %l7
   sub %fp, %l7, %i0
   mov %i0, %i1
   set 4, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   mov %i4, %o0
   call LS$Print
   nop
   mov %o0, %i5
   st %i5, [%i1]
   set 9999, %l0
   mov %l0, %o0
   call printInt
   nop
   set 4, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   mov %l3, %o0
   set 8, %l4
   mov %l4, %o1
   call LS$Search
   nop
   mov %o0, %l5
   mov %l5, %o0
   call printInt
   nop
   set 4, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   mov %i0, %o0
   set 12, %i1
   mov %i1, %o1
   call LS$Search
   nop
   mov %o0, %i2
   mov %i2, %o0
   call printInt
   nop
   set 4, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   mov %i5, %o0
   set 17, %l0
   mov %l0, %o1
   call LS$Search
   nop
   mov %o0, %l1
   mov %l1, %o0
   call printInt
   nop
   set 4, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   mov %l4, %o0
   set 50, %l5
   mov %l5, %o1
   call LS$Search
   nop
   mov %o0, %l6
   mov %l6, %o0
   call printInt
   nop
   set 55, %l7
   mov %l7, %i0
   ba LS$Start$epilogueStart
   nop
LS$Start$epilogueStart:
   ret
   restore
LS$Init:
   save %sp, -4*(24+6+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   set 12, %l5
   sub %fp, %l5, %l6
   set 0, %l7
   st %l7, [%l6]
   set 16, %i0
   sub %fp, %i0, %i1
   set 0, %i2
   st %i2, [%i1]
   set 20, %i3
   sub %fp, %i3, %i4
   set 0, %i5
   st %i5, [%i4]
   set 24, %l0
   sub %fp, %l0, %l1
   set 0, %l2
   st %l2, [%l1]
   ba LS$Init$prologueEnd
   nop
LS$Init$prologueEnd:
   set 4, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 4, %l6
   add %l5, %l6, %l7
   set 8, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   st %i2, [%l7]
   set 4, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   set 0, %l0
   add %i5, %l0, %l1
   mov %l1, %l2
   set 8, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   mov %l5, %o0
   call initArray
   nop
   mov %o0, %l6
   st %l6, [%l2]
   set 12, %l7
   sub %fp, %l7, %i0
   set 1, %i1
   st %i1, [%i0]
   set 16, %i2
   sub %fp, %i2, %i3
   set 4, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 4, %l1
   add %l0, %l1, %l2
   ld [%l2], %l3
   set 1, %l4
   add %l3, %l4, %l5
   st %l5, [%i3]
   ba test023
   nop
test023:
   set 1, %l6
   mov %l6, %l7
   set 12, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 4, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   set 4, %l0
   add %i5, %l0, %l1
   ld [%l1], %l2
   cmp %i2, %l2
   bl t021
   nop
   ba f022
   nop
f022:
   set 0, %l3
   mov %l3, %l7
   ba t021
   nop
t021:
   set 1, %l4
   cmp %l7, %l4
   be body024
   nop
   ba end025
   nop
end025:
   set 0, %l5
   mov %l5, %i0
   ba LS$Init$epilogueStart
   nop
LS$Init$epilogueStart:
   ret
   restore
body024:
   set 20, %l6
   sub %fp, %l6, %l7
   set 2, %i0
   set 12, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   mov %i0, %o0
   mov %i3, %o1
   call .mul
   nop
   mov %o0, %i4
   st %i4, [%l7]
   set 24, %i5
   sub %fp, %i5, %l0
   set 16, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 3, %l4
   sub %l3, %l4, %l5
   st %l5, [%l0]
   set 4, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 0, %i1
   add %i0, %i1, %i2
   ld [%i2], %i3
   set 12, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 4, %l1
   mov %l0, %o0
   mov %l1, %o1
   call .mul
   nop
   mov %o0, %l2
   set 4, %l3
   add %l2, %l3, %l4
   add %i3, %l4, %l5
   set 20, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 24, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   add %i0, %i3, %i4
   st %i4, [%l5]
   set 12, %i5
   sub %fp, %i5, %l0
   set 12, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 1, %l4
   add %l3, %l4, %l5
   st %l5, [%l0]
   set 16, %l6
   sub %fp, %l6, %l7
   set 16, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 1, %i3
   sub %i2, %i3, %i4
   st %i4, [%l7]
   ba test023
   nop
LS$Print:
   save %sp, -4*(24+2+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   set 0, %l5
   st %l5, [%l4]
   ba LS$Print$prologueEnd
   nop
LS$Print$prologueEnd:
   set 8, %l6
   sub %fp, %l6, %l7
   set 1, %i0
   st %i0, [%l7]
   ba test003
   nop
test003:
   set 1, %i1
   mov %i1, %i2
   set 8, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 4, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   cmp %i5, %l5
   bl t001
   nop
   ba f002
   nop
f002:
   set 0, %l6
   mov %l6, %i2
   ba t001
   nop
t001:
   set 1, %l7
   cmp %i2, %l7
   be body004
   nop
   ba end005
   nop
end005:
   set 0, %i0
   mov %i0, %i0
   ba LS$Print$epilogueStart
   nop
LS$Print$epilogueStart:
   ret
   restore
body004:
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 8, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 4, %l4
   mov %l3, %o0
   mov %l4, %o1
   call .mul
   nop
   mov %o0, %l5
   set 4, %l6
   add %l5, %l6, %l7
   add %l0, %l7, %i0
   ld [%i0], %i1
   mov %i1, %o0
   call printInt
   nop
   set 8, %i2
   sub %fp, %i2, %i3
   set 8, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 1, %l1
   add %l0, %l1, %l2
   st %l2, [%i3]
   ba test003
   nop
LinearSearch$main:
main:
   save %sp, -96, %sp
   set 2, %l1
   mov %l1, %o0
   call initObject
   nop
   mov %o0, %l2
   mov %l2, %o0
   set 10, %l3
   mov %l3, %o1
   call LS$Start
   nop
   mov %o0, %l4
   mov %l4, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
