   .section ".text"
   .align 4
   .global main
QuickSort$main:
main:
   save %sp, -96, %sp
   set 2, %l1
   mov %l1, %o0
   call initObject
   nop
   mov %o0, %l2
   mov %l2, %o0
   set 10, %l3
   mov %l3, %o1
   call QS$Start
   nop
   mov %o0, %l4
   mov %l4, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
QS$Init:
   save %sp, -4*(24+2+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   ba QS$Init$prologueEnd
   nop
QS$Init$prologueEnd:
   set 4, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   set 4, %i0
   add %l7, %i0, %i1
   set 8, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   st %i4, [%i1]
   set 4, %i5
   sub %fp, %i5, %l0
   ld [%l0], %l1
   set 0, %l2
   add %l1, %l2, %l3
   mov %l3, %l4
   set 8, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   mov %l7, %o0
   call initArray
   nop
   mov %o0, %i0
   st %i0, [%l4]
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 0, %l1
   set 4, %l2
   mov %l1, %o0
   mov %l2, %o1
   call .mul
   nop
   mov %o0, %l3
   set 4, %l4
   add %l3, %l4, %l5
   add %l0, %l5, %l6
   set 20, %l7
   st %l7, [%l6]
   set 4, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 0, %i3
   add %i2, %i3, %i4
   ld [%i4], %i5
   set 1, %l0
   set 4, %l1
   mov %l0, %o0
   mov %l1, %o1
   call .mul
   nop
   mov %o0, %l2
   set 4, %l3
   add %l2, %l3, %l4
   add %i5, %l4, %l5
   set 7, %l6
   st %l6, [%l5]
   set 4, %l7
   sub %fp, %l7, %i0
   ld [%i0], %i1
   set 0, %i2
   add %i1, %i2, %i3
   ld [%i3], %i4
   set 2, %i5
   set 4, %l0
   mov %i5, %o0
   mov %l0, %o1
   call .mul
   nop
   mov %o0, %l1
   set 4, %l2
   add %l1, %l2, %l3
   add %i4, %l3, %l4
   set 12, %l5
   st %l5, [%l4]
   set 4, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 0, %i1
   add %i0, %i1, %i2
   ld [%i2], %i3
   set 3, %i4
   set 4, %i5
   mov %i4, %o0
   mov %i5, %o1
   call .mul
   nop
   mov %o0, %l0
   set 4, %l1
   add %l0, %l1, %l2
   add %i3, %l2, %l3
   set 18, %l4
   st %l4, [%l3]
   set 4, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   set 0, %i0
   add %l7, %i0, %i1
   ld [%i1], %i2
   set 4, %i3
   set 4, %i4
   mov %i3, %o0
   mov %i4, %o1
   call .mul
   nop
   mov %o0, %i5
   set 4, %l0
   add %i5, %l0, %l1
   add %i2, %l1, %l2
   set 2, %l3
   st %l3, [%l2]
   set 4, %l4
   sub %fp, %l4, %l5
   ld [%l5], %l6
   set 0, %l7
   add %l6, %l7, %i0
   ld [%i0], %i1
   set 5, %i2
   set 4, %i3
   mov %i2, %o0
   mov %i3, %o1
   call .mul
   nop
   mov %o0, %i4
   set 4, %i5
   add %i4, %i5, %l0
   add %i1, %l0, %l1
   set 11, %l2
   st %l2, [%l1]
   set 4, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 0, %l6
   add %l5, %l6, %l7
   ld [%l7], %i0
   set 6, %i1
   set 4, %i2
   mov %i1, %o0
   mov %i2, %o1
   call .mul
   nop
   mov %o0, %i3
   set 4, %i4
   add %i3, %i4, %i5
   add %i0, %i5, %l0
   set 6, %l1
   st %l1, [%l0]
   set 4, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 0, %l5
   add %l4, %l5, %l6
   ld [%l6], %l7
   set 7, %i0
   set 4, %i1
   mov %i0, %o0
   mov %i1, %o1
   call .mul
   nop
   mov %o0, %i2
   set 4, %i3
   add %i2, %i3, %i4
   add %l7, %i4, %i5
   set 9, %l0
   st %l0, [%i5]
   set 4, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 0, %l4
   add %l3, %l4, %l5
   ld [%l5], %l6
   set 8, %l7
   set 4, %i0
   mov %l7, %o0
   mov %i0, %o1
   call .mul
   nop
   mov %o0, %i1
   set 4, %i2
   add %i1, %i2, %i3
   add %l6, %i3, %i4
   set 19, %i5
   st %i5, [%i4]
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 0, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   set 9, %l6
   set 4, %l7
   mov %l6, %o0
   mov %l7, %o1
   call .mul
   nop
   mov %o0, %i0
   set 4, %i1
   add %i0, %i1, %i2
   add %l5, %i2, %i3
   set 5, %i4
   st %i4, [%i3]
   set 0, %i5
   mov %i5, %i0
   ba QS$Init$epilogueStart
   nop
QS$Init$epilogueStart:
   ret
   restore
QS$Print:
   save %sp, -4*(24+2+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   set 0, %l5
   st %l5, [%l4]
   ba QS$Print$prologueEnd
   nop
QS$Print$prologueEnd:
   set 8, %l6
   sub %fp, %l6, %l7
   set 0, %i0
   st %i0, [%l7]
   ba test032
   nop
test032:
   set 1, %i1
   mov %i1, %i2
   set 8, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 4, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   cmp %i5, %l5
   bl t030
   nop
   ba f031
   nop
f031:
   set 0, %l6
   mov %l6, %i2
   ba t030
   nop
t030:
   set 1, %l7
   cmp %i2, %l7
   be body033
   nop
   ba end034
   nop
end034:
   set 0, %i0
   mov %i0, %i0
   ba QS$Print$epilogueStart
   nop
QS$Print$epilogueStart:
   ret
   restore
body033:
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 8, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 4, %l4
   mov %l3, %o0
   mov %l4, %o1
   call .mul
   nop
   mov %o0, %l5
   set 4, %l6
   add %l5, %l6, %l7
   add %l0, %l7, %i0
   ld [%i0], %i1
   mov %i1, %o0
   call printInt
   nop
   set 8, %i2
   sub %fp, %i2, %i3
   set 8, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 1, %l1
   add %l0, %l1, %l2
   st %l2, [%i3]
   ba test032
   nop
QS$Start:
   save %sp, -4*(24+3+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   set 12, %l5
   sub %fp, %l5, %l6
   set 0, %l7
   st %l7, [%l6]
   ba QS$Start$prologueEnd
   nop
QS$Start$prologueEnd:
   set 12, %i0
   sub %fp, %i0, %i1
   mov %i1, %i2
   set 4, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   mov %i5, %o0
   set 8, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   mov %l2, %o1
   call QS$Init
   nop
   mov %o0, %l3
   st %l3, [%i2]
   set 12, %l4
   sub %fp, %l4, %l5
   mov %l5, %l6
   set 4, %l7
   sub %fp, %l7, %i0
   ld [%i0], %i1
   mov %i1, %o0
   call QS$Print
   nop
   mov %o0, %i2
   st %i2, [%l6]
   set 9999, %i3
   mov %i3, %o0
   call printInt
   nop
   set 12, %i4
   sub %fp, %i4, %i5
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 4, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   set 1, %l6
   sub %l5, %l6, %l7
   st %l7, [%i5]
   set 12, %i0
   sub %fp, %i0, %i1
   mov %i1, %i2
   set 4, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   mov %i5, %o0
   set 0, %l0
   mov %l0, %o1
   set 12, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   mov %l3, %o2
   call QS$Sort
   nop
   mov %o0, %l4
   st %l4, [%i2]
   set 12, %l5
   sub %fp, %l5, %l6
   mov %l6, %l7
   set 4, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   mov %i2, %o0
   call QS$Print
   nop
   mov %o0, %i3
   st %i3, [%l7]
   set 0, %i4
   mov %i4, %i0
   ba QS$Start$epilogueStart
   nop
QS$Start$epilogueStart:
   ret
   restore
QS$Sort:
   save %sp, -4*(24+11+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   set 12, %l5
   sub %fp, %l5, %l6
   st %i2, [%l6]
   set 16, %l7
   sub %fp, %l7, %i0
   set 0, %i1
   st %i1, [%i0]
   set 20, %i2
   sub %fp, %i2, %i3
   set 0, %i4
   st %i4, [%i3]
   set 24, %i5
   sub %fp, %i5, %l0
   set 0, %l1
   st %l1, [%l0]
   set 28, %l2
   sub %fp, %l2, %l3
   set 0, %l4
   st %l4, [%l3]
   set 32, %l5
   sub %fp, %l5, %l6
   set 0, %l7
   st %l7, [%l6]
   set 36, %i0
   sub %fp, %i0, %i1
   set 0, %i2
   st %i2, [%i1]
   set 40, %i3
   sub %fp, %i3, %i4
   set 0, %i5
   st %i5, [%i4]
   set 44, %l0
   sub %fp, %l0, %l1
   set 0, %l2
   st %l2, [%l1]
   ba QS$Sort$prologueEnd
   nop
QS$Sort$prologueEnd:
   !! [fp-4]: this
   !! [fp-8]: left
   !! [fp-12]: right
   !! [fp-16]: v
   !! [fp-20]: i
   !! [fp-24]: j
   !! [fp-28]: nt
   !! [fp-32]: t
   !! [fp-36]: cont01
   !! [fp-40]: cont02
   !! [fp-44]: aux03
   set 32, %l3       ! l3 = 32
   sub %fp, %l3, %l4 ! l4 = fp-32 (addr of t)
   set 0, %l5        ! l5 = 0
   st %l5, [%l4]     ! t = 0
   set 1, %l6        ! l6 = 1
   mov %l6, %l7      ! l7 = 1
   set 8, %i0        ! i0 = 8
   sub %fp, %i0, %i1 ! i1 = fp-8 (addr of left)
   ld [%i1], %i2     ! i2 = left
   set 12, %i3       ! i3 = 12
   sub %fp, %i3, %i4 ! i4 = fp-12 (addr of right)
   ld [%i4], %i5     ! i5 = right
   cmp %i2, %i5      ! compare left and right
   bl t001           ! if (left < right) goto t001
   nop
   ba f002           ! else goto f002
   nop
f002:
   set 0, %l0
   mov %l0, %l7
   ba t001
   nop
t001:
   set 1, %l1   ! l1 = 1
   cmp %l7, %l1 ! compare l7 and l1
   be t027      ! if (l7 == l1) goto t027
   nop
   ba f028      ! else goto f028
   nop
f028:
   set 28, %l2
   sub %fp, %l2, %l3
   set 0, %l4
   st %l4, [%l3]
   ba end029
   nop
end029:
   set 0, %l5
   mov %l5, %i0
   ba QS$Sort$epilogueStart
   nop
QS$Sort$epilogueStart:
   ret
   restore
t027: !! if ((left < right) == true)
   set 16, %l6       ! l6 = 16
   sub %fp, %l6, %l7 ! l7 = fp-16 (addr of v)
   set 4, %i0        ! i0 = 4
   sub %fp, %i0, %i1 ! i1 = fp-4 (addr of &this)
   ld [%i1], %i2     ! i2 = left
   set 0, %i3        ! i3 = 0
   add %i2, %i3, %i4 ! i4 = addr of &this = addr of number
   ld [%i4], %i5     ! i5 = addr of number
   set 12, %l0       ! l0 = 12
   sub %fp, %l0, %l1 ! l1 = fp-12 (addr of right)
   ld [%l1], %l2     ! l2 = right
   set 4, %l3        ! l3 = 4
   mov %l2, %o0
   mov %l3, %o1
   call .mul         ! l2*l3 = right*4
   nop
   mov %o0, %l4      ! l4 = right*4
   set 4, %l5        ! l5 = 4
   add %l4, %l5, %l6 ! l6 = right*4 + 4
   add %i5, %l6, %l7 ! l7 = number + (right*4 + 4) = &(number[right])
   ld [%l7], %i0     ! i0 = number[right]
   st %i0, [%l7]     ! redundant?
   set 20, %i1       ! i1 = 20
   sub %fp, %i1, %i2 ! i2 = fp-20 (addr of i)
   set 8, %i3        ! i3 = 8
   sub %fp, %i3, %i4 ! i4 = fp-8 (addr of left)
   ld [%i4], %i5     ! i5 = left
   set 1, %l0        ! l0 = 1
   sub %i5, %l0, %l1 ! l1 = left-1
   st %l1, [%i2]     ! i = left-1
   set 24, %l2       ! l2 = 24
   sub %fp, %l2, %l3 ! l3 = fp-24 (addr of j)
   set 12, %l4       ! l4 = 12
   sub %fp, %l4, %l5 ! l5 = fp-12 (addr of right)
   ld [%l5], %l6     ! l6 = right
   st %l6, [%l3]     ! j = right
   set 36, %l7       ! l7 = 36
   sub %fp, %l7, %i0 ! i0 = fp-36 (addr of cont01)
   set 1, %i1        ! i1 = 1
   st %i1, [%i0]     ! cont01 = 1 = true
   ba test024        ! goto test004
   nop
test024:
   set 36, %i2       ! i2 = 36
   sub %fp, %i2, %i3 ! i3 = fp-36 (addr of cont01)
   ld [%i3], %i4     ! i4 = cont01
   set 1, %i5        ! i5 = 1
   cmp %i4, %i5      ! compare i4 and i5
   be body025        ! if (i4 == i5) goto body025
   nop
   ba end026         ! else goto end026
   nop
end026: !! end of while (cont01)
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 0, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   set 24, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 4, %i1
   mov %i0, %o0
   mov %i1, %o1
   call .mul
   nop
   mov %o0, %i2
   set 4, %i3
   add %i2, %i3, %i4
   add %l5, %i4, %i5
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 0, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   set 20, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 4, %i1
   mov %i0, %o0
   mov %i1, %o1
   call .mul
   nop
   mov %o0, %i2
   set 4, %i3
   add %i2, %i3, %i4
   add %l5, %i4, %i5
   ld [%i5], %l0
   st %l0, [%i5]
   set 4, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 0, %l4
   add %l3, %l4, %l5
   ld [%l5], %l6
   set 20, %l7
   sub %fp, %l7, %i0
   ld [%i0], %i1
   set 4, %i2
   mov %i1, %o0
   mov %i2, %o1
   call .mul
   nop
   mov %o0, %i3
   set 4, %i4
   add %i3, %i4, %i5
   add %l6, %i5, %l0
   set 4, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 0, %l4
   add %l3, %l4, %l5
   ld [%l5], %l6
   set 12, %l7
   sub %fp, %l7, %i0
   ld [%i0], %i1
   set 4, %i2
   mov %i1, %o0
   mov %i2, %o1
   call .mul
   nop
   mov %o0, %i3
   set 4, %i4
   add %i3, %i4, %i5
   add %l6, %i5, %l0
   ld [%l0], %l1
   st %l1, [%l0]
   set 4, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 0, %l5
   add %l4, %l5, %l6
   ld [%l6], %l7
   set 12, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 4, %i3
   mov %i2, %o0
   mov %i3, %o1
   call .mul
   nop
   mov %o0, %i4
   set 4, %i5
   add %i4, %i5, %l0
   add %l7, %l0, %l1
   set 32, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   st %l4, [%l1]
   set 28, %l5
   sub %fp, %l5, %l6
   mov %l6, %l7
   set 4, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   mov %i2, %o0
   set 8, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   mov %i5, %o1
   set 20, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 1, %l3
   sub %l2, %l3, %l4
   mov %l4, %o2
   call QS$Sort
   nop
   mov %o0, %l5
   st %l5, [%l7]
   set 28, %l6
   sub %fp, %l6, %l7
   mov %l7, %i0
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   mov %i3, %o0
   set 20, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 1, %l1
   add %l0, %l1, %l2
   mov %l2, %o1
   set 12, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   mov %l5, %o2
   call QS$Sort
   nop
   mov %o0, %l6
   st %l6, [%i0]
   ba end029
   nop
body025: !! body of while (cont01)
   set 40, %l7       ! l7 = 40
   sub %fp, %l7, %i0
   set 1, %i1
   st %i1, [%i0]
   ba test008
   nop
test008:
   set 40, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   set 1, %i5
   cmp %i4, %i5
   be body009
   nop
   ba end010
   nop
end010:
   set 40, %l0
   sub %fp, %l0, %l1
   set 1, %l2
   st %l2, [%l1]
   ba test016
   nop
test016:
   set 40, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 1, %l6
   cmp %l5, %l6
   be body017
   nop
   ba end018
   nop
end018:
   set 32, %l7
   sub %fp, %l7, %i0
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 20, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 4, %l4
   mov %l3, %o0
   mov %l4, %o1
   call .mul
   nop
   mov %o0, %l5
   set 4, %l6
   add %l5, %l6, %l7
   add %l0, %l7, %i0
   ld [%i0], %i1
   st %i1, [%i0]
   set 4, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   set 0, %i5
   add %i4, %i5, %l0
   ld [%l0], %l1
   set 20, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 4, %l5
   mov %l4, %o0
   mov %l5, %o1
   call .mul
   nop
   mov %o0, %l6
   set 4, %l7
   add %l6, %l7, %i0
   add %l1, %i0, %i1
   set 4, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   set 0, %i5
   add %i4, %i5, %l0
   ld [%l0], %l1
   set 24, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 4, %l5
   mov %l4, %o0
   mov %l5, %o1
   call .mul
   nop
   mov %o0, %l6
   set 4, %l7
   add %l6, %l7, %i0
   add %l1, %i0, %i1
   ld [%i1], %i2
   st %i2, [%i1]
   set 4, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   set 0, %l0
   add %i5, %l0, %l1
   ld [%l1], %l2
   set 24, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 4, %l6
   mov %l5, %o0
   mov %l6, %o1
   call .mul
   nop
   mov %o0, %l7
   set 4, %i0
   add %l7, %i0, %i1
   add %l2, %i1, %i2
   set 32, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   st %i5, [%i2]
   set 1, %l0
   mov %l0, %l1
   set 24, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 20, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   set 1, %i0
   add %l7, %i0, %i1
   cmp %l4, %i1
   bl t019
   nop
   ba f020
   nop
f020:
   set 0, %i2
   mov %i2, %l1
   ba t019
   nop
t019:
   set 1, %i3
   cmp %l1, %i3
   be t021
   nop
   ba f022
   nop
f022:
   set 36, %i4
   sub %fp, %i4, %i5
   set 1, %l0
   st %l0, [%i5]
   ba end023
   nop
end023:
   ba test024
   nop
body009:
   set 20, %l1
   sub %fp, %l1, %l2
   set 20, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 1, %l6
   add %l5, %l6, %l7
   st %l7, [%l2]
   set 44, %i0
   sub %fp, %i0, %i1
   set 4, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   set 0, %i5
   add %i4, %i5, %l0
   ld [%l0], %l1
   set 20, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 4, %l5
   mov %l4, %o0
   mov %l5, %o1
   call .mul
   nop
   mov %o0, %l6
   set 4, %l7
   add %l6, %l7, %i0
   add %l1, %i0, %i1
   ld [%i1], %i2
   st %i2, [%i1]
   set 1, %i3
   mov %i3, %i4
   set 44, %i5
   sub %fp, %i5, %l0
   ld [%l0], %l1
   set 16, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   cmp %l1, %l4
   bl t003
   nop
   ba f004
   nop
f004:
   set 0, %l5
   mov %l5, %i4
   ba t003
   nop
t003:
   set 1, %l6
   xor %i4, %l6, %l7
   set 1, %i0
   cmp %l7, %i0
   be t005
   nop
   ba f006
   nop
f006:
   set 40, %i1
   sub %fp, %i1, %i2
   set 1, %i3
   st %i3, [%i2]
   ba end007
   nop
end007:
   ba test008
   nop
t005:
   set 40, %i4
   sub %fp, %i4, %i5
   set 0, %l0
   st %l0, [%i5]
   ba end007
   nop
body017:
   set 24, %l1
   sub %fp, %l1, %l2
   set 24, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 1, %l6
   sub %l5, %l6, %l7
   st %l7, [%l2]
   set 44, %i0
   sub %fp, %i0, %i1
   set 4, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   set 0, %i5
   add %i4, %i5, %l0
   ld [%l0], %l1
   set 24, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 4, %l5
   mov %l4, %o0
   mov %l5, %o1
   call .mul
   nop
   mov %o0, %l6
   set 4, %l7
   add %l6, %l7, %i0
   add %l1, %i0, %i1
   ld [%i1], %i2
   st %i2, [%i1]
   set 1, %i3
   mov %i3, %i4
   set 16, %i5
   sub %fp, %i5, %l0
   ld [%l0], %l1
   set 44, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   cmp %l1, %l4
   bl t011
   nop
   ba f012
   nop
f012:
   set 0, %l5
   mov %l5, %i4
   ba t011
   nop
t011:
   set 1, %l6
   xor %i4, %l6, %l7
   set 1, %i0
   cmp %l7, %i0
   be t013
   nop
   ba f014
   nop
f014:
   set 40, %i1
   sub %fp, %i1, %i2
   set 1, %i3
   st %i3, [%i2]
   ba end015
   nop
end015:
   ba test016
   nop
t013:
   set 40, %i4
   sub %fp, %i4, %i5
   set 0, %l0
   st %l0, [%i5]
   ba end015
   nop
t021:
   set 36, %l1
   sub %fp, %l1, %l2
   set 0, %l3
   st %l3, [%l2]
   ba end023
   nop
