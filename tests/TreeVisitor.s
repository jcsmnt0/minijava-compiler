   .section ".text"
   .align 4
   .global main
Tree$SetLeft:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   ba Tree$SetLeft$prologueEnd
   nop
Tree$SetLeft$prologueEnd:
   ld [%fp], %r19
   set 0, %r20
   add %r19, %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   st %r16, [%r21]
   set 1, %r17
   mov %r17, %i0
   ba Tree$SetLeft$epilogueStart
   nop
Tree$SetLeft$epilogueStart:
   ret
   restore
Tree$GetRight:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Tree$GetRight$prologueEnd
   nop
Tree$GetRight$prologueEnd:
   ld [%fp], %r17
   set 4, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Tree$GetRight$epilogueStart
   nop
Tree$GetRight$epilogueStart:
   ret
   restore
BB$begin105:
   ld [%fp], %r21
   set 4, %r22
   add %r21, %r22, %r23
   ld [%r23], %r16
   mov %r16, %i0
   ba Tree$GetRight$epilogueStart
   nop
Visitor$visit:
   save %sp, -4*(24+3+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba Visitor$visit$prologueEnd
   nop
Visitor$visit$prologueEnd:
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r17
   set 1, %r18
   cmp %r17, %r18
   be t090
   nop
   ba f091
   nop
f091:
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba end092
   nop
end092:
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r17
   set 1, %r18
   cmp %r17, %r18
   be t093
   nop
   ba f094
   nop
f094:
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba end095
   nop
end095:
   set 0, %r22
   mov %r22, %i0
   ba Visitor$visit$epilogueStart
   nop
Visitor$visit$epilogueStart:
   ret
   restore
t090:
   ld [%fp], %r23
   set 4, %r16
   add %r23, %r16, %r17
   mov %r17, %r18
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call Tree$GetRight
   nop
   mov %o0, %r22
   st %r22, [%r18]
   set 8, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   ld [%fp], %r18
   set 4, %r19
   add %r18, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   ld [%fp], %r22
   mov %r22, %o1
   call Tree$accept
   nop
   mov %o0, %r23
   st %r23, [%r17]
   ba end092
   nop
t093:
   ld [%fp], %r16
   set 0, %r17
   add %r16, %r17, %r18
   mov %r18, %r19
   set 4, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r23
   st %r23, [%r19]
   set 8, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   ld [%fp], %r19
   set 0, %r20
   add %r19, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   ld [%fp], %r23
   mov %r23, %o1
   call Tree$accept
   nop
   mov %o0, %r16
   st %r16, [%r18]
   ba end095
   nop
TreeVisitor$main:
main:
   save %sp, -96, %sp
   set 0, %r17
   mov %r17, %o0
   call initObject
   nop
   mov %o0, %r18
   mov %r18, %o0
   call TV$Start
   nop
   mov %o0, %r19
   mov %r19, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
Tree$GetHas_Left:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Tree$GetHas_Left$prologueEnd
   nop
Tree$GetHas_Left$prologueEnd:
   ld [%fp], %r17
   set 12, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Tree$GetHas_Left$epilogueStart
   nop
Tree$GetHas_Left$epilogueStart:
   ret
   restore
BB$begin106:
   ld [%fp], %r21
   set 12, %r22
   add %r21, %r22, %r23
   ld [%r23], %r16
   mov %r16, %i0
   ba Tree$GetHas_Left$epilogueStart
   nop
Tree$SetHas_Right:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   ba Tree$SetHas_Right$prologueEnd
   nop
Tree$SetHas_Right$prologueEnd:
   ld [%fp], %r19
   set 16, %r20
   add %r19, %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   st %r16, [%r21]
   set 1, %r17
   mov %r17, %i0
   ba Tree$SetHas_Right$epilogueStart
   nop
Tree$SetHas_Right$epilogueStart:
   ret
   restore
Tree$SetHas_Left:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   ba Tree$SetHas_Left$prologueEnd
   nop
Tree$SetHas_Left$prologueEnd:
   ld [%fp], %r19
   set 12, %r20
   add %r19, %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   st %r16, [%r21]
   set 1, %r17
   mov %r17, %i0
   ba Tree$SetHas_Left$epilogueStart
   nop
Tree$SetHas_Left$epilogueStart:
   ret
   restore
Tree$GetHas_Right:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Tree$GetHas_Right$prologueEnd
   nop
Tree$GetHas_Right$prologueEnd:
   ld [%fp], %r17
   set 16, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Tree$GetHas_Right$epilogueStart
   nop
Tree$GetHas_Right$epilogueStart:
   ret
   restore
BB$begin102:
   ld [%fp], %r21
   set 16, %r22
   add %r21, %r22, %r23
   ld [%r23], %r16
   mov %r16, %i0
   ba Tree$GetHas_Right$epilogueStart
   nop
Tree$SetRight:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   ba Tree$SetRight$prologueEnd
   nop
Tree$SetRight$prologueEnd:
   ld [%fp], %r19
   set 4, %r20
   add %r19, %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   st %r16, [%r21]
   set 1, %r17
   mov %r17, %i0
   ba Tree$SetRight$epilogueStart
   nop
Tree$SetRight$epilogueStart:
   ret
   restore
Tree$Insert:
   save %sp, -4*(24+7+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 12, %r22
   sub %fp, %r22, %r23
   set 0, %r16
   st %r16, [%r23]
   set 16, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 20, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 24, %r23
   sub %fp, %r23, %r16
   set 0, %r17
   st %r17, [%r16]
   ba Tree$Insert$prologueEnd
   nop
Tree$Insert$prologueEnd:
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 6, %r21
   mov %r21, %o0
   call initObject
   nop
   mov %o0, %r22
   st %r22, [%r20]
   set 12, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o1
   call Tree$Init
   nop
   mov %o0, %r16
   st %r16, [%r17]
   set 16, %r17
   sub %fp, %r17, %r18
   ld [%fp], %r19
   st %r19, [%r18]
   set 20, %r20
   sub %fp, %r20, %r21
   set 1, %r22
   st %r22, [%r21]
   ba test022
   nop
test022:
   set 20, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   set 1, %r18
   cmp %r17, %r18
   be body023
   nop
   ba end024
   nop
end024:
   set 1, %r19
   mov %r19, %i0
   ba Tree$Insert$epilogueStart
   nop
Tree$Insert$epilogueStart:
   ret
   restore
body023:
   set 24, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   set 16, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   call Tree$GetKey
   nop
   mov %o0, %r18
   st %r18, [%r22]
   set 1, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   set 24, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   cmp %r23, %r18
   bl t011
   nop
   ba f012
   nop
f012:
   set 0, %r19
   mov %r19, %r20
   ba t011
   nop
t011:
   set 1, %r20
   cmp %r20, %r20
   be t019
   nop
   ba f020
   nop
f020:
   set 16, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r16
   set 1, %r17
   cmp %r16, %r17
   be t016
   nop
   ba f017
   nop
f017:
   set 20, %r18
   sub %fp, %r18, %r19
   set 0, %r20
   st %r20, [%r19]
   set 12, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 16, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   set 1, %r19
   mov %r19, %o1
   call Tree$SetHas_Right
   nop
   mov %o0, %r20
   st %r20, [%r23]
   set 12, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 16, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   set 8, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o1
   call Tree$SetRight
   nop
   mov %o0, %r22
   st %r22, [%r23]
   ba end018
   nop
end018:
   ba end021
   nop
end021:
   ba test022
   nop
t019:
   set 16, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r18
   set 1, %r19
   cmp %r18, %r19
   be t013
   nop
   ba f014
   nop
f014:
   set 20, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 12, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   set 16, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   set 1, %r21
   mov %r21, %o1
   call Tree$SetHas_Left
   nop
   mov %o0, %r22
   st %r22, [%r17]
   set 12, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   set 16, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   set 8, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o1
   call Tree$SetLeft
   nop
   mov %o0, %r16
   st %r16, [%r17]
   ba end015
   nop
end015:
   ba end021
   nop
t013:
   set 16, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 16, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r23
   st %r23, [%r19]
   ba end015
   nop
t016:
   set 16, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 16, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call Tree$GetRight
   nop
   mov %o0, %r22
   st %r22, [%r18]
   ba end018
   nop
Tree$Search:
   save %sp, -4*(24+6+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 12, %r22
   sub %fp, %r22, %r23
   set 0, %r16
   st %r16, [%r23]
   set 16, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 20, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   ba Tree$Search$prologueEnd
   nop
Tree$Search$prologueEnd:
   set 8, %r23
   sub %fp, %r23, %r16
   ld [%fp], %r17
   st %r17, [%r16]
   set 16, %r18
   sub %fp, %r18, %r19
   set 1, %r20
   st %r20, [%r19]
   set 12, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   ba test081
   nop
test081:
   set 16, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   set 1, %r19
   cmp %r18, %r19
   be body082
   nop
   ba end083
   nop
end083:
   set 12, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %i0
   ba Tree$Search$epilogueStart
   nop
Tree$Search$epilogueStart:
   ret
   restore
body082:
   set 20, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   call Tree$GetKey
   nop
   mov %o0, %r21
   st %r21, [%r17]
   set 1, %r22
   mov %r22, %r23
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   set 20, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   cmp %r18, %r21
   bl t065
   nop
   ba f066
   nop
f066:
   set 0, %r22
   mov %r22, %r23
   ba t065
   nop
t065:
   set 1, %r23
   cmp %r23, %r23
   be t078
   nop
   ba f079
   nop
f079:
   set 1, %r16
   mov %r16, %r17
   set 20, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   cmp %r20, %r23
   bl t070
   nop
   ba f071
   nop
f071:
   set 0, %r16
   mov %r16, %r17
   ba t070
   nop
t070:
   set 1, %r17
   cmp %r17, %r17
   be t075
   nop
   ba f076
   nop
f076:
   set 12, %r18
   sub %fp, %r18, %r19
   set 1, %r20
   st %r20, [%r19]
   set 16, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   ba end077
   nop
end077:
   ba end080
   nop
end080:
   ba test081
   nop
t078:
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r19
   set 1, %r20
   cmp %r19, %r20
   be t067
   nop
   ba f068
   nop
f068:
   set 16, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   ba end069
   nop
end069:
   ba end080
   nop
t067:
   set 8, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 8, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r22
   st %r22, [%r18]
   ba end069
   nop
t075:
   set 8, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r18
   set 1, %r19
   cmp %r18, %r19
   be t072
   nop
   ba f073
   nop
f073:
   set 16, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   ba end074
   nop
end074:
   ba end077
   nop
t072:
   set 8, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   call Tree$GetRight
   nop
   mov %o0, %r21
   st %r21, [%r17]
   ba end074
   nop
Tree$Remove:
   save %sp, -4*(24+6+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   st %i2, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   set 16, %r16
   sub %fp, %r16, %r17
   set 0, %r18
   st %r18, [%r17]
   set 20, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba Tree$Remove$prologueEnd
   nop
Tree$Remove$prologueEnd:
   set 8, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r17
   set 1, %r18
   cmp %r17, %r18
   be t056
   nop
   ba f057
   nop
f057:
   set 8, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r22
   set 1, %r23
   cmp %r22, %r23
   be t053
   nop
   ba f054
   nop
f054:
   set 16, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 8, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   call Tree$GetKey
   nop
   mov %o0, %r22
   st %r22, [%r18]
   set 20, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   set 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r21
   mov %r21, %o0
   call Tree$GetKey
   nop
   mov %o0, %r22
   st %r22, [%r17]
   ld [%fp], %r23
   mov %r23, %o0
   set 16, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o1
   set 20, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o2
   call Tree$Compare
   nop
   mov %o0, %r22
   set 1, %r23
   cmp %r22, %r23
   be t050
   nop
   ba f051
   nop
f051:
   set 12, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   ld [%fp], %r22
   set 20, %r23
   add %r22, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o1
   call Tree$SetRight
   nop
   mov %o0, %r18
   st %r18, [%r18]
   set 12, %r19
   sub %fp, %r19, %r20
   mov %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   set 0, %r17
   mov %r17, %o1
   call Tree$SetHas_Right
   nop
   mov %o0, %r18
   st %r18, [%r21]
   ba end052
   nop
end052:
   ba end055
   nop
end055:
   ba end058
   nop
end058:
   set 1, %r19
   mov %r19, %i0
   ba Tree$Remove$epilogueStart
   nop
Tree$Remove$epilogueStart:
   ret
   restore
t056:
   set 12, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   ld [%fp], %r23
   mov %r23, %o0
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o1
   set 8, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o2
   call Tree$RemoveLeft
   nop
   mov %o0, %r22
   st %r22, [%r22]
   ba end058
   nop
t053:
   set 12, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   ld [%fp], %r18
   mov %r18, %o0
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o1
   set 8, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o2
   call Tree$RemoveRight
   nop
   mov %o0, %r17
   st %r17, [%r17]
   ba end055
   nop
t050:
   set 12, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   ld [%fp], %r16
   set 20, %r17
   add %r16, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o1
   call Tree$SetLeft
   nop
   mov %o0, %r20
   st %r20, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   set 0, %r19
   mov %r19, %o1
   call Tree$SetHas_Left
   nop
   mov %o0, %r20
   st %r20, [%r23]
   ba end052
   nop
Tree$RemoveLeft:
   save %sp, -4*(24+4+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   st %i2, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   ba Tree$RemoveLeft$prologueEnd
   nop
Tree$RemoveLeft$prologueEnd:
   ba test062
   nop
test062:
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r19
   set 1, %r20
   cmp %r19, %r20
   be body063
   nop
   ba end064
   nop
end064:
   set 12, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   ld [%fp], %r19
   set 20, %r20
   add %r19, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o1
   call Tree$SetLeft
   nop
   mov %o0, %r23
   st %r23, [%r23]
   set 12, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   set 0, %r22
   mov %r22, %o1
   call Tree$SetHas_Left
   nop
   mov %o0, %r23
   st %r23, [%r18]
   set 1, %r16
   mov %r16, %i0
   ba Tree$RemoveLeft$epilogueStart
   nop
Tree$RemoveLeft$epilogueStart:
   ret
   restore
body063:
   set 12, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 8, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %r23
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r19
   mov %r19, %o0
   call Tree$GetKey
   nop
   mov %o0, %r20
   mov %r23, %o0
   mov %r20, %o1
   call Tree$SetKey
   nop
   mov %o0, %r21
   st %r21, [%r19]
   set 4, %r22
   sub %fp, %r22, %r23
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   st %r18, [%r23]
   set 8, %r19
   sub %fp, %r19, %r20
   mov %r20, %r21
   set 8, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r17
   st %r17, [%r21]
   ba test062
   nop
MyVisitor$visit:
   save %sp, -4*(24+3+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba MyVisitor$visit$prologueEnd
   nop
MyVisitor$visit$prologueEnd:
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r17
   set 1, %r18
   cmp %r17, %r18
   be t096
   nop
   ba f097
   nop
f097:
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba end098
   nop
end098:
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetKey
   nop
   mov %o0, %r17
   mov %r17, %o0
   call printInt
   nop
   set 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r21
   set 1, %r22
   cmp %r21, %r22
   be t099
   nop
   ba f100
   nop
f100:
   set 8, %r23
   sub %fp, %r23, %r16
   set 0, %r17
   st %r17, [%r16]
   ba end101
   nop
end101:
   set 0, %r18
   mov %r18, %i0
   ba MyVisitor$visit$epilogueStart
   nop
MyVisitor$visit$epilogueStart:
   ret
   restore
t096:
   ld [%fp], %r19
   set -4, %r20
   add %r19, %r20, %r21
   mov %r21, %r22
   set 4, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   call Tree$GetRight
   nop
   mov %o0, %r18
   st %r18, [%r22]
   set 8, %r19
   sub %fp, %r19, %r20
   mov %r20, %r21
   ld [%fp], %r22
   set -4, %r23
   add %r22, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   ld [%fp], %r18
   mov %r18, %o1
   call Tree$accept
   nop
   mov %o0, %r19
   st %r19, [%r21]
   ba end098
   nop
t099:
   ld [%fp], %r20
   set -4, %r21
   add %r20, %r21, %r22
   mov %r22, %r23
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r19
   st %r19, [%r23]
   set 8, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   ld [%fp], %r23
   set -4, %r16
   add %r23, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   ld [%fp], %r19
   mov %r19, %o1
   call Tree$accept
   nop
   mov %o0, %r20
   st %r20, [%r22]
   ba end101
   nop
Tree$GetKey:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Tree$GetKey$prologueEnd
   nop
Tree$GetKey$prologueEnd:
   ld [%fp], %r17
   set 8, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Tree$GetKey$epilogueStart
   nop
Tree$GetKey$epilogueStart:
   ret
   restore
BB$begin103:
   ld [%fp], %r21
   set 8, %r22
   add %r21, %r22, %r23
   ld [%r23], %r16
   mov %r16, %i0
   ba Tree$GetKey$epilogueStart
   nop
Tree$RemoveRight:
   save %sp, -4*(24+4+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   st %i2, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   ba Tree$RemoveRight$prologueEnd
   nop
Tree$RemoveRight$prologueEnd:
   ba test059
   nop
test059:
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r19
   set 1, %r20
   cmp %r19, %r20
   be body060
   nop
   ba end061
   nop
end061:
   set 12, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   ld [%fp], %r19
   set 20, %r20
   add %r19, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o1
   call Tree$SetRight
   nop
   mov %o0, %r23
   st %r23, [%r23]
   set 12, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   set 0, %r22
   mov %r22, %o1
   call Tree$SetHas_Right
   nop
   mov %o0, %r23
   st %r23, [%r18]
   set 1, %r16
   mov %r16, %i0
   ba Tree$RemoveRight$epilogueStart
   nop
Tree$RemoveRight$epilogueStart:
   ret
   restore
body060:
   set 12, %r17
   sub %fp, %r17, %r18
   mov %r18, %r19
   set 8, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %r23
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetRight
   nop
   mov %o0, %r19
   mov %r19, %o0
   call Tree$GetKey
   nop
   mov %o0, %r20
   mov %r23, %o0
   mov %r20, %o1
   call Tree$SetKey
   nop
   mov %o0, %r21
   st %r21, [%r19]
   set 4, %r22
   sub %fp, %r22, %r23
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   st %r18, [%r23]
   set 8, %r19
   sub %fp, %r19, %r20
   mov %r20, %r21
   set 8, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetRight
   nop
   mov %o0, %r17
   st %r17, [%r21]
   ba test059
   nop
Tree$GetLeft:
   save %sp, -4*(24+1+0)&-8, %sp
   st %i0, [%fp]
   ba Tree$GetLeft$prologueEnd
   nop
Tree$GetLeft$prologueEnd:
   ld [%fp], %r17
   set 0, %r18
   add %r17, %r18, %r19
   ld [%r19], %r20
   mov %r20, %i0
   ba Tree$GetLeft$epilogueStart
   nop
Tree$GetLeft$epilogueStart:
   ret
   restore
BB$begin104:
   ld [%fp], %r21
   set 0, %r22
   add %r21, %r22, %r23
   ld [%r23], %r16
   mov %r16, %i0
   ba Tree$GetLeft$epilogueStart
   nop
Tree$Compare:
   save %sp, -4*(24+5+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   st %i2, [%r20]
   set 12, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   set 16, %r16
   sub %fp, %r16, %r17
   set 0, %r18
   st %r18, [%r17]
   ba Tree$Compare$prologueEnd
   nop
Tree$Compare$prologueEnd:
   set 12, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 16, %r22
   sub %fp, %r22, %r23
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   set 1, %r19
   add %r18, %r19, %r20
   st %r20, [%r23]
   set 1, %r21
   mov %r21, %r22
   set 4, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   set 8, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   cmp %r17, %r20
   bl t001
   nop
   ba f002
   nop
f002:
   set 0, %r21
   mov %r21, %r22
   ba t001
   nop
t001:
   set 1, %r22
   cmp %r22, %r22
   be t008
   nop
   ba f009
   nop
f009:
   set 1, %r23
   mov %r23, %r16
   set 4, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   set 16, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   cmp %r19, %r22
   bl t003
   nop
   ba f004
   nop
f004:
   set 0, %r23
   mov %r23, %r16
   ba t003
   nop
t003:
   set 1, %r16
   xor %r16, %r16, %r17
   set 1, %r18
   cmp %r17, %r18
   be t005
   nop
   ba f006
   nop
f006:
   set 12, %r19
   sub %fp, %r19, %r20
   set 1, %r21
   st %r21, [%r20]
   ba end007
   nop
end007:
   ba end010
   nop
end010:
   set 12, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %i0
   ba Tree$Compare$epilogueStart
   nop
Tree$Compare$epilogueStart:
   ret
   restore
t008:
   set 12, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   ba end010
   nop
t005:
   set 12, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   ba end007
   nop
Tree$Delete:
   save %sp, -4*(24+9+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   set 12, %r22
   sub %fp, %r22, %r23
   set 0, %r16
   st %r16, [%r23]
   set 16, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 20, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 24, %r23
   sub %fp, %r23, %r16
   set 0, %r17
   st %r17, [%r16]
   set 28, %r18
   sub %fp, %r18, %r19
   set 0, %r20
   st %r20, [%r19]
   set 32, %r21
   sub %fp, %r21, %r22
   set 0, %r23
   st %r23, [%r22]
   ba Tree$Delete$prologueEnd
   nop
Tree$Delete$prologueEnd:
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%fp], %r18
   st %r18, [%r17]
   set 12, %r19
   sub %fp, %r19, %r20
   ld [%fp], %r21
   st %r21, [%r20]
   set 16, %r22
   sub %fp, %r22, %r23
   set 1, %r16
   st %r16, [%r23]
   set 20, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 28, %r20
   sub %fp, %r20, %r21
   set 1, %r22
   st %r22, [%r21]
   ba test047
   nop
test047:
   set 16, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   set 1, %r18
   cmp %r17, %r18
   be body048
   nop
   ba end049
   nop
end049:
   set 20, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %i0
   ba Tree$Delete$epilogueStart
   nop
Tree$Delete$epilogueStart:
   ret
   restore
body048:
   set 32, %r22
   sub %fp, %r22, %r23
   mov %r23, %r16
   set 8, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o0
   call Tree$GetKey
   nop
   mov %o0, %r20
   st %r20, [%r16]
   set 1, %r21
   mov %r21, %r22
   set 4, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   set 32, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   cmp %r17, %r20
   bl t025
   nop
   ba f026
   nop
f026:
   set 0, %r21
   mov %r21, %r22
   ba t025
   nop
t025:
   set 1, %r22
   cmp %r22, %r22
   be t044
   nop
   ba f045
   nop
f045:
   set 1, %r23
   mov %r23, %r16
   set 32, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   set 4, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   cmp %r19, %r22
   bl t030
   nop
   ba f031
   nop
f031:
   set 0, %r23
   mov %r23, %r16
   ba t030
   nop
t030:
   set 1, %r16
   cmp %r16, %r16
   be t041
   nop
   ba f042
   nop
f042:
   set 28, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   set 1, %r20
   cmp %r19, %r20
   be t038
   nop
   ba f039
   nop
f039:
   set 24, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   ld [%fp], %r16
   mov %r16, %o0
   set 12, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o1
   set 8, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o2
   call Tree$Remove
   nop
   mov %o0, %r23
   st %r23, [%r23]
   ba end040
   nop
end040:
   set 20, %r16
   sub %fp, %r16, %r17
   set 1, %r18
   st %r18, [%r17]
   set 16, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba end043
   nop
end043:
   ba end046
   nop
end046:
   set 28, %r22
   sub %fp, %r22, %r23
   set 0, %r16
   st %r16, [%r23]
   ba test047
   nop
t044:
   set 8, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r20
   set 1, %r21
   cmp %r20, %r21
   be t027
   nop
   ba f028
   nop
f028:
   set 16, %r22
   sub %fp, %r22, %r23
   set 0, %r16
   st %r16, [%r23]
   ba end029
   nop
end029:
   ba end046
   nop
t027:
   set 12, %r17
   sub %fp, %r17, %r18
   set 8, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   st %r21, [%r18]
   set 8, %r22
   sub %fp, %r22, %r23
   mov %r23, %r16
   set 8, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r20
   st %r20, [%r16]
   ba end029
   nop
t041:
   set 8, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r16
   set 1, %r17
   cmp %r16, %r17
   be t032
   nop
   ba f033
   nop
f033:
   set 16, %r18
   sub %fp, %r18, %r19
   set 0, %r20
   st %r20, [%r19]
   ba end034
   nop
end034:
   ba end043
   nop
t032:
   set 12, %r21
   sub %fp, %r21, %r22
   set 8, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   st %r17, [%r22]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 8, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call Tree$GetRight
   nop
   mov %o0, %r16
   st %r16, [%r20]
   ba end034
   nop
t038:
   set 8, %r17
   sub %fp, %r17, %r18
   ld [%r18], %r19
   mov %r19, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r20
   set 1, %r21
   xor %r20, %r21, %r22
   mov %r22, %r23
   set 8, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r19
   set 1, %r20
   xor %r19, %r20, %r21
   and %r23, %r21, %r22
   set 1, %r23
   cmp %r22, %r23
   be t035
   nop
   ba f036
   nop
f036:
   set 24, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   ld [%fp], %r19
   mov %r19, %o0
   set 12, %r20
   sub %fp, %r20, %r21
   ld [%r21], %r22
   mov %r22, %o1
   set 8, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o2
   call Tree$Remove
   nop
   mov %o0, %r18
   st %r18, [%r18]
   ba end037
   nop
end037:
   ba end040
   nop
t035:
   set 24, %r19
   sub %fp, %r19, %r20
   set 1, %r21
   st %r21, [%r20]
   ba end037
   nop
TV$Start:
   save %sp, -4*(24+5+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 8, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   set 12, %r23
   sub %fp, %r23, %r16
   set 0, %r17
   st %r17, [%r16]
   set 16, %r18
   sub %fp, %r18, %r19
   set 0, %r20
   st %r20, [%r19]
   ba TV$Start$prologueEnd
   nop
TV$Start$prologueEnd:
   set 4, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   set 6, %r16
   mov %r16, %o0
   call initObject
   nop
   mov %o0, %r17
   st %r17, [%r23]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 16, %r16
   mov %r16, %o1
   call Tree$Init
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call Tree$Print
   nop
   mov %o0, %r16
   st %r16, [%r20]
   set 100000000, %r17
   mov %r17, %o0
   call printInt
   nop
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 8, %r16
   mov %r16, %o1
   call Tree$Insert
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 24, %r16
   mov %r16, %o1
   call Tree$Insert
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 4, %r16
   mov %r16, %o1
   call Tree$Insert
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 12, %r16
   mov %r16, %o1
   call Tree$Insert
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 20, %r16
   mov %r16, %o1
   call Tree$Insert
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 28, %r16
   mov %r16, %o1
   call Tree$Insert
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 14, %r16
   mov %r16, %o1
   call Tree$Insert
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 8, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   call Tree$Print
   nop
   mov %o0, %r16
   st %r16, [%r20]
   set 100000000, %r17
   mov %r17, %o0
   call printInt
   nop
   set 16, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   set 0, %r21
   mov %r21, %o0
   call initObject
   nop
   mov %o0, %r22
   st %r22, [%r20]
   set 50000000, %r23
   mov %r23, %o0
   call printInt
   nop
   set 12, %r16
   sub %fp, %r16, %r17
   mov %r17, %r18
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   set 16, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o1
   call Tree$accept
   nop
   mov %o0, %r17
   st %r17, [%r18]
   set 100000000, %r18
   mov %r18, %o0
   call printInt
   nop
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   set 24, %r22
   mov %r22, %o1
   call Tree$Search
   nop
   mov %o0, %r23
   mov %r23, %o0
   call printInt
   nop
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   set 12, %r19
   mov %r19, %o1
   call Tree$Search
   nop
   mov %o0, %r20
   mov %r20, %o0
   call printInt
   nop
   set 4, %r21
   sub %fp, %r21, %r22
   ld [%r22], %r23
   mov %r23, %o0
   set 16, %r16
   mov %r16, %o1
   call Tree$Search
   nop
   mov %o0, %r17
   mov %r17, %o0
   call printInt
   nop
   set 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   set 50, %r21
   mov %r21, %o1
   call Tree$Search
   nop
   mov %o0, %r22
   mov %r22, %o0
   call printInt
   nop
   set 4, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   set 12, %r18
   mov %r18, %o1
   call Tree$Search
   nop
   mov %o0, %r19
   mov %r19, %o0
   call printInt
   nop
   set 8, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   set 4, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   set 12, %r18
   mov %r18, %o1
   call Tree$Delete
   nop
   mov %o0, %r19
   st %r19, [%r22]
   set 8, %r20
   sub %fp, %r20, %r21
   mov %r21, %r22
   set 4, %r23
   sub %fp, %r23, %r16
   ld [%r16], %r17
   mov %r17, %o0
   call Tree$Print
   nop
   mov %o0, %r18
   st %r18, [%r22]
   set 4, %r19
   sub %fp, %r19, %r20
   ld [%r20], %r21
   mov %r21, %o0
   set 12, %r22
   mov %r22, %o1
   call Tree$Search
   nop
   mov %o0, %r23
   mov %r23, %o0
   call printInt
   nop
   set 0, %r16
   mov %r16, %i0
   ba TV$Start$epilogueStart
   nop
TV$Start$epilogueStart:
   ret
   restore
Tree$Init:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   ba Tree$Init$prologueEnd
   nop
Tree$Init$prologueEnd:
   ld [%fp], %r19
   set 8, %r20
   add %r19, %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   st %r16, [%r21]
   ld [%fp], %r17
   set 12, %r18
   add %r17, %r18, %r19
   set 0, %r20
   st %r20, [%r19]
   ld [%fp], %r21
   set 16, %r22
   add %r21, %r22, %r23
   set 0, %r16
   st %r16, [%r23]
   set 1, %r17
   mov %r17, %i0
   ba Tree$Init$epilogueStart
   nop
Tree$Init$epilogueStart:
   ret
   restore
Tree$Print:
   save %sp, -4*(24+3+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   set 0, %r19
   st %r19, [%r18]
   set 8, %r20
   sub %fp, %r20, %r21
   set 0, %r22
   st %r22, [%r21]
   ba Tree$Print$prologueEnd
   nop
Tree$Print$prologueEnd:
   set 8, %r23
   sub %fp, %r23, %r16
   ld [%fp], %r17
   st %r17, [%r16]
   set 4, %r18
   sub %fp, %r18, %r19
   mov %r19, %r20
   ld [%fp], %r21
   mov %r21, %o0
   set 8, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o1
   call Tree$RecPrint
   nop
   mov %o0, %r17
   st %r17, [%r20]
   set 1, %r18
   mov %r18, %i0
   ba Tree$Print$epilogueStart
   nop
Tree$Print$epilogueStart:
   ret
   restore
Tree$RecPrint:
   save %sp, -4*(24+3+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba Tree$RecPrint$prologueEnd
   nop
Tree$RecPrint$prologueEnd:
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetHas_Left
   nop
   mov %o0, %r17
   set 1, %r18
   cmp %r17, %r18
   be t084
   nop
   ba f085
   nop
f085:
   set 8, %r19
   sub %fp, %r19, %r20
   set 1, %r21
   st %r21, [%r20]
   ba end086
   nop
end086:
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   mov %r16, %o0
   call Tree$GetKey
   nop
   mov %o0, %r17
   mov %r17, %o0
   call printInt
   nop
   set 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   call Tree$GetHas_Right
   nop
   mov %o0, %r21
   set 1, %r22
   cmp %r21, %r22
   be t087
   nop
   ba f088
   nop
f088:
   set 8, %r23
   sub %fp, %r23, %r16
   set 1, %r17
   st %r17, [%r16]
   ba end089
   nop
end089:
   set 1, %r18
   mov %r18, %i0
   ba Tree$RecPrint$epilogueStart
   nop
Tree$RecPrint$epilogueStart:
   ret
   restore
t084:
   set 8, %r19
   sub %fp, %r19, %r20
   mov %r20, %r21
   ld [%fp], %r22
   mov %r22, %r23
   set 4, %r16
   sub %fp, %r16, %r17
   ld [%r17], %r18
   mov %r18, %o0
   call Tree$GetLeft
   nop
   mov %o0, %r19
   mov %r23, %o0
   mov %r19, %o1
   call Tree$RecPrint
   nop
   mov %o0, %r20
   st %r20, [%r21]
   ba end086
   nop
t087:
   set 8, %r21
   sub %fp, %r21, %r22
   mov %r22, %r23
   ld [%fp], %r16
   mov %r16, %r17
   set 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   call Tree$GetRight
   nop
   mov %o0, %r21
   mov %r17, %o0
   mov %r21, %o1
   call Tree$RecPrint
   nop
   mov %o0, %r22
   st %r22, [%r23]
   ba end089
   nop
Tree$accept:
   save %sp, -4*(24+3+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   set 8, %r19
   sub %fp, %r19, %r20
   set 0, %r21
   st %r21, [%r20]
   ba Tree$accept$prologueEnd
   nop
Tree$accept$prologueEnd:
   set 333, %r22
   mov %r22, %o0
   call printInt
   nop
   set 8, %r23
   sub %fp, %r23, %r16
   mov %r16, %r17
   set 4, %r18
   sub %fp, %r18, %r19
   ld [%r19], %r20
   mov %r20, %o0
   ld [%fp], %r21
   mov %r21, %o1
   call Visitor$visit
   nop
   mov %o0, %r22
   st %r22, [%r17]
   set 0, %r23
   mov %r23, %i0
   ba Tree$accept$epilogueStart
   nop
Tree$accept$epilogueStart:
   ret
   restore
Tree$SetKey:
   save %sp, -4*(24+2+0)&-8, %sp
   st %i0, [%fp]
   set 4, %r17
   sub %fp, %r17, %r18
   st %i1, [%r18]
   ba Tree$SetKey$prologueEnd
   nop
Tree$SetKey$prologueEnd:
   ld [%fp], %r19
   set 8, %r20
   add %r19, %r20, %r21
   set 4, %r22
   sub %fp, %r22, %r23
   ld [%r23], %r16
   st %r16, [%r21]
   set 1, %r17
   mov %r17, %i0
   ba Tree$SetKey$epilogueStart
   nop
Tree$SetKey$epilogueStart:
   ret
   restore
