   .section ".text"
   .align 4
   .global main
BBS$Sort:
   save %sp, -4*(24+10+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   set 0, %l5
   st %l5, [%l4]
   set 12, %l6
   sub %fp, %l6, %l7
   set 0, %i0
   st %i0, [%l7]
   set 16, %i1
   sub %fp, %i1, %i2
   set 0, %i3
   st %i3, [%i2]
   set 20, %i4
   sub %fp, %i4, %i5
   set 0, %l0
   st %l0, [%i5]
   set 24, %l1
   sub %fp, %l1, %l2
   set 0, %l3
   st %l3, [%l2]
   set 28, %l4
   sub %fp, %l4, %l5
   set 0, %l6
   st %l6, [%l5]
   set 32, %l7
   sub %fp, %l7, %i0
   set 0, %i1
   st %i1, [%i0]
   set 36, %i2
   sub %fp, %i2, %i3
   set 0, %i4
   st %i4, [%i3]
   set 40, %i5
   sub %fp, %i5, %l0
   set 0, %l1
   st %l1, [%l0]
   ba BBS$Sort$prologueEnd
   nop
BBS$Sort$prologueEnd:
   set 12, %l2
   sub %fp, %l2, %l3
   set 4, %l4
   sub %fp, %l4, %l5
   ld [%l5], %l6
   set 4, %l7
   add %l6, %l7, %i0
   ld [%i0], %i1
   set 1, %i2
   sub %i1, %i2, %i3
   st %i3, [%l3]
   set 16, %i4
   sub %fp, %i4, %i5
   set 0, %l0
   set 1, %l1
   sub %l0, %l1, %l2
   st %l2, [%i5]
   ba test013
   nop
test013:
   set 1, %l3
   mov %l3, %l4
   set 16, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   set 12, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   cmp %l7, %i2
   bl t001
   nop
   ba f002
   nop
f002:
   set 0, %i3
   mov %i3, %l4
   ba t001
   nop
t001:
   set 1, %i4
   cmp %l4, %i4
   be body014
   nop
   ba end015
   nop
end015:
   set 0, %i5
   mov %i5, %i0
   ba BBS$Sort$epilogueStart
   nop
BBS$Sort$epilogueStart:
   ret
   restore
body014:
   set 36, %l0
   sub %fp, %l0, %l1
   set 1, %l2
   st %l2, [%l1]
   ba test010
   nop
test010:
   set 1, %l3
   mov %l3, %l4
   set 36, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   set 12, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 1, %i3
   add %i2, %i3, %i4
   cmp %l7, %i4
   bl t003
   nop
   ba f004
   nop
f004:
   set 0, %i5
   mov %i5, %l4
   ba t003
   nop
t003:
   set 1, %l0
   cmp %l4, %l0
   be body011
   nop
   ba end012
   nop
end012:
   set 12, %l1
   sub %fp, %l1, %l2
   set 12, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 1, %l6
   sub %l5, %l6, %l7
   st %l7, [%l2]
   ba test013
   nop
body011:
   set 32, %i0
   sub %fp, %i0, %i1
   set 36, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   set 1, %i5
   sub %i4, %i5, %l0
   st %l0, [%i1]
   set 20, %l1
   sub %fp, %l1, %l2
   set 4, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 0, %l6
   add %l5, %l6, %l7
   ld [%l7], %i0
   set 32, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 4, %i4
   mov %i3, %o0
   mov %i4, %o1
   call .mul
   nop
   mov %o0, %i5
   set 4, %l0
   add %i5, %l0, %l1
   add %i0, %l1, %l2
   ld [%l2], %l3
   st %l3, [%l2]
   set 24, %l4
   sub %fp, %l4, %l5
   set 4, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 0, %i1
   add %i0, %i1, %i2
   ld [%i2], %i3
   set 36, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 4, %l1
   mov %l0, %o0
   mov %l1, %o1
   call .mul
   nop
   mov %o0, %l2
   set 4, %l3
   add %l2, %l3, %l4
   add %i3, %l4, %l5
   ld [%l5], %l6
   st %l6, [%l5]
   set 1, %l7
   mov %l7, %i0
   set 24, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 20, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   cmp %i3, %l0
   bl t005
   nop
   ba f006
   nop
f006:
   set 0, %l1
   mov %l1, %i0
   ba t005
   nop
t005:
   set 1, %l2
   cmp %i0, %l2
   be t007
   nop
   ba f008
   nop
f008:
   set 8, %l3
   sub %fp, %l3, %l4
   set 0, %l5
   st %l5, [%l4]
   ba end009
   nop
end009:
   set 36, %l6
   sub %fp, %l6, %l7
   set 36, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 1, %i3
   add %i2, %i3, %i4
   st %i4, [%l7]
   ba test010
   nop
t007:
   set 28, %i5
   sub %fp, %i5, %l0
   set 36, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 1, %l4
   sub %l3, %l4, %l5
   st %l5, [%l0]
   set 40, %l6
   sub %fp, %l6, %l7
   set 4, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 0, %i3
   add %i2, %i3, %i4
   ld [%i4], %i5
   set 28, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 4, %l3
   mov %l2, %o0
   mov %l3, %o1
   call .mul
   nop
   mov %o0, %l4
   set 4, %l5
   add %l4, %l5, %l6
   add %i5, %l6, %l7
   ld [%l7], %i0
   st %i0, [%l7]
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 28, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 4, %l4
   mov %l3, %o0
   mov %l4, %o1
   call .mul
   nop
   mov %o0, %l5
   set 4, %l6
   add %l5, %l6, %l7
   add %l0, %l7, %i0
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 36, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 4, %l4
   mov %l3, %o0
   mov %l4, %o1
   call .mul
   nop
   mov %o0, %l5
   set 4, %l6
   add %l5, %l6, %l7
   add %l0, %l7, %i0
   ld [%i0], %i1
   st %i1, [%i0]
   set 4, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   set 0, %i5
   add %i4, %i5, %l0
   ld [%l0], %l1
   set 36, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 4, %l5
   mov %l4, %o0
   mov %l5, %o1
   call .mul
   nop
   mov %o0, %l6
   set 4, %l7
   add %l6, %l7, %i0
   add %l1, %i0, %i1
   set 40, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   st %i4, [%i1]
   ba end009
   nop
BubbleSort$main:
main:
   save %sp, -96, %sp
   set 2, %l1
   mov %l1, %o0
   call initObject
   nop
   mov %o0, %l2
   mov %l2, %o0
   set 10, %l3
   mov %l3, %o1
   call BBS$Start
   nop
   mov %o0, %l4
   mov %l4, %o0
   call printInt
   nop
   ba $$mainEnd
   nop
$$mainEnd:
   clr %o0
   mov 1, %g1
   ta 0
BBS$Print:
   save %sp, -4*(24+2+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   set 0, %l5
   st %l5, [%l4]
   ba BBS$Print$prologueEnd
   nop
BBS$Print$prologueEnd:
   set 8, %l6
   sub %fp, %l6, %l7
   set 0, %i0
   st %i0, [%l7]
   ba test018
   nop
test018:
   set 1, %i1
   mov %i1, %i2
   set 8, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 4, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   cmp %i5, %l5
   bl t016
   nop
   ba f017
   nop
f017:
   set 0, %l6
   mov %l6, %i2
   ba t016
   nop
t016:
   set 1, %l7
   cmp %i2, %l7
   be body019
   nop
   ba end020
   nop
end020:
   set 0, %i0
   mov %i0, %i0
   ba BBS$Print$epilogueStart
   nop
BBS$Print$epilogueStart:
   ret
   restore
body019:
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 8, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 4, %l4
   mov %l3, %o0
   mov %l4, %o1
   call .mul
   nop
   mov %o0, %l5
   set 4, %l6
   add %l5, %l6, %l7
   add %l0, %l7, %i0
   ld [%i0], %i1
   mov %i1, %o0
   call printInt
   nop
   set 8, %i2
   sub %fp, %i2, %i3
   set 8, %i4
   sub %fp, %i4, %i5
   ld [%i5], %l0
   set 1, %l1
   add %l0, %l1, %l2
   st %l2, [%i3]
   ba test018
   nop
BBS$Init:
   save %sp, -4*(24+2+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   ba BBS$Init$prologueEnd
   nop
BBS$Init$prologueEnd:
   set 4, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   set 4, %i0
   add %l7, %i0, %i1
   set 8, %i2
   sub %fp, %i2, %i3
   ld [%i3], %i4
   st %i4, [%i1]
   set 4, %i5
   sub %fp, %i5, %l0
   ld [%l0], %l1
   set 0, %l2
   add %l1, %l2, %l3
   mov %l3, %l4
   set 8, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   mov %l7, %o0
   call initArray
   nop
   mov %o0, %i0
   st %i0, [%l4]
   set 4, %i1
   sub %fp, %i1, %i2
   ld [%i2], %i3
   set 0, %i4
   add %i3, %i4, %i5
   ld [%i5], %l0
   set 0, %l1
   set 4, %l2
   mov %l1, %o0
   mov %l2, %o1
   call .mul
   nop
   mov %o0, %l3
   set 4, %l4
   add %l3, %l4, %l5
   add %l0, %l5, %l6
   set 20, %l7
   st %l7, [%l6]
   set 4, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   set 0, %i3
   add %i2, %i3, %i4
   ld [%i4], %i5
   set 1, %l0
   set 4, %l1
   mov %l0, %o0
   mov %l1, %o1
   call .mul
   nop
   mov %o0, %l2
   set 4, %l3
   add %l2, %l3, %l4
   add %i5, %l4, %l5
   set 7, %l6
   st %l6, [%l5]
   set 4, %l7
   sub %fp, %l7, %i0
   ld [%i0], %i1
   set 0, %i2
   add %i1, %i2, %i3
   ld [%i3], %i4
   set 2, %i5
   set 4, %l0
   mov %i5, %o0
   mov %l0, %o1
   call .mul
   nop
   mov %o0, %l1
   set 4, %l2
   add %l1, %l2, %l3
   add %i4, %l3, %l4
   set 12, %l5
   st %l5, [%l4]
   set 4, %l6
   sub %fp, %l6, %l7
   ld [%l7], %i0
   set 0, %i1
   add %i0, %i1, %i2
   ld [%i2], %i3
   set 3, %i4
   set 4, %i5
   mov %i4, %o0
   mov %i5, %o1
   call .mul
   nop
   mov %o0, %l0
   set 4, %l1
   add %l0, %l1, %l2
   add %i3, %l2, %l3
   set 18, %l4
   st %l4, [%l3]
   set 4, %l5
   sub %fp, %l5, %l6
   ld [%l6], %l7
   set 0, %i0
   add %l7, %i0, %i1
   ld [%i1], %i2
   set 4, %i3
   set 4, %i4
   mov %i3, %o0
   mov %i4, %o1
   call .mul
   nop
   mov %o0, %i5
   set 4, %l0
   add %i5, %l0, %l1
   add %i2, %l1, %l2
   set 2, %l3
   st %l3, [%l2]
   set 4, %l4
   sub %fp, %l4, %l5
   ld [%l5], %l6
   set 0, %l7
   add %l6, %l7, %i0
   ld [%i0], %i1
   set 5, %i2
   set 4, %i3
   mov %i2, %o0
   mov %i3, %o1
   call .mul
   nop
   mov %o0, %i4
   set 4, %i5
   add %i4, %i5, %l0
   add %i1, %l0, %l1
   set 11, %l2
   st %l2, [%l1]
   set 4, %l3
   sub %fp, %l3, %l4
   ld [%l4], %l5
   set 0, %l6
   add %l5, %l6, %l7
   ld [%l7], %i0
   set 6, %i1
   set 4, %i2
   mov %i1, %o0
   mov %i2, %o1
   call .mul
   nop
   mov %o0, %i3
   set 4, %i4
   add %i3, %i4, %i5
   add %i0, %i5, %l0
   set 6, %l1
   st %l1, [%l0]
   set 4, %l2
   sub %fp, %l2, %l3
   ld [%l3], %l4
   set 0, %l5
   add %l4, %l5, %l6
   ld [%l6], %l7
   set 7, %i0
   set 4, %i1
   mov %i0, %o0
   mov %i1, %o1
   call .mul
   nop
   mov %o0, %i2
   set 4, %i3
   add %i2, %i3, %i4
   add %l7, %i4, %i5
   set 9, %l0
   st %l0, [%i5]
   set 4, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   set 0, %l4
   add %l3, %l4, %l5
   ld [%l5], %l6
   set 8, %l7
   set 4, %i0
   mov %l7, %o0
   mov %i0, %o1
   call .mul
   nop
   mov %o0, %i1
   set 4, %i2
   add %i1, %i2, %i3
   add %l6, %i3, %i4
   set 19, %i5
   st %i5, [%i4]
   set 4, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   set 0, %l3
   add %l2, %l3, %l4
   ld [%l4], %l5
   set 9, %l6
   set 4, %l7
   mov %l6, %o0
   mov %l7, %o1
   call .mul
   nop
   mov %o0, %i0
   set 4, %i1
   add %i0, %i1, %i2
   add %l5, %i2, %i3
   set 5, %i4
   st %i4, [%i3]
   set 0, %i5
   mov %i5, %i0
   ba BBS$Init$epilogueStart
   nop
BBS$Init$epilogueStart:
   ret
   restore
BBS$Start:
   save %sp, -4*(24+3+0)&-8, %sp
   set 4, %l1
   sub %fp, %l1, %l2
   st %i0, [%l2]
   set 8, %l3
   sub %fp, %l3, %l4
   st %i1, [%l4]
   set 12, %l5
   sub %fp, %l5, %l6
   set 0, %l7
   st %l7, [%l6]
   ba BBS$Start$prologueEnd
   nop
BBS$Start$prologueEnd:
   set 12, %i0
   sub %fp, %i0, %i1
   mov %i1, %i2
   set 4, %i3
   sub %fp, %i3, %i4
   ld [%i4], %i5
   mov %i5, %o0
   set 8, %l0
   sub %fp, %l0, %l1
   ld [%l1], %l2
   mov %l2, %o1
   call BBS$Init
   nop
   mov %o0, %l3
   st %l3, [%i2]
   set 12, %l4
   sub %fp, %l4, %l5
   mov %l5, %l6
   set 4, %l7
   sub %fp, %l7, %i0
   ld [%i0], %i1
   mov %i1, %o0
   call BBS$Print
   nop
   mov %o0, %i2
   st %i2, [%l6]
   set 99999, %i3
   mov %i3, %o0
   call printInt
   nop
   set 12, %i4
   sub %fp, %i4, %i5
   mov %i5, %l0
   set 4, %l1
   sub %fp, %l1, %l2
   ld [%l2], %l3
   mov %l3, %o0
   call BBS$Sort
   nop
   mov %o0, %l4
   st %l4, [%l0]
   set 12, %l5
   sub %fp, %l5, %l6
   mov %l6, %l7
   set 4, %i0
   sub %fp, %i0, %i1
   ld [%i1], %i2
   mov %i2, %o0
   call BBS$Print
   nop
   mov %o0, %i3
   st %i3, [%l7]
   set 0, %i4
   mov %i4, %i0
   ba BBS$Start$epilogueStart
   nop
BBS$Start$epilogueStart:
   ret
   restore
