/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

package assem;

@Deprecated
public class COMMENT extends Instruction {
   public COMMENT ()         { super ("!"); }
   public COMMENT (String a) { super ("! " + a); }
}
