/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

/*
 * The entire "assem" package is provided to the student to be used as
 * is in the completion of the "Program: Instruction Selection", Chapter 9
 * page 216.
 */

/*
 * The "assem" package provides abstract assembly language instructions;
 * these instructions are independent of any map of registers to temps.
 */

package assem;

import tree.NameOfTemp;
import tree.NameOfLabel;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;

/*
  The concrete subclasses are: COMMENT, LABEL, MOVE, OPER
  The concrete subclasses are: Comment, LabelInstruction, Move, Oper
*/

public abstract class Instruction {

   // A concrete subclass should override these three methods
   public List<NameOfTemp>  use()   { return null;}   // List of tree.NameOfTemp
   public List<NameOfTemp>  def()   { return null;}   // List of tree.NameOfTemp
   public List<NameOfLabel> jumps() { return null;}   // List of tree.NameOfLabel
   public Set<NameOfTemp>   temps() {
      final Set<NameOfTemp> set = new HashSet<NameOfTemp>();
      if (use()!=null) set.addAll (use());
      if (def()!=null) set.addAll (def());
      return set;
   }

   private static final String FMT = "%s \t! %s";  // assembler dependent

   public final String assem;

   protected Instruction (String a) { assem=a; }

   protected Instruction (String a, String comment) {
      this (String.format (FMT, a, comment));
   }

   public String toString ()  { return assem; }

   /*
     Map every "temp" to its name for the purposes of printing instructions out.
   */
   public final static Map<NameOfTemp,String> DEFAULT_MAP = new HashMap<NameOfTemp,String> () {
      @java.lang.Override
      public String get(Object t) {
         if (containsKey(t)) return super.get(t); else return t.toString();
      }
   };


   public String format () {
      return format (DEFAULT_MAP);
   }

   public String format (Map<NameOfTemp,String> map) {
      if (map==null) return this.toString();
      final List<NameOfTemp>  src = use();
      final List<NameOfTemp>  dst = def();
      final List<NameOfLabel> jump = jumps();
      final StringBuilder s = new StringBuilder();
      final int len = assem.length();
      for (int i=0; i<len; i++)
         if (assem.charAt(i)=='`') {
            int n;
            switch(assem.charAt(++i)) {
            case 's':
               n = Character.digit(assem.charAt(++i),10);
               s.append (map.get (src.get(n)));
               break;

            case 'd':
               n = Character.digit(assem.charAt(++i),10);
               s.append (map.get (dst.get(n)));
               break;

            case 'j':
               n = Character.digit(assem.charAt(++i),10);
               s.append (jump.get(n)).toString();
               break;

            case '`':
               s.append('`'); 
               break;

            default:
               throw new RuntimeException ("bad instruction format:" + assem);
            }
         } else {
            s.append(assem.charAt(i));
         }
      return s.toString();
   }

}
