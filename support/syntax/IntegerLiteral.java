/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

public class IntegerLiteral extends Expression {
   public final int i;

   public IntegerLiteral (int ai) { i=ai;  }

   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }
}
