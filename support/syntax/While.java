/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

public class While extends Statement {
   public final Expression e;
   public final Statement s;

   public While (Expression ae, Statement as) { this (0,0,ae,as); }
   public While (int l, int c, Expression ae, Statement as) {
      super (l,c); e=ae; s=as;
   }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }
}

