/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

import java.util.List;

public class Call extends Expression {
   public final Expression e;           // Instance receiving the method invocation
   public final Identifier i;           // Name of method invoked
   public final List <Expression> el;
  
   public Call (Expression ae, Identifier ai, List <Expression> ael) {
      this(0,0,ae,ai,ael);
   }
   public Call (int l, int c, Expression ae, Identifier ai, List <Expression> ael) {
      super(l,c); e=ae; i=ai; el=ael;
   }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }

   /* It is very convenient during semantic analysis to determine and remember the
      class in which the invoked method is defined. */

   private String receiverClassName = null;
   public void setReceiverClassName (String r) { receiverClassName = r; }
   public String getReceiverClassName () { return receiverClassName; }
}
