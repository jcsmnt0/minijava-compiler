/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

public class NewObject extends Expression {
   public final Identifier i;   // Should this be an IdentifierType?  

   // Don't really need a position as long as the ident has one.
   public NewObject (Identifier ai) { i=ai;   }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }

}
