/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

import java.io.PrintWriter;

public class PrettyPrint implements SyntaxTreeVisitor <Void>  {
   private final PrintWriter pw;
   public PrettyPrint (PrintWriter pw) {
      this.pw = pw;
   }
   public PrettyPrint () { this (new PrintWriter(System.out));}

   private int tab = 0;
   private void tab () {
      for (int i=0; i<tab; i++) pw.print ("   ");
   }

   private void print   (int i)    { pw.print (i); }
   private void print   (String s) { pw.print (s); }
   private void println (String s) { pw.println (s); }
   private void println ()         { pw.println ();}


   // Subcomponents of Program:  MainClass m; List<ClassDecl> cl;
   public Void visit (Program n) {
      tab = 0;
      if (n==null) {
         print ("// null Program!!");
      } else if (n.m==null) {
         print ("// null Main class!!");
      } else {
         n.m.accept (this);
         for (ClassDecl c: n.cl) c.accept (this);
         assert tab==0;
      }
      pw.flush();
      return null;
   }
  
   // Subcomponents of MainClass:  Identifier i1, i2; Statement s;
   public Void visit (MainClass n) {
      tab ();
      print ("class ");
      n.i1.accept (this);  // identifier:  name of class
      println (" {");  tab++;
      tab();
      print ("public static void main (String [] ");
      n.i2.accept (this);  // identifier:  name of arguments
      print (")");

      println (" {"); tab++;
      n.s.accept (this);   // statement:  body of main 
      tab--; tab(); println ("}");
      tab--; tab(); println ("}");
      return null;
   }

   // Subcomponents of SimpleClassDecl: Identifier i; List<VarDecl> vl; List<MethodDecl> ml;
   public Void visit (final SimpleClassDecl n) {
      tab ();
      print ("class ");
      n.i.accept (this);
      println (" {");  tab++;
      for (VarDecl v: n.vl) v.accept (this);
      for (MethodDecl m: n.ml) m.accept (this);
      tab--; tab (); println ("}");
      // Does end with a newline
      return null;
   }
 
   // Subcomponents of ExtendingClassDecl: Identifier i, j; List<VarDecl> vl; List<MethodDecl> ml;
   public Void visit (final ExtendingClassDecl n) {
      tab ();
      print ("class ");
      n.i.accept (this);
      print (" extends ");
      n.j.accept (this);
      println (" {");  tab++;
      for (VarDecl v: n.vl) v.accept (this);
      for (MethodDecl m: n.ml) m.accept (this);
      tab--; tab (); println ("}");
      // Does end with a newline
      return null;
   }

   // Subcomponents of VarDecl:  Type t; Identifier i;
   public Void visit (VarDecl n) {
      tab ();
      n.t.accept (this);   // Type t: no new line
      print (" ");
      n.i.accept (this);   // Identifier i: no new line
      println (";");
      // Does end with a newline
      return null;
   }

   // Subcomponents of MethodDecl:
   // Type t; Identifier i; List<Formal> fl; List<VarDecl> vl; List<Statement>t sl; Expression e;
   public Void visit (MethodDecl n) {
      tab ();
      print ("public ");
      n.t.accept (this);
      print (" ");
      n.i.accept (this);
      print (" (");

      if (n.fl.size()>0) {
         n.fl.get (0).accept (this);
         // Loop over all actuals excluding the first one
         for (Formal f: n.fl.subList(1, n.fl.size())) {
            print (", ");
            f.accept (this);
         }
      }
      print (")");
      println (" {"); tab++;
      for (VarDecl v: n.vl) v.accept (this);
      for (Statement s: n.sl) s.accept(this);

      // Return statement
      tab ();
      print ("return ");
      n.e.accept (this);      // Expression e: no new line
      println (";");
      tab--; tab(); println ("}");

      // Does end with a newline
      return null;
   }

   // Subcomponents of Formal:  Type t; Identifier i;
   public Void visit (Formal n) {
      n.t.accept (this);
      print (" ");
      n.i.accept (this);
      // Does not end with a newline
      return null;
   }

   public Void visit (IntArrayType n) {
      print ("int[]");
      // Does not end with a newline
      return null;
   }

   public Void visit (BooleanType n) {
      print ("boolean");
      return null;
   }

   public Void visit (IntegerType n) {
      print ("int");
      return null;
   }

   // String s;
   public Void visit (IdentifierType n) {
      print (n.s);
      return null;
   }

   // Subcomponents of Block statement:  StatementList sl;
   public Void visit (Block n) {
      tab(); println ("{"); tab++;
      for (Statement s: n.sl) s.accept (this);
      tab--; tab(); println ("}");
      return null;
   }

   // Subcomponents of If statement: Expression e; Statement s1,s2;
   public Void visit (If n) {
      tab();
      print ("if (");
      n.e.accept (this);
      print (")");
      printT (n.s1);  // "then" block
      printC (n.s2);  // "else" block (not optional in MiniJava)
      return null;
   }

   private void printT (Statement s) {
      if (s instanceof Block) {
         println (" {"); tab++;
         for (Statement ss: ((Block)s).sl) ss.accept (this);
         tab--; tab(); print ("} else");
      } else {
         println (); tab++;
         s.accept (this);
         tab--; tab(); print ("else");
      }
   }


   private void printC (Statement s) {
      if (s instanceof Block) {
         println (" {"); tab++;
         for (Statement ss: ((Block)s).sl) ss.accept (this);
         tab--; tab(); println ("}");
      } else {
         println (); tab++;
         s.accept (this);
         tab--;
      }
   }

   // Subcomponents of While statement: Expression e, Statement s
   public Void visit (final While n) {
      tab ();
      print ("while (");
      n.e.accept (this);
      print (")");
      printC (n.s);
      return null;
   }

   // Subcomponents of Print statement:  Expression e;
   public Void visit (Print n) {
      tab ();
      print ("System.out.println (");
      n.e.accept (this);
      println (");");
      return null;
   }
  
   // subcomponents of Assignment statement:  Identifier i; Expression e;
   public Void visit (Assign n) {
      tab ();
      n.i.accept (this);
      print (" = ");
      n.e.accept (this);
      println (";");
      return null;
   }

   // Identifier i; Expression e1,e2;
   public Void visit (ArrayAssign n) {
      tab ();
      n.i.accept(this);
      print ("[");
      n.e1.accept(this);
      print ("] = ");
      n.e2.accept(this);
      println (";");
      return null;
   }

   // Expression e1,e2;
   public Void visit(And n) {
      print("(");
      n.e1.accept(this);
      print(" && ");
      n.e2.accept(this);
      print(")");
      return null;
   }

   // Expression e1,e2;
   public Void visit(LessThan n) {
      print("(");
      n.e1.accept(this);
      print(" < ");
      n.e2.accept(this);
      print(")");
      return null;
   }

   // Expression e1,e2;
   public Void visit(Plus n) {
      print("(");
      n.e1.accept(this);
      print(" + ");
      n.e2.accept(this);
      print(")");
      return null;
   }

   // Expression e1,e2;
   public Void visit(Minus n) {
      print("(");
      n.e1.accept(this);
      print(" - ");
      n.e2.accept(this);
      print(")");
      return null;
   }

   // Expression e1,e2;
   public Void visit(Times n) {
      print("(");
      n.e1.accept(this);
      print(" * ");
      n.e2.accept(this);
      print(")");
      return null;
   }

   // Expression e1,e2;
   public Void visit(ArrayLookup n) {
      n.e1.accept(this);
      print("[");
      n.e2.accept(this);
      print("]");
      return null;
   }

   // Expression e;
   public Void visit(ArrayLength n) {
      n.e.accept(this);
      print(".length");
      return null;
   }


   // Subcomponents of Call:  Expression e; Identifier i; ExpressionList el;
   public Void visit (Call n) {
      n.e.accept (this);
      print (".");
      n.i.accept (this);
      print ("(");

      if (n.el.size()>0) {
         n.el.get (0).accept (this);

         // Loop over all actuals excluding the first one
         for (Expression e: n.el.subList(1, n.el.size())) {
            print (", ");
            e.accept (this);
         }
      }
      print (")");
      return null;
   }

   public Void visit (True n) {
      print ("true");
      return null;
   }

   public Void visit (False n) {
      print ("false");
      return null;
   }

   public Void visit (IntegerLiteral n) {
      print (n.i);
      return null;
   }

   public Void visit (IdentifierExp n) {
      print(n.s);
      return null;
   }

   public Void visit (This n) {
      print ("this");
      return null;
   }

   // Expression e;
   public Void visit (NewArray n) {
      print ("new int [");
      n.e.accept (this);
      print ("]");
      return null;
   }

   // Identifier i;
   public Void visit (NewObject n) {
      print ("new ");
      print (n.i.s);
      print ("()");
      return null;
   }

   // Expression e;
   public Void visit (Not n) {
      print ("!");
      n.e.accept (this);
      return null;
   }

   // String s;
   public Void visit (Identifier n) {
      print (n.s);
      return null;
   }
}
