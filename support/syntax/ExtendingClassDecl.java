/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

import java.util.List;

public class ExtendingClassDecl extends ClassDecl {
   public final Identifier i; // The name of the class.  It carries position info: line number, column number
   public final Identifier j; // name of super class
   public final List <VarDecl> vl;  
   public final List <MethodDecl> ml;
 
   public ExtendingClassDecl (
      Identifier ai, Identifier aj, List <VarDecl> avl, List <MethodDecl> aml) {
      i=ai; j=aj; vl=avl; ml=aml;
   }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }
}
