/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

/*
  How does an IdentifierExp and IdentifierType differ from an Identifier?

  An Identifier is as part of particular, syntactic structures:
    new IDENTIFIER ()           // class instantiation
    ... . IDENTIFIER ( ... )    // a call
    IDENTIFIER [...] := ...     // array assignment
    type IDENTIFIER, ....       // formal argument
                                // method declaration

  An IdentiferExp is the use of an identifer in an expression.
  An IdentiferType is the use of an identifer in a type.
*/

public class IdentifierExp extends Expression {
   public final String s;
   
   public IdentifierExp (int l, int c, String as) { super (l,c); s=as; }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }

   @Override
   public String toString () { return s; }
}
