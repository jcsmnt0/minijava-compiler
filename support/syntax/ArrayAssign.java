/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

package syntax;

public class ArrayAssign extends Statement {

   public final Identifier i;
   public final Expression e1, e2;

   public ArrayAssign (Identifier ai, Expression ae1, Expression ae2) {
      this(0,0,ai,ae1,ae2);
   }
   public ArrayAssign (int l, int c, Identifier ai, Expression ae1, Expression ae2) {
      super(l,c); i=ai; e1=ae1; e2=ae2;
   }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }

}

