/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

/*
  The location of a formal declaration in the original source file could be
  taken to be that of the syntax.Identifier "i".  This subclass of AST has
  a linenumber and a column number.

  syntax.Type also is a subclass of AST and so also contains a linenumber
  and a column number.
*/

public class Formal {
   public final Type t;
   public final Identifier i;  // Should this not simply be String?  Line number?
 
   public Formal (Type at, Identifier ai) { t=at; i=ai;  }

   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }
}
