/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

public abstract class Expression extends AST {

   protected Expression () { this(0,0); }
   public Expression (int l, int c) { super (l,c); }
}
