/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

import java.util.List;

public class SimpleClassDecl extends ClassDecl {
   public final Identifier i;       // The name of the class.  It carries position info: line number, column number
   public final List<VarDecl> vl;  
   public final List<MethodDecl> ml;
 
   public SimpleClassDecl (Identifier ai, List <VarDecl> avl, List <MethodDecl> aml) {
      i=ai; vl=avl; ml=aml;
   }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }
}
