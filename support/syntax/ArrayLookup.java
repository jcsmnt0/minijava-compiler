/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

public class ArrayLookup extends Expression {
   public final Expression e1,e2;
  
   public ArrayLookup (Expression ae1, Expression ae2) { 
      e1=ae1; e2=ae2;
   }

   @Override
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }
}
