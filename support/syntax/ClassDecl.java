/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

package syntax;

/*
 * "SimpleClassDecl" and "ExtendingClassDecl" are the two concrete subclasses
 * of this class, "ClassDecl".
 *
 * No need to extend AST as the class names, "Identifier", carry the position
 * (line number and column number) which is adequate for reporting errors
 * concerning classes in general.
 */

public abstract class ClassDecl {
   public abstract <T> T accept (final SyntaxTreeVisitor <T> v);
}
