/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

import java.util.List;

public class MethodDecl {
   public final Type t;
   public final Identifier i;
   public final List <Formal> fl;
   public final List <VarDecl> vl;
   public final List <Statement> sl;
   public final Expression e;

   public MethodDecl (
      Type at, Identifier ai, List <Formal>afl, List <VarDecl> avl, List <Statement> asl, Expression ae) {
      t=at; i=ai; fl=afl; vl=avl; sl=asl; e=ae;
   }
 
   public <T> T accept (final SyntaxTreeVisitor <T> v) {
      return v.visit(this);
   }
}
