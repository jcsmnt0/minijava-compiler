/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package syntax;

/*
  The concrete subclasses are:
     BooleanType
     IntegerType
     IntArrayType
     IdentifierType

  No method can return "void" in MiniJava
 */
public abstract class Type extends AST {

   // We lose the position of the unique built-in types this way.
   public final static IntegerType THE_INTEGER_TYPE = new IntegerType ();
   public final static BooleanType THE_BOOLEAN_TYPE = new BooleanType ();
   public final static IntArrayType THE_INT_ARRAY_TYPE = new IntArrayType ();

   protected Type () { super (0,0); }
   protected Type (int l, int c) { super (l,c); }

   public String getName () { return toString(); }
}
