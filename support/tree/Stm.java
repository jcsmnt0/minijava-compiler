/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

package tree;

import tree.ExpList;

/*
  Concrete subclasses:  MOVE, EXP, JUMP, CJUMP, SEQ, LABEL
  Concrete subclasses:  MOVE, EVAL, JUMP, CJUMP, SEQ, LABEL
  Concrete subclasses:  MOVE, EVAL, JUMP, CJUMP, SEQ, Label_Next_Stm
 */

abstract public class Stm {
   public Stm     build (ExpList kids) {throw new UnsupportedOperationException();}
   public Stm     create (Exp... kids) {throw new UnsupportedOperationException();}
   public ExpList kids()               {throw new UnsupportedOperationException();}
   public String  toString ()          { return TreePrint.toString (this); }
}

