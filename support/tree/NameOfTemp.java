/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

/*
  Temps are abstract names for values temporarily held in registers.

  See sketch of this class in Appel, 2nd, page 131.  He puts in the package
  'temp'.  It does not really belong in package 'tree', but ...
*/

/*
  Need to implement equals correctly for HashMap!
*/
package tree;

public final class NameOfTemp {

   private final String name;
   private static final String FMT = "%s%03d";

   public  NameOfTemp (String n)        { name = n; }
   private NameOfTemp (String n, int c) { this (String.format (FMT, n, c)); }

   //Static, factory methods.
   private static int count = 0;
   public static NameOfTemp generateTemp () { return generateTemp ("t"); }
   public static NameOfTemp generateTemp (String s) { return new NameOfTemp (s, ++count); }

   @java.lang.Override
   public String toString() { return name; }
   @java.lang.Override
   public int hashCode()    { return name.hashCode(); }
   @java.lang.Override
   public boolean equals (Object other) {
      if (this == other) return true;
      if (other == null) return false;
      if (this.getClass() != other.getClass()) return false;
      final NameOfTemp o = (NameOfTemp) other;
      return name.equals (o.name);
   }

}
