/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package tree;

import tree.Stm;
import tree.ExpList;

import java.util.List;

public class LABEL extends Stm { 
   public final NameOfLabel label;
   public LABEL(NameOfLabel l) {label=l;}
   public LABEL(String s) {this (new NameOfLabel(s)); }
   public LABEL(String... s) {this (NameOfLabel.generateLabel(s)); }
   public List<Exp> subexpressions () { return null;}
   public ExpList kids() {return null;}
   public Stm build(ExpList kids) { return this; }
   public Stm build(List<Exp> kids) { return this; }
   public Stm create (Exp...kids){ return this; }

   public static LABEL generateLABEL (String... s) {
      return new LABEL (NameOfLabel.generateLabel(s));
   }
}

