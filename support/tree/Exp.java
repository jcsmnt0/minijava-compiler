/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

package tree;

import tree.ExpList;

/*
  Concrete subclass: BINOP, CALL, CONST, ESEQ, MEM, NAME, TEMP
  Concrete subclass: BINOP, CALL, CONST, ESEQ, MEM, NAME, Temp_As_Exp
*/
abstract public class Exp {
   public int     need=0;   // Registers exp needs; used in Sethi-Ullman algo
   public Exp     build(ExpList kids) {throw new UnsupportedOperationException();}
   public Exp     create (Exp...kids) {throw new UnsupportedOperationException();}
   public ExpList kids()              {throw new UnsupportedOperationException();}
   public Exp[]   subcomponents ()    {throw new UnsupportedOperationException();}
   public String  toString ()         { return TreePrint.toString (this); }

}

