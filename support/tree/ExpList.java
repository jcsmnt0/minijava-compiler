// Needs to be replace by List<Exp>

package tree;
import java.util.List;
import java.util.LinkedList;

public class ExpList {
   public final Exp head;
   public final ExpList tail;
   public ExpList(Exp h, ExpList t) {head=h; tail=t;}

   public static ExpList toExpList (Exp... l) {
      if (l.length==0) {
         return null;
      } else if (l.length==1) {
         return new ExpList (l[0], null);
      } else if (l.length==2) {
         return new ExpList (l[0], new ExpList (l[1], null));
      } else {
         throw new IllegalArgumentException ();
      }
   }

   public LinkedList<Exp> toList() {
      final LinkedList<Exp> ll = tail==null ? new LinkedList<Exp> (): tail.toList();
      ll.addFirst(head);
      return ll;
   }

}



