package tree;

import java.io.PrintWriter;

public class Printer {

   final PrintWriter out;

   public Printer (PrintWriter o) { out=o; }

   void indent (int d) {
      for (int i=0; i<d; i++) {
	 out.print (' ');
      }
   }

   void say (String s) { out.print(s); }

   void sayln (String s) { say(s); say("\n"); }

   void say (int d, String s) { indent (d); say (s); }

   void sayln (int d, String s) { indent (d); sayln (s); }

   void flush () { out.flush(); }
}

