/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package tree;

import tree.Stm;
import tree.ExpList;

public class MOVE extends Stm {
   public final Exp dst, src;
   public MOVE(Exp d, Exp s) {dst=d; src=s;}
   public ExpList kids() {
      if (dst instanceof MEM) {
	 return new ExpList(((MEM)dst).exp, new ExpList(src,null));
      } else {
	 return new ExpList(src,null);
      }
   }
   public Stm build(ExpList kids) {
      if (dst instanceof MEM) {
	 return new MOVE(new MEM(kids.head), kids.tail.head);
      } else {
	 return new MOVE(dst, kids.head);
      }
   }
}

