/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package tree;

/*
  A renaming of the class LABEL to conform with Java class naming standards
  and to permit compilation on Windows which does not like files which differ
  only by capitalization.
 */

import tree.Stm;
import tree.ExpList;

import java.util.List;

@Deprecated
public class Label_Next_Stm extends Stm { 
   public final NameOfLabel label;
   private Label_Next_Stm(String s) {this (new NameOfLabel(s)); }
   public Label_Next_Stm(NameOfLabel l) {label=l;}
   public List<Exp> subexpressions () { return null;}
   public ExpList kids() {return null;}
   public Stm build(ExpList kids) { return this; }
   public Stm build(List<Exp> kids) { return this; }
   public Stm create (Exp...kids){ return this; }

   public static Label_Next_Stm generateLABEL (String... s) {
      return new Label_Next_Stm (NameOfLabel.generateLabel(s));
   }
}

