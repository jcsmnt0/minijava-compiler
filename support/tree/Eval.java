/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */

package tree;

/*
  A renaming of the class EVAL to conform with Java class naming standards.
 */


import tree.Stm;
import tree.ExpList;
import java.util.List;
/*
   Eval(e)  --  Evaluate e and discard the result.

   Used to be named EXP; renamed to reduce confusion.
*/
@Deprecated
public class Eval extends Stm {
   public final Exp exp; 
   public Eval (Exp e) {exp=e;}
   public ExpList kids() {return new ExpList(exp,null);}
   public Stm build (ExpList kids) {
      return new Eval(kids.head);
   }
   public Stm build (List<Exp> kids) {
      return new Eval(kids.get(0));
   }
}

