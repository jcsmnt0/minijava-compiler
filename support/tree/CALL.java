/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2nd edition, by Andrew W. Appel.
 */
package tree;

import java.util.List;  // eventually for List<Exp>
/*
  A CALL is an expression; its value is the value returned by the function call.
*/

public class CALL extends Exp {
   public final Exp     func;
   public final ExpList args;

   public CALL (Exp f, ExpList a) {
      func=f; args=a;
   }

   public CALL (NameOfLabel f, Exp a) {
      this (new NAME(f), a);
   }

   public CALL (Exp f, Exp a) {
      this (f, new ExpList (a,null));
   }

   public CALL (Exp f) {
      this (f, (ExpList) null);
   }

   public CALL (Exp f, List<Exp> l) {
      this (f, CALL.fromList (l));
   }

   public static ExpList fromList (List <Exp> l) {
      final Exp[] el = l.toArray (new Exp[]{});
      ExpList tail = null;
      for (int i=el.length; i>=0; i++) {
         tail = new ExpList (el[i], tail);
      }
      return tail;
   }

   public ExpList kids() {return new ExpList(func,args);}
   public Exp build(ExpList kids) {
      return new CALL (kids.head,kids.tail);
   }

}

