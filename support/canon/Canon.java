/*
 * Florida Tech, CSE4251: Compiler Design.  Part of the compiler project from
 * "Modern Compiler Implementation in Java," 2st edition, by Andrew W. Appel.
 */

package canon;

import tree.*;
import java.util.List;
import java.util.ArrayList;

public final class Canon {

   // Remove ESEQ (=RET) and move CALLs to top level
   public static List<Stm> linearize (final Stm s) {
      assert (s!=null);
      return linear (do_stm(s), new ArrayList<Stm>());
   }

   private static List<Stm> linear (final Stm s, final List<Stm> l) {
      if (s instanceof SEQ) return linear((SEQ)s, l);
      else return cons(s,l);
   }

   private static List<Stm> linear (final SEQ s, final List<Stm> l) {
      return linear (s.left,linear(s.right,l));
   }

   private static List<Stm> cons (final Stm s, final List<Stm> l) {
      l.add (0, s);
      return l;
   }



   private static class MoveCall extends Stm {
      final TEMP dst;
      final CALL src;
      MoveCall (TEMP d, CALL s) {dst=d; src=s;}
      public ExpList kids() {return src.kids();}
      public Stm build (ExpList kids) {
         return new tree.MOVE (dst, src.build(kids));
      }
   }   
  
   private static class ExpCall extends Stm {
      final CALL call;
      ExpCall (CALL c) {call=c;}
      public ExpList kids() {return call.kids();}
      public Stm build(ExpList kids) {
         return new tree.EVAL (call.build(kids));
      }
   }   

   // A pair (Stm,ExpList)
   private static final class StmExpList {
      final Stm stm;
      final ExpList exps;
      StmExpList (Stm s, ExpList e) {stm=s; exps=e;}
   }

   private static boolean isNop (Stm a) {
      return a instanceof EVAL && ((EVAL)a).exp instanceof CONST;
   }

   private static Stm seq (final Stm a, final Stm b) {
      if (isNop(a)) return b;
      else if (isNop(b)) return a;
      else return new SEQ(a,b);
   }

   private static Stm seq (final Stm a, final Stm b, final Stm c) {
      return (seq (a, seq (b, c)));
   }

   private static boolean commute (final Stm a, final Exp b) {
      return isNop(a) || b instanceof NAME || b instanceof CONST;
   }

   private static Stm do_stm (final SEQ s) { 
      return seq (do_stm(s.left), do_stm(s.right));
   }

   private static Stm do_stm (final MOVE s) { 
      assert (s!=null);
      if (s.dst instanceof TEMP && s.src instanceof CALL) 
	 return reorder_stm (new MoveCall((TEMP)s.dst, (CALL)s.src));
      else if (s.dst instanceof ESEQ)
	 return do_stm (new SEQ(((ESEQ)s.dst).stm, new MOVE(((ESEQ)s.dst).exp, s.src)));
      else return reorder_stm (s);
   }

   private static Stm do_stm (final EVAL s) {
      assert (s!=null);
      if (s.exp instanceof CALL)
	 return reorder_stm (new ExpCall((tree.CALL)s.exp));
      else return reorder_stm(s);
   }

   private static Stm do_stm (final Stm s) {
      assert s!=null;
      if (s instanceof SEQ) return do_stm ((SEQ)s);
      else if (s instanceof MOVE) return do_stm ((MOVE)s);
      else if (s instanceof EVAL) return do_stm ((EVAL)s);
      else return reorder_stm(s);
   }

   private static Stm reorder_stm (final Stm s) {
      assert (s!=null);
      final StmExpList x = reorder (s.kids());
      assert (x!=null);
      return seq (x.stm, s.build (x.exps));
   }

   private static ESEQ do_exp (final ESEQ e) {
      final Stm stms = do_stm (e.stm);
      final ESEQ b = do_exp (e.exp);
      return new ESEQ (seq(stms,b.stm), b.exp);
   }

   private static ESEQ do_exp (final Exp e) {
      assert e!=null;
      if (e instanceof tree.ESEQ) return do_exp((tree.ESEQ)e);
      else return reorder_exp(e);
   }
         
   private static ESEQ reorder_exp (final Exp e) {
      final StmExpList x = reorder(e.kids());
      return new ESEQ (x.stm, e.build(x.exps));
   }

   private static final StmExpList nopNull =
      new StmExpList(new EVAL(new tree.CONST(0)),null);

   static StmExpList reorder (final ExpList exps) {
      if (exps==null) {
	 return nopNull;
      } else {
	 final Exp a = exps.head;
         assert a!=null;
	 if (a instanceof tree.CALL) {
            final TEMP t = TEMP.generateTEMP("t");
	    final Exp e = new tree.ESEQ(new tree.MOVE(t, a), t);
	    return reorder(new ExpList(e, exps.tail));
	 } else {
	    tree.ESEQ aa = do_exp(a);
	    final StmExpList bb = reorder(exps.tail);
	    if (commute(bb.stm, aa.exp)) {
	       return new StmExpList(seq(aa.stm,bb.stm), 
                  new ExpList(aa.exp,bb.exps));
	    } else {
               final TEMP t = TEMP.generateTEMP("t");
	       return new StmExpList(
                  seq(aa.stm, new tree.MOVE(t,aa.exp), bb.stm),
                  new ExpList(t, bb.exps));
	    }
	 }
      }
   }

}
