/*
 * CSE3001.  Compiler project based on "Modern Compiler Implementation
 * in Java" by Andrew W. Appel.
 *
 */
package canon;

import tree.*;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

public class TraceSchedule {

   public static List<Stm> trace (final List<List<Stm>> bb) {
      final TraceSchedule ts = new TraceSchedule(bb);
      ts.getNext();
      return ts.getSchedule();
   }

   private final List<List<Stm>> theBB;
   private final HashMap<NameOfLabel,List<Stm>> table = new HashMap<NameOfLabel,List<Stm>>();

   private TraceSchedule (final List<List<Stm>> bb) {
      theBB=bb;
      /*
        Associate each block (Statement List) with the label that heads it.
      */
      for (List<Stm> stml: bb) {
         table.put (head(stml).label, stml);
      }
   }


   // Get last *two* statements!
   private static List<Stm> getLast (final List<Stm> block) {
      final int n = block.size();
      return block.subList (n-2,n);
   }

   private void tracex (List<Stm> stml) {
      for(;;) {
	 final tree.LABEL lab = head (stml);
         assert table.containsKey(lab.label);
         scheduleNext (table.get(lab.label));
         table.remove(lab.label);

         final Stm last_stm = stml.get (stml.size()-1);
         if (last_stm instanceof tree.JUMP) {
            // Unconditional jump
            final tree.JUMP j = (tree.JUMP)last_stm;
            final List<Stm> target = table.get(j.targets.get(0));
            if (j.targets.size()==1 && target!=null) {
               stml=target;
            } else {
               getNext();
               return;
            }
         } else if (last_stm instanceof tree.CJUMP) {
            // As with real machines' instructions, schedule false trace immediately
            // after condition jump.
            final tree.CJUMP j = (tree.CJUMP)last_stm;
            assert j.iftrue!=null;
            final List<Stm> t = table.get(j.iftrue);
            assert j.iffalse!=null;
            final List<Stm> f = table.get(j.iffalse);
            if (f!=null) {
               // If there is a trace for the false branch, schedule it next
               stml=f;
            } else if (t!=null) {
               // If there is no trace for the false branch, negate the condition
               final tree.CJUMP cj =
                  new tree.CJUMP (tree.CJUMP.notRel(j.relop), j.left, j.right, j.iffalse, j.iftrue);
               // Update/modify last of list
               stml.set (stml.size()-1, cj);
               stml=t;
            } else {
               final String msg = String.format ("Bad conditional jump (t=%s, f=%s) in TraceSchedule", j.iftrue, j.iffalse);
               throw new RuntimeException(msg);
               // Normally both labels ought to head some block, but if not ...
               /*
               final Label ff = new Label("trace$begin");
               final tree.CJUMP cj = new tree.CJUMP(j.relop, j.left, j.right, j.iftrue, ff);
               final StmList x = new StmList(new tree.Label_Next_Stm(ff), new StmList(new tree.JUMP(j.iffalse), getNext()));
               last.tail = new StmList (cj, x);
               return;
               */
            }
         } else {
            throw new RuntimeException("Bad basic block in TraceSchedule");
         }
      }
   }


   private static tree.LABEL head (List<Stm> l) {
      assert l.get(0) instanceof tree.LABEL;
      return (tree.LABEL) (l.get(0));
   }

   private final List<List<Stm>> theTrace = new ArrayList<List<Stm>>();
   private void scheduleNext (List<Stm> bb) {
      theTrace.add (bb);
   }
   private List<Stm> getSchedule() {
      final List<Stm> list = new ArrayList<Stm> ();
      for (List<Stm> l: theTrace) {
         for (Stm s: l) list.add (s);
      }
      return list;
   }

   private List<Stm> getNext() {
      if (theBB.size()==0) {
         return null;
      } else {
         final List<Stm> s = theBB.get(0);
         final tree.LABEL lab = head (s);
         if (table.containsKey(lab.label)) {
            tracex(s);
            return s;
         } else {
            // already added lab to the trace
            theBB.remove(0);
            return getNext();
         }
      }
   }
}

