! from sys/syscall.h
.set SYS_exit, 1

! from stdio.h
.set stdout, __iob+16

.macro exit_program
   clr %o0
   mov SYS_exit, %g1
   ta 0
.endm

.macro flush_stdout
   set stdout, %o0
   call fflush
   nop
.endm

.macro print_word addr
   ld \addr, %o0
   call println
   nop
   flush_stdout
.endm
