import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private static final String PROLOGUE = String.format(
      "   .section \".text\"%n"
    + "   .align 4%n"
    + "   .global main%n"
    + "main:%n"
    + "   save %%sp, -144, %%sp%n"
    );

    private static final String EPILOGUE = String.format(
      "   restore%n"
    + "   exit_program%n"
    );

    private static String createR(final int fpOffset) {
        final String cmd
                = "   call newR%n"
                + "   nop%n"
                + "   st %%o0, [%%fp-%d]%n";
        return String.format(cmd, fpOffset);
    }

    private static Map<String, Integer> rOffsets = new HashMap<String, Integer>() {
        {
            put("a", 0);
            put("b", 4);
            put("c", 8);
        }
    };

    private static String printRAttr(final int fpOffset,
                                     final String attr) {
        final String cmd
                = "   ld [%%fp-%d], %%l0%n"
                + "   print_word [%%l0+%d]%n";
        return String.format(cmd, fpOffset, rOffsets.get(attr));
    }

    private static String setRAttr(final int fpOffset,
                                   final String attr,
                                   final int val) {
        final String cmd
                = "   ld [%%fp-%d], %%l0%n"
                + "   mov %d, %%l1%n"
                + "   st %%l1, [%%l0+%d]%n";
        return String.format(cmd, fpOffset, val, rOffsets.get(attr));
    }

    private static String printArrayWord(final int fpOffset,
                                         final int arrOffset) {
        final String cmd
                = "   print_word [%%fp - %d + (4*%d)]%n";
        return String.format(cmd, fpOffset, arrOffset);
    }

    private static String setArrayWord(final int fpOffset,
                                       final int arrOffset,
                                       final int val) {
        final String cmd
                = "   mov %d, %%l0%n"
                + "   st %%l0, [%%fp - %d + (4*%d)]%n";
        return String.format(cmd, val, fpOffset, arrOffset);
    }

    private static String zeroIntArray(final int fpOffset,
                                       final int len) {
        final String cmd
                = "   sub %%fp, %d, %%o0%n"
                + "   mov %d, %%o0%n"
                + "   call zero_ints%n"
                + "   nop%n";
        return String.format(cmd, fpOffset, len);
    }

    public static void main(final String[] args) {
        final int rPtrOffset = 4;
        final int yOffset = 44;

        System.out.print(PROLOGUE);
        System.out.print(createR(rPtrOffset));
        System.out.print(zeroIntArray(yOffset, 10));

        final Scanner in = new Scanner(System.in);

        while (in.hasNextLine()) {
            final Scanner line = new Scanner(in.nextLine());
            final String token = line.next();

            final boolean setOp = line.hasNext();

            if (Character.isDigit(token.charAt(0))) {
                final int elem = Integer.parseInt(token);

                if (setOp) {
                    final int val = line.nextInt();
                    System.out.print(setArrayWord(yOffset, elem, val));
                } else {
                    System.out.print(printArrayWord(yOffset, elem));
                }
            } else {
                if (setOp) {
                    final int val = line.nextInt();
                    System.out.print(setRAttr(rPtrOffset, token, val));
                } else {
                    System.out.print(printRAttr(rPtrOffset, token));
                }
            }
        }

        System.out.print(EPILOGUE);
    }
}
