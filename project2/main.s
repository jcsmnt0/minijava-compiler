   ! from sys/syscall.h
   .set SYS_exit, 1

   ! from stdio.h
   .set stdout, __iob+16

   .macro exit_program
      clr %o0
      mov SYS_exit, %g1
      ta 0
   .endm

   .macro flush_stdout
      set stdout, %o0
      call fflush
      nop
   .endm

   .macro print_word addr
      ld \addr, %o0
      call println
      nop
      flush_stdout
   .endm

   ! arr[i] = elem
   .macro set_word arr i elem
      mov \elem, %l1
      st %l1, [\arr + (\i*4)]
   .endm

   ! print (*arr)[i]
   .macro print_word_ptr addr i
      ld [\addr], %l1
      print_word [%l1 + (\i*4)]
   .endm

   ! (*ptr)[i] = elem
   .macro set_word_ptr ptr i elem
      ld [\ptr], %l1
      mov \elem, %l2
      st %l2, [%l1 + (\i*4)]
   .endm

   .section ".data"
   .align 8
r: .word 0
y: .word 0, 0, 0, 0, 0, 0, 0, 0, 0, 0

   .section ".text"
   .align   4
   .global  main
main:
   save %sp, -144, %sp

   ! [%fp-4]: &r
   ! [%fp - (8 + (4*i))]: y[i]

   mov 15, %l1
   st %l1, [%fp-4]

   print_word [%fp-4]

   set y, %l0
   set_word %l0 1 15

   print_word [%l0+4]

   ! Get an R struct (r) and store it at [r]
   call newR
   nop
   set r, %l0
   st %o0, [%l0]

   ! Store 3 in r.b
   set r, %l0
   set_word_ptr %l0 1 3

   ! Load r.b into %o0 and print it
   set r, %l0
   print_word_ptr %l0 1

   restore
   exit_program

