#include <stdio.h>
#include <stdlib.h>

struct R {
   int a, b, c;
};

struct R * newR () {
   struct R *r = malloc (sizeof *r);
   r->a = 0;
   r->b = 0;
   r->c = 0;
   return r;
}

void println (int n) {
   printf ("%d\n", n);
   fflush (stdout);
}

void zero_ints (int *a, int len) {
   int i;
   for (i = 0; i < len; i++) {
      a[i] = 0;
   }
}
