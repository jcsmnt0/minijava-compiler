import parser.*;
import java.io.PrintWriter;
import java.io.FileInputStream;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.HashMap;

public class Parse {
   private static final Map<Integer, String> kindImages = new HashMap<Integer, String>();
   static {
      try {
         final Class c = Class.forName("parser.MiniJavaParserConstants");
         final Field[] fs = c.getDeclaredFields();
         for (int i = 0; i < fs.length; i++) {
            if (fs[i].getType() == int.class) {
               final int val = fs[i].getInt(null);
               if (!kindImages.containsKey(val)) {
                  kindImages.put(val, fs[i].getName());
               }
            }
         }
      } catch (final Exception e) {
         throw new RuntimeException("Could not locate token kind constants");
      }
   }

   private static final boolean verbose = System.getProperty("verbose") != null;

   public static void main(final String[] args) {
      final String filePath = args[0];
      final PrintStream out = System.out;

      final MiniJavaParser parser;
      try {
         parser = new MiniJavaParser(new FileInputStream(filePath), "UTF-8");

         if (verbose) {
            System.setOut(new PrintStream(new FileOutputStream(new File("verbose.txt"))));
            parser.enable_tracing();
         } else {
            parser.disable_tracing();
         }
      } catch (final FileNotFoundException e) {
         System.err.println(e);
         throw new RuntimeException("The file could not be read.");
      }

      try {
         parser.Program();
      } catch (final ParseException e) {
         // If this catch block is triggered, the ParseException that triggered it has no
         // recovery method implemented in the parser
         parser.exceptions.add(e);
      }

      final int errorCount = parser.exceptions.size();

      for (final ParseException e : parser.exceptions) {
         final StringBuilder expectedTokens = new StringBuilder();
         final Token t = e.currentToken;

         if (e.expectedTokenSequences.length > 0) {
            for (int i = 0; i < e.expectedTokenSequences.length; i++) {
               final int[] sequence = e.expectedTokenSequences[i];

               if (i > 0) {
                  expectedTokens.append("or ");
               }

               for (final int kind : sequence) {
                  expectedTokens.append(e.tokenImage[kind]).append(" ");
               }
            }

            System.err.printf("%s:%03d:%03d: Syntax Error: expecting %s%n", filePath, t.beginLine,
                  t.beginColumn, expectedTokens);
         } else {
            System.err.printf("%s:%03d:%03d: Syntax Error: expecting something or other%n", filePath,
                  t.beginLine, t.beginColumn);
         }
      }

      final String result = String.format("filename=%s, errors=%d%n", filePath, errorCount);
      out.printf(result);
   }
}
