PARSER_BEGIN(MiniJavaParser)
package parser;

import java.util.List;
import java.util.LinkedList;
import java.util.Stack;

public class MiniJavaParser {
   public static final List<ParseException> exceptions = new LinkedList<ParseException>();
}
PARSER_END(MiniJavaParser)

SKIP : {
   < "//" (~["\n"])* "\n" >
 | < "/*" (["\u0000"-"\uFFFE"])* "*/" >
 | < " " | "\t" | "\n" | "\f" | "\r" >
}

TOKEN : {
   < CLASS : "class" >
 | < PUBLIC : "public" >
 | < STATIC : "static" >
 | < PRINT : "System.out.println" >
 | < VOID : "void" >
 | < EXTENDS : "extends" >
 | < RETURN : "return" >
 | < INT_TYPE : "int" >
 | < BOOLEAN_TYPE : "boolean" >
 | < STRING : "String" >
 | < MAIN : "main" >
 | < LENGTH : "length" >
 | < IF : "if" >
 | < ELSE : "else" >
 | < WHILE : "while" >
 | < TRUE : "true" >
 | < FALSE : "false" >
 | < THIS : "this" >
 | < NEW : "new" >
 | < BANG : "!" >
 | < LEFT_PAREN : "(" >
 | < RIGHT_PAREN : ")" >
 | < LEFT_BRACE : "{" >
 | < RIGHT_BRACE : "}" >
 | < LEFT_BRACKET : "[" >
 | < RIGHT_BRACKET : "]" >
 | < ASSIGN : "=" >
 | < DOT : "." >
 | < COMMA : "," >
 | < SEMICOLON : ";" >
 | < INT_LITERAL : ["0"-"9"] (["0"-"9"])* >
 | < OP : "&&" | "<" | "+" | "-" | "*" >
 | < ID : ["a"-"z", "A"-"Z"] (["a"-"z", "A"-"Z", "0"-"9", "_"])* >
 | < ERROR : ["\u0000"-"\uFFFE"] >
}

void Program() : {} {
   MainClass() (ClassDecl())*
}

void MainClass() : {} {
   <CLASS> <ID> <LEFT_BRACE> <PUBLIC> <STATIC> <VOID> <MAIN>
         <LEFT_PAREN> <STRING> <LEFT_BRACKET> <RIGHT_BRACKET> <ID> <RIGHT_PAREN>
         <LEFT_BRACE> Statement() <RIGHT_BRACE> <RIGHT_BRACE>
}

void ClassDecl() : {} {
   <CLASS> <ID> [LOOKAHEAD(1) <EXTENDS> <ID>]
         <LEFT_BRACE> (VarDecl())* (MethodDecl())* <RIGHT_BRACE>
}

void VarDecl() : {} {
   try {
      Type() <ID> <SEMICOLON>
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void MethodDecl() : {} {
   try {
      <PUBLIC> Type() <ID> <LEFT_PAREN> [FormalList()] <RIGHT_PAREN>
            <LEFT_BRACE> (LOOKAHEAD(2) VarDecl())* (Statement())* <RETURN>
            Exp() <SEMICOLON> <RIGHT_BRACE>
   } catch (ParseException e) {
      recoverError(RIGHT_BRACE);
   }
}

void FormalList() : {} {
   try {
      Type() <ID> (FormalRest())*
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void FormalRest() : {} {
   try {
      <COMMA> Type() <ID>
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void Type() : {} {
   try {
      <BOOLEAN_TYPE>
    | <ID>
    | <INT_TYPE> [<LEFT_BRACKET> <RIGHT_BRACKET>]
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void Statement() : {} {
   try {
      <LEFT_BRACE> (Statement())* <RIGHT_BRACE>
    | <IF> <LEFT_PAREN> Exp() <RIGHT_PAREN> Statement() <ELSE> Statement()
    | <WHILE> <LEFT_PAREN> Exp() <RIGHT_PAREN> Statement()
    | "System.out.println" <LEFT_PAREN> Exp() <RIGHT_PAREN> <SEMICOLON>
    | <ID> [<LEFT_BRACKET> Exp() <RIGHT_BRACKET>] <ASSIGN> Exp() <SEMICOLON>
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void Exp() : {} {
   try {
      ( <INT_LITERAL>
      | <TRUE>
      | <FALSE>
      | <ID>
      | <THIS>
      | <NEW> ( <INT_TYPE> <LEFT_BRACKET> Exp() <RIGHT_BRACKET>
              | <ID> <LEFT_PAREN> <RIGHT_PAREN>
              )
      | <BANG> Exp()
      | <LEFT_PAREN> Exp() <RIGHT_PAREN>
      ) [LOOKAHEAD(1) Exp1()]
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void Exp1() : {} {
   try {
      <OP> Exp()
    | <LEFT_BRACKET> Exp() <RIGHT_BRACKET>
    | <DOT> ( <LENGTH>
            | <ID> <LEFT_PAREN> [ExpList()] <RIGHT_PAREN>
            )
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void ExpList() : {} {
   try {
      Exp() (ExpRest())*
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

void ExpRest() : {} {
   try {
      <COMMA> Exp()
   } catch (ParseException e) {
      recoverError(SEMICOLON);
   }
}

// adapted from http://javacc.java.net/doc/errorrecovery.html
JAVACODE
void recoverError(final int... kinds) {
   exceptions.add(generateParseException());

   Token t;
   int curr = 0;
   do {
      t = getNextToken();
      if (t.kind == kinds[curr]) {
         curr++;
      }
   } while (t.kind != EOF && curr < kinds.length);
}
